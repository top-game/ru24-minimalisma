﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Net;
using Ookii.Dialogs;

namespace DayZ_Epoch_test
{
    public class Main : RU24Lib.Types.LauncherAddon
    {
        public override void Init()
        {
            Console.WriteLine("Load DayZ Epoch plugin");
            var dayz_epoch = new RU24Lib.Types.ArmA2Modification(); // create modification
            Console.WriteLine("[DayZ Epoch plugin] Settings up...");
            // Setting up:
            dayz_epoch.Name = "DayZ Epoch"; // Name
            Console.WriteLine("[DayZ Epoch plugin] Settings up... 1");
            dayz_epoch.Version = "1.0.5.1"; // Version
            Console.WriteLine("[DayZ Epoch plugin] Settings up... 2");
            dayz_epoch.FolderName = "@DayZ_Epoch";
            Console.WriteLine("[DayZ Epoch plugin] Settings up... 3");
            dayz_epoch.PictureUrl = "http://hydra-media.cursecdn.com/dayz-ru.gamepedia.com/thumb/1/1f/Dayz_logo_ca.png/300px-Dayz_logo_ca.png"; // Picture Url (local or network)
            Console.WriteLine("[DayZ Epoch plugin] Settings up... 4");
            dayz_epoch.UID = "DAYZEPOCH_1051"; // UID is a pointer for repositories
            Console.WriteLine("[DayZ Epoch plugin] Settings up... 5");
            // dayz_epoch.ModificationLibaryUri = null; -- We doesn't setup this 
            //dayz_epoch.Extract.ExtractType = RU24Lib.Types.ArmA2ModificationExtractType.TorrentZIP; // We load torrent and unzip (unrar, extract) it
            Console.WriteLine("[DayZ Epoch plugin] : Settings up complete.");
            Console.WriteLine("[DayZ Epoch plugin] : AddMod");

            // += delegate
            {
               // controller = Simple_RU24.Engine.MainWindow.AddMod(dayz_epoch);
               // controller.OnDownloadRequest += controller_OnDownloadRequest;

                Console.WriteLine("[DayZ Epoch plugin] Mod Added");
            }//;
            Console.WriteLine("DayZ Epoch plugin loaded");

            // We don't need to include this:
            //base.Init();
        }

        public string Name = "DayZ Epoch plugin";
        public string Version = "1.0.5.1";

        Simple_RU24.LocalTypes.ModsGridItemController controller { get; set; }


        void controller_OnDownloadRequest(object sender, EventArgs e)
        {
            using (var td = new TaskDialog())
            {
                var okButton = new TaskDialogButton("Загрузить");
                var cancelButton = new TaskDialogButton("Отмена");

                okButton.ButtonType = ButtonType.Ok;
                cancelButton.ButtonType = ButtonType.Cancel;

                td.Buttons.Add(okButton);
                td.Buttons.Add(cancelButton);

                td.WindowTitle = "DayZ Epoch - установка";

                td.MainInstruction = "Перед установкой мода, прочтите описание мода. Если вы уже готовы к установке, нажмите загрузить";
                td.MainIcon = TaskDialogIcon.Custom;
                td.CustomMainIcon = System.Drawing.SystemIcons.Question;

                td.ExpandedByDefault = false;
                td.ExpandedControlText = "Описание мода";
                td.ExpandedInformation = "Arma2 DayZ: Epoch is an unofficial open source client and server mod based on DayZ Community Edition. Arma2 DayZ: Epoch Mod brings more RP to DayZ Mod with; Metals based economy, NPC Traders, Lockable Vaults, More roles, and more."
                + "\n" +
                "Arma2 DayZ: Epoch is set in a future time after the Great Infection. Humanity is slowly returning to the wasteland, industrious survivors have begun to join together in small groups and rebuild society. Small camps of merchants have started to pop up to do business with survivors who collect goods from the wasted remnants of civilization. Wholesalers have landed on the shores offering imported goods to aid the rebuilding effort."
                + "\n" +
                "All is not safe however some vestiges of infection still remain and rogue bandits still roam the countryside preying on unsuspecting survivors.ЭSurvive, explore, build wealth, make your way through the harsh environment and assist in the effort to rebuild, or burn it all down its your choice, live in the Epoch.";

                Console.WriteLine("[DayZ Epoch plugin] Show window");
                var whichButtonWasPressed = td.ShowDialog(Simple_RU24.Engine.MainWindow);
                if (whichButtonWasPressed == okButton)
                {
                    Console.WriteLine("[DayZ Epoch plugin] Yey, wait to load start...");
                    LoadMod();
                }
                else
                {
                    Console.WriteLine("[DayZ Epoch plugin] Load canceled");
                    return;
                }
            }
        }

        public void LoadMod()
        {
            Console.WriteLine("Request to load sended");
            
                pd.Animation = Ookii.Dialogs.AnimationResource.GetShellAnimation(Ookii.Dialogs.ShellAnimation.FileMove);
                pd.ShowTimeRemaining = true;
            
                pd.DoWork += ModDownloader_DoWork;
                pd.Show();
            
        }

        Ookii.Dialogs.ProgressDialog pd = new Ookii.Dialogs.ProgressDialog();
        void ModDownloader_DoWork(object sender, DoWorkEventArgs e)
        {
            Console.WriteLine("[DayZ Epoch plugin] Load Mod...");
            //controller.Item.downloadButton.Enabled = false;
            var dmanager = new Simple_RU24.DownloadManagers.TorrentDownloadManager();
            dmanager.SavePath = Simple_RU24.DevelopersInfos.Variables.Paths.CachePath + "\\some.7z";
            try
            {
                var wc = new WebClient();
                bool completed =false;
                wc.DownloadFile("http://epochmod.com/ClientFiles/DayZ_Epoch_Client_1.0.5.1_Release.7z.torrent", Simple_RU24.DevelopersInfos.Variables.Paths.CachePath + "\\dayzepoch.torrent");
                // You should to know: Simple_RU24.DevelopersInfos gives all need info
                wc.DownloadFileCompleted += delegate{ completed = true; };
                while (completed)
                {

                }
            }
            catch (Exception exc)
            {
                pd.ReportProgress(0);
                System.Windows.Forms.MessageBox.Show("Error: " + exc + ".\n" + exc.Message, "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                return;
            }

            
            var tmanager = dmanager.DownloadTorrent(Simple_RU24.DevelopersInfos.Variables.Paths.CachePath + "\\dayzepoch.torrent", false);
            tmanager.Start();

            while (tmanager.State != MonoTorrent.Common.TorrentState.Seeding || tmanager.Progress == 100)
            {
                if (pd.CancellationPending) { tmanager.Stop(); return; }

                string state = "";
                if (tmanager.State == MonoTorrent.Common.TorrentState.Metadata)
                {
                    state = "Получение мета-даты";
                }
                else if (tmanager.State == MonoTorrent.Common.TorrentState.Paused)
                {
                    state = "Пауза";
                }
                else if (tmanager.State == MonoTorrent.Common.TorrentState.Seeding)
                {
                    state = "Раздача";
                }
                else if (tmanager.State == MonoTorrent.Common.TorrentState.Error)
                {
                    state = "Ошибка";
                }
                else if (tmanager.State == MonoTorrent.Common.TorrentState.Downloading)
                {
                    state = "Загрузка";
                }

                pd.WindowTitle = "DayZ Epoch download - " + tmanager.Progress + "% completed";
                pd.ProgressBarStyle = ProgressBarStyle.ProgressBar;
                pd.ReportProgress((int)tmanager.Progress, state, tmanager.State != MonoTorrent.Common.TorrentState.Error ? "↓ D: " + tmanager.Monitor.DownloadSpeed + ". ↑ U: " + tmanager.Monitor.UploadSpeed + ". S: " + tmanager.Peers.Seeds : tmanager.Error.Reason.ToString());

                Thread.Sleep(1000);
                Console.WriteLine("" + tmanager.Progress);
                //controller.SetProgress(Simple_RU24.WindowsFunctions.Main.ProgressBarState.Normal, (int)tmanager.Progress);
            }

            pd.ReportProgress(100, "Загрузка завершена", "Загрузка завершенна");

            //controller.Item.downloadButton.Enabled = false;
            
        }

        

    }
}
