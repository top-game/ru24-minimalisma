﻿namespace Simple_RU24
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.homePage = new System.Windows.Forms.TabPage();
            this.commandLink1 = new CommandLink();
            this.welcomeLabel2 = new System.Windows.Forms.Label();
            this.welcomeLabel1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.serversPage = new System.Windows.Forms.TabPage();
            this.serversGrid1 = new Simple_RU24.Controls.ServersGrid();
            this.servStrip = new System.Windows.Forms.StatusStrip();
            this.sbar_lab = new System.Windows.Forms.ToolStripStatusLabel();
            this.sbar_progbar = new System.Windows.Forms.ToolStripProgressBar();
            this.serverSearch_Panel = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.serverSearch_PBAR = new System.Windows.Forms.ProgressBar();
            this.srvFilter_Modifications = new System.Windows.Forms.CheckBox();
            this.srvFilter_NotOfficial = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.srvFilter_Empty = new System.Windows.Forms.CheckBox();
            this.srvFilter_Full = new System.Windows.Forms.CheckBox();
            this.srvFilter_NotTrusted = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.serverSearch_Button = new System.Windows.Forms.Button();
            this.modsPage = new System.Windows.Forms.TabPage();
            this.modsWarning_Panel = new System.Windows.Forms.Panel();
            this.modsWarning_Button = new System.Windows.Forms.Button();
            this.modsWarning_Image = new System.Windows.Forms.PictureBox();
            this.modsWarning_Text = new System.Windows.Forms.Label();
            this.modsWarning_Title = new System.Windows.Forms.Label();
            this.modsPage_LoadLabel = new System.Windows.Forms.Label();
            this.modsPage_LoadBar = new System.Windows.Forms.ProgressBar();
            this.modsGrid1 = new Simple_RU24.Controls.ModsGrid();
            this.musicPage = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.TabsImages = new System.Windows.Forms.ImageList(this.components);
            this.ServersUpdater = new System.ComponentModel.BackgroundWorker();
            this.ServerPanel = new System.Windows.Forms.Panel();
            this.ServerPanel_PlayersListBox = new System.Windows.Forms.ListBox();
            this.ServerPanel_NowPlaying = new System.Windows.Forms.Label();
            this.ServerPanel_MaxPlayersCounter = new System.Windows.Forms.Label();
            this.ServerPanel_NameOfServer = new System.Windows.Forms.Label();
            this.ServerPanel_InnerContrent = new System.Windows.Forms.Label();
            this.ServerPanel_JoinButton = new System.Windows.Forms.Button();
            this.ServerPanel_Close = new System.Windows.Forms.PictureBox();
            this.ServerPanel_Header = new System.Windows.Forms.Label();
            this.ServerImages = new System.Windows.Forms.ImageList(this.components);
            this.tabControl1.SuspendLayout();
            this.homePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.serversPage.SuspendLayout();
            this.servStrip.SuspendLayout();
            this.serverSearch_Panel.SuspendLayout();
            this.modsPage.SuspendLayout();
            this.modsWarning_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modsWarning_Image)).BeginInit();
            this.musicPage.SuspendLayout();
            this.ServerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ServerPanel_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.homePage);
            this.tabControl1.Controls.Add(this.serversPage);
            this.tabControl1.Controls.Add(this.modsPage);
            this.tabControl1.Controls.Add(this.musicPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.ImageList = this.TabsImages;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 3, 40, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1052, 547);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            this.tabControl1.TabStopChanged += new System.EventHandler(this.tabControl1_TabStopChanged);
            // 
            // homePage
            // 
            this.homePage.Controls.Add(this.commandLink1);
            this.homePage.Controls.Add(this.welcomeLabel2);
            this.homePage.Controls.Add(this.welcomeLabel1);
            this.homePage.Controls.Add(this.pictureBox1);
            this.homePage.ImageKey = "house.png";
            this.homePage.Location = new System.Drawing.Point(4, 39);
            this.homePage.Name = "homePage";
            this.homePage.Padding = new System.Windows.Forms.Padding(3);
            this.homePage.Size = new System.Drawing.Size(1044, 504);
            this.homePage.TabIndex = 0;
            this.homePage.Text = "Главная";
            this.homePage.UseVisualStyleBackColor = true;
            // 
            // commandLink1
            // 
            this.commandLink1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.commandLink1.Location = new System.Drawing.Point(14, 209);
            this.commandLink1.Name = "commandLink1";
            this.commandLink1.Note = "Настройте игру и лаунчер, как удобно вам.";
            this.commandLink1.Size = new System.Drawing.Size(326, 77);
            this.commandLink1.TabIndex = 6;
            this.commandLink1.Text = "Настройки";
            this.commandLink1.UseVisualStyleBackColor = true;
            this.commandLink1.Click += new System.EventHandler(this.commandLink1_Click);
            // 
            // welcomeLabel2
            // 
            this.welcomeLabel2.AutoSize = true;
            this.welcomeLabel2.BackColor = System.Drawing.Color.White;
            this.welcomeLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.welcomeLabel2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.welcomeLabel2.Location = new System.Drawing.Point(10, 38);
            this.welcomeLabel2.Name = "welcomeLabel2";
            this.welcomeLabel2.Size = new System.Drawing.Size(135, 20);
            this.welcomeLabel2.TabIndex = 2;
            this.welcomeLabel2.Text = "Добро пожаловать!";
            // 
            // welcomeLabel1
            // 
            this.welcomeLabel1.AutoSize = true;
            this.welcomeLabel1.BackColor = System.Drawing.Color.White;
            this.welcomeLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.welcomeLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.welcomeLabel1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.welcomeLabel1.Location = new System.Drawing.Point(7, 6);
            this.welcomeLabel1.Name = "welcomeLabel1";
            this.welcomeLabel1.Size = new System.Drawing.Size(333, 29);
            this.welcomeLabel1.TabIndex = 1;
            this.welcomeLabel1.Text = "RU24 MinimalismaLauncher";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Simple_RU24.Properties.Resources.ru24_2;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(906, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // serversPage
            // 
            this.serversPage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.serversPage.Controls.Add(this.serversGrid1);
            this.serversPage.Controls.Add(this.servStrip);
            this.serversPage.Controls.Add(this.serverSearch_Panel);
            this.serversPage.ImageKey = "server.png";
            this.serversPage.Location = new System.Drawing.Point(4, 39);
            this.serversPage.Name = "serversPage";
            this.serversPage.Padding = new System.Windows.Forms.Padding(3);
            this.serversPage.Size = new System.Drawing.Size(1044, 504);
            this.serversPage.TabIndex = 1;
            this.serversPage.Text = "Сервера";
            // 
            // serversGrid1
            // 
            this.serversGrid1.BackColor = System.Drawing.Color.White;
            this.serversGrid1.Location = new System.Drawing.Point(4, 1);
            this.serversGrid1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.serversGrid1.Name = "serversGrid1";
            this.serversGrid1.Size = new System.Drawing.Size(858, 500);
            this.serversGrid1.TabIndex = 3;
            // 
            // servStrip
            // 
            this.servStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbar_lab,
            this.sbar_progbar});
            this.servStrip.Location = new System.Drawing.Point(3, 479);
            this.servStrip.Name = "servStrip";
            this.servStrip.Size = new System.Drawing.Size(860, 22);
            this.servStrip.TabIndex = 2;
            this.servStrip.Text = "undefined";
            this.servStrip.Visible = false;
            // 
            // sbar_lab
            // 
            this.sbar_lab.Name = "sbar_lab";
            this.sbar_lab.Size = new System.Drawing.Size(55, 17);
            this.sbar_lab.Text = "Загрузка";
            // 
            // sbar_progbar
            // 
            this.sbar_progbar.Name = "sbar_progbar";
            this.sbar_progbar.Size = new System.Drawing.Size(100, 16);
            this.sbar_progbar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // serverSearch_Panel
            // 
            this.serverSearch_Panel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.serverSearch_Panel.Controls.Add(this.button4);
            this.serverSearch_Panel.Controls.Add(this.serverSearch_PBAR);
            this.serverSearch_Panel.Controls.Add(this.srvFilter_Modifications);
            this.serverSearch_Panel.Controls.Add(this.srvFilter_NotOfficial);
            this.serverSearch_Panel.Controls.Add(this.comboBox1);
            this.serverSearch_Panel.Controls.Add(this.label2);
            this.serverSearch_Panel.Controls.Add(this.srvFilter_Empty);
            this.serverSearch_Panel.Controls.Add(this.srvFilter_Full);
            this.serverSearch_Panel.Controls.Add(this.srvFilter_NotTrusted);
            this.serverSearch_Panel.Controls.Add(this.label1);
            this.serverSearch_Panel.Controls.Add(this.serverSearch_Button);
            this.serverSearch_Panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.serverSearch_Panel.Location = new System.Drawing.Point(863, 3);
            this.serverSearch_Panel.Name = "serverSearch_Panel";
            this.serverSearch_Panel.Size = new System.Drawing.Size(178, 498);
            this.serverSearch_Panel.TabIndex = 1;
            this.serverSearch_Panel.TabStop = false;
            this.serverSearch_Panel.Text = "Поиск серверов";
            // 
            // button4
            // 
            this.button4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button4.Location = new System.Drawing.Point(9, 287);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 29);
            this.button4.TabIndex = 12;
            this.button4.Text = "Обновить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // serverSearch_PBAR
            // 
            this.serverSearch_PBAR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.serverSearch_PBAR.Location = new System.Drawing.Point(13, 258);
            this.serverSearch_PBAR.Name = "serverSearch_PBAR";
            this.serverSearch_PBAR.Size = new System.Drawing.Size(157, 23);
            this.serverSearch_PBAR.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.serverSearch_PBAR.TabIndex = 11;
            // 
            // srvFilter_Modifications
            // 
            this.srvFilter_Modifications.AutoSize = true;
            this.srvFilter_Modifications.Enabled = false;
            this.srvFilter_Modifications.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.srvFilter_Modifications.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.srvFilter_Modifications.Location = new System.Drawing.Point(9, 174);
            this.srvFilter_Modifications.Name = "srvFilter_Modifications";
            this.srvFilter_Modifications.Size = new System.Drawing.Size(130, 21);
            this.srvFilter_Modifications.TabIndex = 9;
            this.srvFilter_Modifications.Text = "С модификациями";
            this.srvFilter_Modifications.UseVisualStyleBackColor = true;
            // 
            // srvFilter_NotOfficial
            // 
            this.srvFilter_NotOfficial.AutoSize = true;
            this.srvFilter_NotOfficial.Enabled = false;
            this.srvFilter_NotOfficial.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.srvFilter_NotOfficial.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.srvFilter_NotOfficial.Location = new System.Drawing.Point(9, 151);
            this.srvFilter_NotOfficial.Name = "srvFilter_NotOfficial";
            this.srvFilter_NotOfficial.Size = new System.Drawing.Size(125, 21);
            this.srvFilter_NotOfficial.TabIndex = 8;
            this.srvFilter_NotOfficial.Text = "Не официальные";
            this.srvFilter_NotOfficial.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox1.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "TOP-GAME VK Based",
            "-- Интегрированные --",
            "-- Все --",
            "-- Добавить -- "});
            this.comboBox1.Location = new System.Drawing.Point(9, 32);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(162, 25);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Репозиторий:";
            // 
            // srvFilter_Empty
            // 
            this.srvFilter_Empty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.srvFilter_Empty.AutoSize = true;
            this.srvFilter_Empty.Enabled = false;
            this.srvFilter_Empty.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.srvFilter_Empty.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.srvFilter_Empty.Location = new System.Drawing.Point(9, 128);
            this.srvFilter_Empty.Name = "srvFilter_Empty";
            this.srvFilter_Empty.Size = new System.Drawing.Size(69, 21);
            this.srvFilter_Empty.TabIndex = 5;
            this.srvFilter_Empty.Text = "Пустые";
            this.srvFilter_Empty.UseVisualStyleBackColor = true;
            // 
            // srvFilter_Full
            // 
            this.srvFilter_Full.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.srvFilter_Full.AutoSize = true;
            this.srvFilter_Full.Enabled = false;
            this.srvFilter_Full.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.srvFilter_Full.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.srvFilter_Full.Location = new System.Drawing.Point(9, 105);
            this.srvFilter_Full.Name = "srvFilter_Full";
            this.srvFilter_Full.Size = new System.Drawing.Size(98, 21);
            this.srvFilter_Full.TabIndex = 4;
            this.srvFilter_Full.Text = "Заполненые";
            this.srvFilter_Full.UseVisualStyleBackColor = true;
            // 
            // srvFilter_NotTrusted
            // 
            this.srvFilter_NotTrusted.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.srvFilter_NotTrusted.AutoSize = true;
            this.srvFilter_NotTrusted.Enabled = false;
            this.srvFilter_NotTrusted.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.srvFilter_NotTrusted.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.srvFilter_NotTrusted.Location = new System.Drawing.Point(9, 84);
            this.srvFilter_NotTrusted.Name = "srvFilter_NotTrusted";
            this.srvFilter_NotTrusted.Size = new System.Drawing.Size(125, 21);
            this.srvFilter_NotTrusted.TabIndex = 2;
            this.srvFilter_NotTrusted.Text = "Не проверенные";
            this.srvFilter_NotTrusted.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(6, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Не находить сервера:";
            // 
            // serverSearch_Button
            // 
            this.serverSearch_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverSearch_Button.Enabled = false;
            this.serverSearch_Button.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.serverSearch_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.serverSearch_Button.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.serverSearch_Button.Location = new System.Drawing.Point(13, 209);
            this.serverSearch_Button.Name = "serverSearch_Button";
            this.serverSearch_Button.Size = new System.Drawing.Size(158, 43);
            this.serverSearch_Button.TabIndex = 0;
            this.serverSearch_Button.Text = "Искать";
            this.serverSearch_Button.UseVisualStyleBackColor = true;
            this.serverSearch_Button.Click += new System.EventHandler(this.button1_Click);
            // 
            // modsPage
            // 
            this.modsPage.Controls.Add(this.modsWarning_Panel);
            this.modsPage.Controls.Add(this.modsPage_LoadLabel);
            this.modsPage.Controls.Add(this.modsPage_LoadBar);
            this.modsPage.Controls.Add(this.modsGrid1);
            this.modsPage.ImageKey = "mods.png";
            this.modsPage.Location = new System.Drawing.Point(4, 39);
            this.modsPage.Name = "modsPage";
            this.modsPage.Size = new System.Drawing.Size(1044, 504);
            this.modsPage.TabIndex = 2;
            this.modsPage.Text = "Моды";
            this.modsPage.UseVisualStyleBackColor = true;
            // 
            // modsWarning_Panel
            // 
            this.modsWarning_Panel.BackColor = System.Drawing.Color.IndianRed;
            this.modsWarning_Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modsWarning_Panel.Controls.Add(this.modsWarning_Button);
            this.modsWarning_Panel.Controls.Add(this.modsWarning_Image);
            this.modsWarning_Panel.Controls.Add(this.modsWarning_Text);
            this.modsWarning_Panel.Controls.Add(this.modsWarning_Title);
            this.modsWarning_Panel.Location = new System.Drawing.Point(354, 130);
            this.modsWarning_Panel.Margin = new System.Windows.Forms.Padding(0);
            this.modsWarning_Panel.Name = "modsWarning_Panel";
            this.modsWarning_Panel.Size = new System.Drawing.Size(367, 157);
            this.modsWarning_Panel.TabIndex = 3;
            this.modsWarning_Panel.Visible = false;
            this.modsWarning_Panel.VisibleChanged += new System.EventHandler(this.modsWarning_Panel_VisibleChanged);
            // 
            // modsWarning_Button
            // 
            this.modsWarning_Button.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.modsWarning_Button.Image = global::Simple_RU24.Properties.Resources.repository_add;
            this.modsWarning_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.modsWarning_Button.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.modsWarning_Button.Location = new System.Drawing.Point(0, 114);
            this.modsWarning_Button.Name = "modsWarning_Button";
            this.modsWarning_Button.Size = new System.Drawing.Size(365, 41);
            this.modsWarning_Button.TabIndex = 10;
            this.modsWarning_Button.Text = "Добавить репозитории";
            this.modsWarning_Button.UseVisualStyleBackColor = true;
            this.modsWarning_Button.Click += new System.EventHandler(this.modsWarning_Button_Click);
            // 
            // modsWarning_Image
            // 
            this.modsWarning_Image.BackColor = System.Drawing.Color.Transparent;
            this.modsWarning_Image.Image = ((System.Drawing.Image)(resources.GetObject("modsWarning_Image.Image")));
            this.modsWarning_Image.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.modsWarning_Image.Location = new System.Drawing.Point(8, 31);
            this.modsWarning_Image.Name = "modsWarning_Image";
            this.modsWarning_Image.Size = new System.Drawing.Size(32, 32);
            this.modsWarning_Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.modsWarning_Image.TabIndex = 13;
            this.modsWarning_Image.TabStop = false;
            // 
            // modsWarning_Text
            // 
            this.modsWarning_Text.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modsWarning_Text.BackColor = System.Drawing.Color.Transparent;
            this.modsWarning_Text.ForeColor = System.Drawing.Color.White;
            this.modsWarning_Text.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.modsWarning_Text.Location = new System.Drawing.Point(46, 31);
            this.modsWarning_Text.Name = "modsWarning_Text";
            this.modsWarning_Text.Size = new System.Drawing.Size(318, 71);
            this.modsWarning_Text.TabIndex = 12;
            this.modsWarning_Text.Text = "Не один репозиторий не найден";
            // 
            // modsWarning_Title
            // 
            this.modsWarning_Title.AutoSize = true;
            this.modsWarning_Title.BackColor = System.Drawing.Color.Transparent;
            this.modsWarning_Title.Font = new System.Drawing.Font("Segoe UI Light", 15F);
            this.modsWarning_Title.ForeColor = System.Drawing.Color.White;
            this.modsWarning_Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.modsWarning_Title.Location = new System.Drawing.Point(3, 0);
            this.modsWarning_Title.Name = "modsWarning_Title";
            this.modsWarning_Title.Size = new System.Drawing.Size(47, 28);
            this.modsWarning_Title.TabIndex = 11;
            this.modsWarning_Title.Text = "Оу! ";
            // 
            // modsPage_LoadLabel
            // 
            this.modsPage_LoadLabel.AutoSize = true;
            this.modsPage_LoadLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.modsPage_LoadLabel.Location = new System.Drawing.Point(8, 6);
            this.modsPage_LoadLabel.Name = "modsPage_LoadLabel";
            this.modsPage_LoadLabel.Size = new System.Drawing.Size(175, 20);
            this.modsPage_LoadLabel.TabIndex = 1;
            this.modsPage_LoadLabel.Text = "Пожалуйста, подождите...";
            // 
            // modsPage_LoadBar
            // 
            this.modsPage_LoadBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.modsPage_LoadBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.modsPage_LoadBar.Location = new System.Drawing.Point(0, 0);
            this.modsPage_LoadBar.Name = "modsPage_LoadBar";
            this.modsPage_LoadBar.Size = new System.Drawing.Size(1044, 3);
            this.modsPage_LoadBar.TabIndex = 0;
            this.modsPage_LoadBar.Value = 50;
            // 
            // modsGrid1
            // 
            this.modsGrid1.Location = new System.Drawing.Point(0, 0);
            this.modsGrid1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.modsGrid1.Name = "modsGrid1";
            this.modsGrid1.Size = new System.Drawing.Size(1044, 500);
            this.modsGrid1.TabIndex = 4;
            // 
            // musicPage
            // 
            this.musicPage.Controls.Add(this.label3);
            this.musicPage.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.musicPage.ImageKey = "music.png";
            this.musicPage.Location = new System.Drawing.Point(4, 39);
            this.musicPage.Name = "musicPage";
            this.musicPage.Size = new System.Drawing.Size(1044, 504);
            this.musicPage.TabIndex = 3;
            this.musicPage.Text = "Музыка";
            this.musicPage.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(613, 84);
            this.label3.TabIndex = 0;
            this.label3.Text = "Раздел в разработке. \r\nВозможно, эта возможность потребует установки специального" +
    " модуля для лаунчера.\r\n\r\nНо позже!";
            // 
            // TabsImages
            // 
            this.TabsImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TabsImages.ImageStream")));
            this.TabsImages.TransparentColor = System.Drawing.Color.Transparent;
            this.TabsImages.Images.SetKeyName(0, "house.png");
            this.TabsImages.Images.SetKeyName(1, "mods.png");
            this.TabsImages.Images.SetKeyName(2, "music.png");
            this.TabsImages.Images.SetKeyName(3, "server.png");
            // 
            // ServersUpdater
            // 
            this.ServersUpdater.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ServersUpdater_DoWork);
            // 
            // ServerPanel
            // 
            this.ServerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ServerPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ServerPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ServerPanel.Controls.Add(this.ServerPanel_PlayersListBox);
            this.ServerPanel.Controls.Add(this.ServerPanel_NowPlaying);
            this.ServerPanel.Controls.Add(this.ServerPanel_MaxPlayersCounter);
            this.ServerPanel.Controls.Add(this.ServerPanel_NameOfServer);
            this.ServerPanel.Controls.Add(this.ServerPanel_InnerContrent);
            this.ServerPanel.Controls.Add(this.ServerPanel_JoinButton);
            this.ServerPanel.Controls.Add(this.ServerPanel_Close);
            this.ServerPanel.Controls.Add(this.ServerPanel_Header);
            this.ServerPanel.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.ServerPanel.Location = new System.Drawing.Point(1048, -20);
            this.ServerPanel.Name = "ServerPanel";
            this.ServerPanel.Size = new System.Drawing.Size(259, 573);
            this.ServerPanel.TabIndex = 6;
            // 
            // ServerPanel_PlayersListBox
            // 
            this.ServerPanel_PlayersListBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ServerPanel_PlayersListBox.FormattingEnabled = true;
            this.ServerPanel_PlayersListBox.ItemHeight = 17;
            this.ServerPanel_PlayersListBox.Items.AddRange(new object[] {
            "..."});
            this.ServerPanel_PlayersListBox.Location = new System.Drawing.Point(14, 184);
            this.ServerPanel_PlayersListBox.Name = "ServerPanel_PlayersListBox";
            this.ServerPanel_PlayersListBox.Size = new System.Drawing.Size(231, 174);
            this.ServerPanel_PlayersListBox.TabIndex = 7;
            // 
            // ServerPanel_NowPlaying
            // 
            this.ServerPanel_NowPlaying.AutoSize = true;
            this.ServerPanel_NowPlaying.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServerPanel_NowPlaying.Location = new System.Drawing.Point(11, 147);
            this.ServerPanel_NowPlaying.Name = "ServerPanel_NowPlaying";
            this.ServerPanel_NowPlaying.Size = new System.Drawing.Size(17, 17);
            this.ServerPanel_NowPlaying.TabIndex = 6;
            this.ServerPanel_NowPlaying.Text = "...";
            // 
            // ServerPanel_MaxPlayersCounter
            // 
            this.ServerPanel_MaxPlayersCounter.AutoSize = true;
            this.ServerPanel_MaxPlayersCounter.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServerPanel_MaxPlayersCounter.Location = new System.Drawing.Point(11, 114);
            this.ServerPanel_MaxPlayersCounter.Name = "ServerPanel_MaxPlayersCounter";
            this.ServerPanel_MaxPlayersCounter.Size = new System.Drawing.Size(17, 17);
            this.ServerPanel_MaxPlayersCounter.TabIndex = 5;
            this.ServerPanel_MaxPlayersCounter.Text = "...";
            // 
            // ServerPanel_NameOfServer
            // 
            this.ServerPanel_NameOfServer.AutoSize = true;
            this.ServerPanel_NameOfServer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServerPanel_NameOfServer.Location = new System.Drawing.Point(11, 79);
            this.ServerPanel_NameOfServer.Name = "ServerPanel_NameOfServer";
            this.ServerPanel_NameOfServer.Size = new System.Drawing.Size(17, 17);
            this.ServerPanel_NameOfServer.TabIndex = 4;
            this.ServerPanel_NameOfServer.Text = "...";
            // 
            // ServerPanel_InnerContrent
            // 
            this.ServerPanel_InnerContrent.AutoSize = true;
            this.ServerPanel_InnerContrent.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Bold);
            this.ServerPanel_InnerContrent.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServerPanel_InnerContrent.Location = new System.Drawing.Point(11, 62);
            this.ServerPanel_InnerContrent.Name = "ServerPanel_InnerContrent";
            this.ServerPanel_InnerContrent.Size = new System.Drawing.Size(244, 119);
            this.ServerPanel_InnerContrent.TabIndex = 3;
            this.ServerPanel_InnerContrent.Text = "Название сервера:\r\n\r\nМаксимальное количество игроков:\r\n\r\nСейчас играет:\r\n\r\nПолный" +
    " список:";
            // 
            // ServerPanel_JoinButton
            // 
            this.ServerPanel_JoinButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServerPanel_JoinButton.Location = new System.Drawing.Point(3, 507);
            this.ServerPanel_JoinButton.Name = "ServerPanel_JoinButton";
            this.ServerPanel_JoinButton.Size = new System.Drawing.Size(253, 28);
            this.ServerPanel_JoinButton.TabIndex = 2;
            this.ServerPanel_JoinButton.Text = "Вступить в игру";
            this.ServerPanel_JoinButton.UseVisualStyleBackColor = true;
            this.ServerPanel_JoinButton.Click += new System.EventHandler(this.ServerPanel_JoinButton_Click);
            // 
            // ServerPanel_Close
            // 
            this.ServerPanel_Close.Image = global::Simple_RU24.Properties.Resources.close_button_simple;
            this.ServerPanel_Close.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServerPanel_Close.Location = new System.Drawing.Point(213, 22);
            this.ServerPanel_Close.Name = "ServerPanel_Close";
            this.ServerPanel_Close.Size = new System.Drawing.Size(34, 33);
            this.ServerPanel_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ServerPanel_Close.TabIndex = 1;
            this.ServerPanel_Close.TabStop = false;
            this.ServerPanel_Close.Click += new System.EventHandler(this.ServerPanel_Close_Click);
            // 
            // ServerPanel_Header
            // 
            this.ServerPanel_Header.AutoSize = true;
            this.ServerPanel_Header.Font = new System.Drawing.Font("Segoe UI Light", 14.25F);
            this.ServerPanel_Header.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServerPanel_Header.Location = new System.Drawing.Point(9, 25);
            this.ServerPanel_Header.Name = "ServerPanel_Header";
            this.ServerPanel_Header.Size = new System.Drawing.Size(75, 25);
            this.ServerPanel_Header.TabIndex = 0;
            this.ServerPanel_Header.Text = "Сервер";
            // 
            // ServerImages
            // 
            this.ServerImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ServerImages.ImageStream")));
            this.ServerImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ServerImages.Images.SetKeyName(0, "server.png");
            this.ServerImages.Images.SetKeyName(1, "server_add.png");
            this.ServerImages.Images.SetKeyName(2, "server_chart.png");
            this.ServerImages.Images.SetKeyName(3, "server_connect.png");
            this.ServerImages.Images.SetKeyName(4, "server_database.png");
            this.ServerImages.Images.SetKeyName(5, "server_delete.png");
            this.ServerImages.Images.SetKeyName(6, "server_edit.png");
            this.ServerImages.Images.SetKeyName(7, "server_error.png");
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1052, 547);
            this.Controls.Add(this.ServerPanel);
            this.Controls.Add(this.tabControl1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RU24 MinimalismaLauncher";
            this.TransparencyKey = System.Drawing.Color.LavenderBlush;
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.MainWindow_ControlAdded);
            this.tabControl1.ResumeLayout(false);
            this.homePage.ResumeLayout(false);
            this.homePage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.serversPage.ResumeLayout(false);
            this.serversPage.PerformLayout();
            this.servStrip.ResumeLayout(false);
            this.servStrip.PerformLayout();
            this.serverSearch_Panel.ResumeLayout(false);
            this.serverSearch_Panel.PerformLayout();
            this.modsPage.ResumeLayout(false);
            this.modsPage.PerformLayout();
            this.modsWarning_Panel.ResumeLayout(false);
            this.modsWarning_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modsWarning_Image)).EndInit();
            this.musicPage.ResumeLayout(false);
            this.musicPage.PerformLayout();
            this.ServerPanel.ResumeLayout(false);
            this.ServerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ServerPanel_Close)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage homePage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage serversPage;
        private System.Windows.Forms.TabPage modsPage;
        private System.Windows.Forms.TabPage musicPage;
        private System.Windows.Forms.Label welcomeLabel1;
        private System.Windows.Forms.Label welcomeLabel2;
        private System.Windows.Forms.GroupBox serverSearch_Panel;
        private System.Windows.Forms.Button serverSearch_Button;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox srvFilter_Empty;
        private System.Windows.Forms.CheckBox srvFilter_Full;
        private System.Windows.Forms.CheckBox srvFilter_NotTrusted;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip servStrip;
        private System.Windows.Forms.ToolStripStatusLabel sbar_lab;
        private System.Windows.Forms.ToolStripProgressBar sbar_progbar;
        private System.Windows.Forms.CheckBox srvFilter_Modifications;
        private System.Windows.Forms.CheckBox srvFilter_NotOfficial;
        private System.Windows.Forms.ProgressBar modsPage_LoadBar;
        private System.ComponentModel.BackgroundWorker ServersUpdater;
        private System.Windows.Forms.ProgressBar serverSearch_PBAR;
        private System.Windows.Forms.Panel ServerPanel;
        private System.Windows.Forms.PictureBox ServerPanel_Close;
        private System.Windows.Forms.Label ServerPanel_Header;
        private System.Windows.Forms.ListBox ServerPanel_PlayersListBox;
        private System.Windows.Forms.Label ServerPanel_NowPlaying;
        private System.Windows.Forms.Label ServerPanel_MaxPlayersCounter;
        private System.Windows.Forms.Label ServerPanel_NameOfServer;
        private System.Windows.Forms.Label ServerPanel_InnerContrent;
        private System.Windows.Forms.Button ServerPanel_JoinButton;
        private System.Windows.Forms.Panel modsWarning_Panel;
        private System.Windows.Forms.PictureBox modsWarning_Image;
        private System.Windows.Forms.Label modsWarning_Text;
        private System.Windows.Forms.Label modsWarning_Title;
        private System.Windows.Forms.Button modsWarning_Button;
        private System.Windows.Forms.ImageList TabsImages;
        private System.Windows.Forms.ImageList ServerImages;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label3;
        private CommandLink commandLink1;
        private System.Windows.Forms.Label modsPage_LoadLabel;
        private Controls.ServersGrid serversGrid1;
        private Controls.ModsGrid modsGrid1;
    }
}