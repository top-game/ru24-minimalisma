﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Simple_RU24
{
    public partial class TOPGAME_AUTH : Form
    {
        public TOPGAME_AUTH()
        {
            InitializeComponent();
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "somelog@#in@ru2##4.com" && textBox1.Enabled)
            {
                textBox1.Text = "";
                Transition t = new Transition(new TransitionType_EaseInEaseOut(500));
                t.add(label2, "Left", textBox1.Left);
                t.add(label2, "Top", textBox1.Top - label2.Height - 2);
                t.add(label2, "ForeColor", Color.Black);
                t.add(textBox1, "ForeColor", Color.Black);
                t.TransitionCompletedEvent += delegate
                {
                    label2.Left = textBox1.Left;
                    label2.Top = textBox1.Top - label2.Height - 2;
                    label2.ForeColor = Color.Black;
                    textBox1.ForeColor = Color.Black;
                };
                t.run();
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "s@omepassword!" && textBox2.Enabled)
            {
                textBox2.Text = "";
                Transition t = new Transition(new TransitionType_EaseInEaseOut(500));
                t.add(label3, "Left", textBox2.Left);
                t.add(label3, "Top", textBox2.Top - label2.Height - 2);
                t.add(label3, "ForeColor", Color.Black);
                t.add(textBox2, "ForeColor", Color.Black);
                t.TransitionCompletedEvent += delegate
                {
                    label3.Left = textBox2.Left;
                    label3.Top = textBox2.Top - label3.Height - 2;
                    label3.ForeColor = Color.Black;
                    textBox2.ForeColor = Color.Black;
                };
                t.run();
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                Transition t = new Transition(new TransitionType_EaseInEaseOut(500));
                t.add(label3, "Left", textBox2.Left + 2);
                t.add(label3, "Top", textBox2.Top + 1);
                t.add(label3, "ForeColor", Color.Gray);
                t.add(textBox2, "ForeColor", Color.Black);
                t.TransitionCompletedEvent += delegate
                {
                    textBox2.Text = "s@omepassword!";
                    textBox2.ForeColor = Color.Transparent;
                };
                t.run();
            }

        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                Transition t = new Transition(new TransitionType_EaseInEaseOut(500));
                t.add(label2, "Left", textBox1.Left + 2);
                t.add(label2, "Top", textBox1.Top + 1);
                t.add(label2, "ForeColor", Color.Gray);
                t.add(textBox1, "ForeColor", Color.Black);
                t.TransitionCompletedEvent += delegate
                {
                    textBox1.Text = "somelog@#in@ru2##4.com";
                    textBox1.ForeColor = Color.Transparent;
                };
                t.run();
                
                
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            textBox1.Focus();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            textBox2.Focus();
        }

        private void label4_TextChanged(object sender, EventArgs e)
        {
            label4.Left = pictureBox2.Left + pictureBox2.Top + 1;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            panel1.Visible = true;
            button1.Enabled = false;
            button2.Enabled = false;
            await Task.Factory.StartNew(() => Thread.Sleep(6000));
            panel1.Visible = false;
            this.Hide();
            Engine.Run(Engine.MainWindow);
        }
        //202
    }
}
