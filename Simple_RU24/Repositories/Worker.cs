﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RU24Lib.Types;
using System;
using System.Net;
using System.Xml;

namespace Simple_RU24.Repositories
{
    /// <summary>
    /// Представляет класс работы с репозиториями
    /// </summary>
    public class RepositoryWorker
    {
        public static ArmA2Server[] GetFrom(RU24Lib.Types.ArmA2ServerRepository repository)
        {
            try
            {
                var obj = new ArmA2ServerRepository();
                // XML
                if (repository.Info.LoadType == BaseRepositoryLoadType.XML)
                {
                    var xml = new XmlDocument();
                    xml.Load(repository.Info.LoadUri.OriginalString);
                    var json = JsonConvert.SerializeXmlNode(xml);
                    if (repository.Info.Node != null)
                    {
                        var qjson = JObject.Parse(json);
                        var inner = qjson[repository.Info.Node].ToString();
                        obj = JsonConvert.DeserializeObject<ArmA2ServerRepository>(inner);
                    }
                    else
                    {
                        obj = JsonConvert.DeserializeObject<ArmA2ServerRepository>(json);
                    }
                }
                // JSON
                else if (repository.Info.LoadType == BaseRepositoryLoadType.JSON)
                {
                    var str = new WebClient().DownloadString(repository.Info.LoadUri);
                    if (repository.Info.Node != null)
                    {
                        var qjson = JObject.Parse(str);
                        var inner = qjson[repository.Info.Node].ToString();
                        obj = JsonConvert.DeserializeObject<ArmA2ServerRepository>(inner);
                    }
                    else
                    {
                        obj = JsonConvert.DeserializeObject<ArmA2ServerRepository>(str);
                    }
                }

                // Если все отлично, вернет сервера.
                return obj.Servers;
            }
            catch (Exception)
            {
                // А если произошло исключение вернет нулевую ссылку.
                return null;
            }
        }
        public static ArmA2Modification[] GetFrom(RU24Lib.Types.ArmA2ModificationsRepository repository)
        {
            try
            {
                var obj = new ArmA2ModificationsRepository();
                // XML
                if (repository.Info.LoadType == BaseRepositoryLoadType.XML)
                {
                    var xml = new XmlDocument();
                    xml.Load(repository.Info.LoadUri.OriginalString);
                    var json = JsonConvert.SerializeXmlNode(xml);
                    if (repository.Info.Node != null)
                    {
                        var qjson = JObject.Parse(json);
                        var inner = qjson[repository.Info.Node].ToString();
                        obj = JsonConvert.DeserializeObject<ArmA2ModificationsRepository>(inner);
                    }
                    else
                    {
                        obj = JsonConvert.DeserializeObject<ArmA2ModificationsRepository>(json);
                    }
                }
                // JSON
                else if (repository.Info.LoadType == BaseRepositoryLoadType.JSON)
                {
                    var str = new WebClient().DownloadString(repository.Info.LoadUri);
                    if (repository.Info.Node != null)
                    {
                        var qjson = JObject.Parse(str);
                        var inner = qjson[repository.Info.Node].ToString();
                        obj = JsonConvert.DeserializeObject<ArmA2ModificationsRepository>(inner);
                    }
                    else
                    {
                        obj = JsonConvert.DeserializeObject<ArmA2ModificationsRepository>(str);
                    }
                }

                // Если все отлично, вернет модификации.
                return obj.Modifications;
            }
            catch (Exception)
            {
                // А если произошло исключение вернет нулевую ссылку.
                return null;
            }
        }
    }
}