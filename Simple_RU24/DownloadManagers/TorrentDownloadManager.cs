﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoTorrent.Client;
using MonoTorrent.Common;
using System.Net;

namespace Simple_RU24.DownloadManagers
{
    /// <summary>
    /// Менеджер загрузки торрент-файлов
    /// </summary>
    public class TorrentDownloadManager
    {
        public TorrentDownloadManager()
        {
            var es = new EngineSettings() // Settings
            {
                SavePath = AllSettings.LauncherSettings.Default.TorrentSavePath, // Torrent save path
                GlobalMaxDownloadSpeed = AllSettings.LauncherSettings.Default.LimitDownloadSpeed, // Speed limit
                GlobalMaxUploadSpeed = AllSettings.LauncherSettings.Default.LimitUploadSpeed, // Upload speed limit
                AllowedEncryption = MonoTorrent.Client.Encryption.EncryptionTypes.All, // Encryption
            };
            _TorrentClientEngine = new ClientEngine(es); // New Torrent Egnine
        }
        public TorrentDownloadManager(string SavePath)
        {
            if (SavePath == null) { throw new ArgumentNullException(); } else if (SavePath == String.Empty) { throw new ArgumentException("empty path"); } // Check
            if (!System.IO.Directory.Exists(SavePath)) { // If directory not exists
                Console.WriteLine("[TDM] Directory not exists: " + SavePath); 
                try { System.IO.Directory.CreateDirectory(SavePath); } // Create dir
                catch (Exception exc) { Console.WriteLine("[TDM] [Exception] Can't create directory"); throw exc;  } // Throw exception if can't create dir
            }
            var es = new EngineSettings() // Settings
            {
                SavePath = SavePath, // Torrent save path
                AllowedEncryption = MonoTorrent.Client.Encryption.EncryptionTypes.All // Encryption
            };

            if (AllSettings.LauncherSettings.Default.LimitDownloadSpeed != 0)
            {
                es.GlobalMaxDownloadSpeed = AllSettings.LauncherSettings.Default.LimitDownloadSpeed; // Speed limit
            }
            if (AllSettings.LauncherSettings.Default.LimitUploadSpeed != 0)
            {
                es.GlobalMaxUploadSpeed = AllSettings.LauncherSettings.Default.LimitUploadSpeed; // Upload speed limit
            }

            _TorrentClientEngine = new ClientEngine(es); // New Torrent Engine
        }


        string _savePath = AllSettings.LauncherSettings.Default.TorrentSavePath;
        public string SavePath
        {
            set
            {
                if (value == null) { throw new ArgumentNullException(); } else if (value == String.Empty) { throw new ArgumentException("empty path"); } // Check
                if (!System.IO.Directory.Exists(value))
                { // If directory not exists
                    Console.WriteLine("[TDM] Directory not exists: " + value);
                    try { System.IO.Directory.CreateDirectory(value); } // Create dir
                    catch (Exception exc) { Console.WriteLine("[TDM] [Exception] Can't create directory"); throw exc; } // Throw exception if can't create dir
                }
                // If all OK, set:
                _savePath = value; // local variable
                _TorrentClientEngine.Settings.SavePath = value; // Torrent Client Engine path to save
            }
            get // On get, we return _savePath
            {
                return _savePath;
            }
        }
        ClientEngine _TorrentClientEngine { get; set; }

        /// <summary>
        /// Загружает Torrent файл
        /// </summary>
        /// <param name="TorrentPath">Путь к Torrent файлу</param>
        /// <param name="AutoStart">Начинать загрузку сразу</param>
        /// <returns></returns>
        public TorrentManager DownloadTorrent(string TorrentPath, bool AutoStart = true)
        {
            var tf = Torrent.Load(TorrentPath); // Load torrent file

            var torrentSettings = new TorrentSettings(); // Create new settings

            if(AllSettings.LauncherSettings.Default.LimitDownloadSpeed != 0){
                torrentSettings.MaxDownloadSpeed = AllSettings.LauncherSettings.Default.LimitDownloadSpeed; // If speed limited
            }
            if(AllSettings.LauncherSettings.Default.LimitUploadSpeed != 0){
                torrentSettings.MaxUploadSpeed = AllSettings.LauncherSettings.Default.LimitUploadSpeed; // If speed limited
            }
            torrentSettings.UseDht = true;
            //_TorrentClientEngine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, 6969));

            var tm = new TorrentManager(tf, _savePath, torrentSettings);

           
            _TorrentClientEngine.Register(tm); // Register torrent
            if (AutoStart) tm.Start(); _TorrentClientEngine.StartAll();

            return tm; // Return TorrentManager
        }


    }


}
