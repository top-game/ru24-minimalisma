﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_RU24.DevelopersInfos
{
    /// <summary>
    /// Переменные
    /// </summary>
    public class Variables
    {
        /// <summary>
        /// Пути к папкам
        /// </summary>
        public class Paths
        {
            /// <summary>
            /// Путь сохранения кэша (папка будет очищена по завершению работы лаунчера)
            /// </summary>
            public static string CachePath {
                get
                {
                    return AllSettings.LauncherSettings.Default.CacheFolder;
                }
            }

            /// <summary>
            /// Путь, куда следует сохранять файлы торрентов
            /// </summary>
            public static string TorrentDownloadsPath
            {
                get
                {
                    return AllSettings.LauncherSettings.Default.TorrentSavePath;
                }
            }
        }
    }
}
