using System;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Simple_RU24.LogSystem
{
    public class LogTextWriter : TextWriter
    {
        StreamWriter _sw;

        public LogTextWriter(StreamWriter sw)
        {
            this._sw = sw;
            this._sw.AutoFlush = true;
        }

        public override void Write(char value)
        {
            base.Write(value);
            _sw.Write(value.ToString()); 
        }

        public override Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }
    }
}