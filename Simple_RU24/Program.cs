﻿using System;

namespace Simple_RU24
{
    internal static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            // Creating log
#if (!DEBUG)
            Console.SetOut(new LogSystem.LogTextWriter(CreateLog()));
#endif
            Console.WriteLine("RU24 - Program Started.");
            Console.WriteLine("Requesting to engine...");
            // Loading Engine
            Engine main = new Engine();
            main.Main(args);
        }

        private static string GetLogString()
        {
            return Environment.CurrentDirectory + "\\logs\\" + DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second + DateTime.Now.Millisecond + "_" + DateTime.Now.GetHashCode() + "_log.txt";
        }

        private static System.IO.StreamWriter CreateLog()
        {
            if (!System.IO.Directory.Exists(Environment.CurrentDirectory + "\\logs\\"))
            {
                System.IO.Directory.CreateDirectory(Environment.CurrentDirectory + "\\logs\\");
            }

            var logname = GetLogString();
            System.IO.File.Create(logname).Close();

            return new System.IO.StreamWriter(logname);
        }

    }
}