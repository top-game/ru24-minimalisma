﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using RU24Lib.Types;
using System.Xml;

namespace Simple_RU24.Forms
{
    public partial class Repositories_Edit : Form
    {
        public Repositories_Edit()
        {
            InitializeComponent();
        }

        public BaseRepository Show()
        {
            throw new Exception("wtf???");
            return null;
        }
        
        public BaseRepository Show(BaseRepository repository)
        {
            this.textBox1.Text = repository.Info.LoadUri.AbsoluteUri;
            this.comboBox1.SelectedIndex = repository.Info.LoadType == BaseRepositoryLoadType.JSON ? 0 : 1;
            this.textBox2.Text = repository.Info.Node;


            base.ShowDialog();

           
            
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            infoLabel.Visible = true;
            TryToLoad();
        }

        private BaseRepository result;

        private string resp = "";

        private void TryToLoad()
        {
            string current_str = "";
            try
            {
                if (comboBox1.SelectedIndex == 1)
                {
                    // Получаем XML документ
                    current_str = "Ошибка получения [X1Q1]";
                    XmlDocument Xml = new XmlDocument();
                    Xml.Load(textBox1.Text);
                    // Парсим его как JSON
                    current_str = "Ошибка парсинга [XXQ1]";
                    resp = Xml.InnerXml;
                    var str = JsonConvert.SerializeXmlNode(Xml);
                    if (textBox2.Text != "")
                    {
                        current_str = "Ошибка парсинга [XXQ2]";
                        // Если ветка задана парсим её
                        var json = JObject.Parse(str);
                        current_str = "Ошибка ветки [XXQ3]";
                        // Вход в ветку
                        var inner = json[textBox2.Text].ToString();
                        current_str = "Ошибка создания виртуального репозитория [XZQ4]";
                        // Обработка ветки как базовый репозиторий
                        result = JsonConvert.DeserializeObject<BaseRepository>(inner);
                    }
                    else
                    {
                        current_str = "Ошибка создания виртуального репозитория [XZQ5]";
                        // Просто обрабатываем
                        result = JsonConvert.DeserializeObject<BaseRepository>(str);
                    }
                }
                else if (comboBox1.SelectedIndex == 0)
                {
                    current_str = "Ошибка получения [X2Q1]";
                    // Получаем JSON
                    var str = (new WebClient()).DownloadString(textBox1.Text);
                    resp = str;
                    current_str = "Ошибка парсинга [XXQ2B]";
                    // Если ветвь задана:
                    if (textBox2.Text != "")
                    {
                        // Парсим все ветки
                        var json = JObject.Parse(str);
                        current_str = "Ошибка ветки [XXQ3B]";
                        // Вход в ветку
                        var inner = json[textBox2.Text].ToString();
                        current_str = "Ошибка создания виртуального репозитория [XZQ4B]";
                        // Обработка ветки как базовый репозиторий
                        result = JsonConvert.DeserializeObject<BaseRepository>(inner);
                    }
                    else
                    {
                        current_str = "Ошибка создания виртуального репозитория [XZQ5BA]";
                        // Также, просто обрабатываем
                        result = JsonConvert.DeserializeObject<BaseRepository>(str);
                    }
                }
                else
                {
                    infoLabel.Visible = true;
                    infoLabel.Text = "Неверно: формат [XXXQ0]";
                    return;
                }
                // Спрашиваем нашего пользователя
                var add_result = MessageBox.Show("Подтвердите добавление следующего репозитория:\nНазвание -\t" + result.Name + "\nВерсия -\t" + result.Version + "\nЕсли это то, что нужно нажмите кнопку \"Да\", иначе нажмите \"Нет\".\n\nНекоторые репозитории могут быть опасны! Не доверяйте непровренным репозиториям.", "Информация о репозитории", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (add_result == System.Windows.Forms.DialogResult.Yes)
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    return;
                }
                result.Info = new BaseRepositoryInfo();
                result.Info.LoadUri = new Uri(textBox1.Text);
                result.Info.Node = textBox2.Text.Length == 0 ? null : textBox2.Text;
                result.Info.LoadType = comboBox1.SelectedIndex == 0 ? BaseRepositoryLoadType.JSON : BaseRepositoryLoadType.XML;

                Console.WriteLine(result.Info.LoadUri);

                this.Close();
            }
            catch
            {
                infoLabel.Text = current_str;
                infoLabel.ForeColor = Color.OrangeRed;
                infoLabel.Visible = true;
            }
        }

        private void Repositories_Edit_Load(object sender, EventArgs e)
        {

        }

        private void infoLabel_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Ответ, полученный от репозитория:\n" + resp, "Dev-Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}
