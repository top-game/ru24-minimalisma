﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Simple_RU24.Forms
{
    public partial class SettingsMaster_ArmAII : Controls.SettingsMasterForm
    {
        public SettingsMaster_ArmAII()
        {
            InitializeComponent();
        }

        private void SettingsMaster_ArmAII_Load(object sender, EventArgs e)
        {

        }

        private void BackBtn_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string key = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Bohemia Interactive\\ArmA 2\\", "MAIN", "no");
            if (key == "no" || key == "" || key == null)
            {
                MessageBox.Show("В вашем реестре не существует записи MAIN, которая содержит путь к папке с ArmA 2. \n\nERR_A2MNF", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var req = MessageBox.Show(key + "\nЭтот путь является директорией ArmA 2?\nНа некоторых серверах не рекомендуется играть с лицензионной версией игры!", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (req == DialogResult.Yes)
                {
                    textBox1.Text = key;
                }
                else
                {
                    return;
                }
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(!System.IO.Directory.Exists(textBox1.Text))
            {
                MessageBox.Show("Директория не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            var req = MessageBox.Show("Если путь является неверным игра может перестать запускаться, также могут возникнуть некоторые проблемы со Steam.\n\nВы уверены что хотите сохранить/заменить путь к ArmA 2 в реестре?", "Подтверждение действия", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if(req == DialogResult.OK)
            {
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Bohemia Interactive\\ArmA 2\\", "MAIN", textBox1.Text);
            }
        }
    }
}
