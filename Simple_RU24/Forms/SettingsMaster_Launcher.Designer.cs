﻿namespace Simple_RU24.Forms
{
    partial class SettingsMaster_Launcher
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.LimitSpeedCheckBox = new System.Windows.Forms.CheckBox();
            this.LimitSpeedDownload = new System.Windows.Forms.NumericUpDown();
            this.LimitSpeedUpload = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ModsFolder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CacheFolder = new System.Windows.Forms.TextBox();
            this.BrowseButton1 = new System.Windows.Forms.Button();
            this.BrowseButton2 = new System.Windows.Forms.Button();
            this.SettingsSavedLabel = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.LimitSpeedDownload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LimitSpeedUpload)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(343, 344);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(283, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Сохранить настройки и бережно заботится о них";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LimitSpeedCheckBox
            // 
            this.LimitSpeedCheckBox.AutoSize = true;
            this.LimitSpeedCheckBox.Location = new System.Drawing.Point(15, 96);
            this.LimitSpeedCheckBox.Name = "LimitSpeedCheckBox";
            this.LimitSpeedCheckBox.Size = new System.Drawing.Size(283, 17);
            this.LimitSpeedCheckBox.TabIndex = 1;
            this.LimitSpeedCheckBox.Text = "Лимитировать скорость загрузки торрент-файлов";
            this.LimitSpeedCheckBox.UseVisualStyleBackColor = true;
            this.LimitSpeedCheckBox.CheckedChanged += new System.EventHandler(this.LimitSpeedCheckBox_CheckedChanged);
            // 
            // LimitSpeedDownload
            // 
            this.LimitSpeedDownload.Enabled = false;
            this.LimitSpeedDownload.Location = new System.Drawing.Point(178, 119);
            this.LimitSpeedDownload.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.LimitSpeedDownload.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LimitSpeedDownload.Name = "LimitSpeedDownload";
            this.LimitSpeedDownload.Size = new System.Drawing.Size(120, 20);
            this.LimitSpeedDownload.TabIndex = 2;
            this.LimitSpeedDownload.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LimitSpeedUpload
            // 
            this.LimitSpeedUpload.Enabled = false;
            this.LimitSpeedUpload.Location = new System.Drawing.Point(178, 145);
            this.LimitSpeedUpload.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.LimitSpeedUpload.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LimitSpeedUpload.Name = "LimitSpeedUpload";
            this.LimitSpeedUpload.Size = new System.Drawing.Size(120, 20);
            this.LimitSpeedUpload.TabIndex = 3;
            this.LimitSpeedUpload.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Скорость загрузки:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Скорость отдачи:";
            // 
            // ModsFolder
            // 
            this.ModsFolder.Location = new System.Drawing.Point(15, 69);
            this.ModsFolder.Name = "ModsFolder";
            this.ModsFolder.Size = new System.Drawing.Size(283, 20);
            this.ModsFolder.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Моды загружаются в папку:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Кэш загружается в папку:";
            // 
            // CacheFolder
            // 
            this.CacheFolder.Location = new System.Drawing.Point(18, 193);
            this.CacheFolder.Name = "CacheFolder";
            this.CacheFolder.Size = new System.Drawing.Size(280, 20);
            this.CacheFolder.TabIndex = 9;
            // 
            // BrowseButton1
            // 
            this.BrowseButton1.Location = new System.Drawing.Point(299, 69);
            this.BrowseButton1.Name = "BrowseButton1";
            this.BrowseButton1.Size = new System.Drawing.Size(35, 21);
            this.BrowseButton1.TabIndex = 10;
            this.BrowseButton1.Text = "...";
            this.BrowseButton1.UseVisualStyleBackColor = true;
            this.BrowseButton1.Click += new System.EventHandler(this.BrowseButton1_Click);
            // 
            // BrowseButton2
            // 
            this.BrowseButton2.Location = new System.Drawing.Point(299, 193);
            this.BrowseButton2.Name = "BrowseButton2";
            this.BrowseButton2.Size = new System.Drawing.Size(35, 21);
            this.BrowseButton2.TabIndex = 11;
            this.BrowseButton2.Text = "...";
            this.BrowseButton2.UseVisualStyleBackColor = true;
            this.BrowseButton2.Click += new System.EventHandler(this.BrowseButton2_Click);
            // 
            // SettingsSavedLabel
            // 
            this.SettingsSavedLabel.AutoSize = true;
            this.SettingsSavedLabel.BackColor = System.Drawing.Color.Transparent;
            this.SettingsSavedLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.SettingsSavedLabel.Location = new System.Drawing.Point(217, 349);
            this.SettingsSavedLabel.Name = "SettingsSavedLabel";
            this.SettingsSavedLabel.Size = new System.Drawing.Size(120, 13);
            this.SettingsSavedLabel.TabIndex = 12;
            this.SettingsSavedLabel.Text = "Настройки сохранены";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(18, 220);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(227, 17);
            this.checkBox1.TabIndex = 13;
            this.checkBox1.Text = "Торрент-файлы тоже загружатся в кэш";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(299, 242);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(35, 21);
            this.button2.TabIndex = 15;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(18, 242);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(280, 20);
            this.textBox1.TabIndex = 14;
            // 
            // SettingsMaster_Launcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.SettingsSavedLabel);
            this.Controls.Add(this.BrowseButton2);
            this.Controls.Add(this.BrowseButton1);
            this.Controls.Add(this.CacheFolder);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ModsFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LimitSpeedUpload);
            this.Controls.Add(this.LimitSpeedDownload);
            this.Controls.Add(this.LimitSpeedCheckBox);
            this.Controls.Add(this.button1);
            this.Name = "SettingsMaster_Launcher";
            this.Load += new System.EventHandler(this.SettingsMaster_Launcher_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LimitSpeedDownload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LimitSpeedUpload)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox LimitSpeedCheckBox;
        private System.Windows.Forms.NumericUpDown LimitSpeedDownload;
        private System.Windows.Forms.NumericUpDown LimitSpeedUpload;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ModsFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CacheFolder;
        private System.Windows.Forms.Button BrowseButton1;
        private System.Windows.Forms.Button BrowseButton2;
        private System.Windows.Forms.Label SettingsSavedLabel;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
    }
}
