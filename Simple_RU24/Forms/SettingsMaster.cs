﻿#if (!DEBUG)
#define RELEASE
#endif

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Simple_RU24.Forms
{
    /// <summary>
    /// Представляет форму настроек
    /// </summary>
#if (RELEASE)
    public partial class SettingsMaster : Windows.AeroWindow
#else

    public partial class SettingsMaster : System.Windows.Forms.Form
#endif
    {
        public SettingsMaster()
        {
            InitializeComponent();
            this.backBtn.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingsMaster_Load(object sender, EventArgs e)
        {
            ChangePage(new SettingsMaster_MainPage(), true);
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private async void commandLink1_Click_1(object sender, EventArgs e)
        {
            await ChangePage(new SettingsMaster_ArmAII());
        }

        /// <summary>
        /// Изменяет текущую страницу мастера настройки
        /// </summary>
        /// <param name="page">Страница</param>
        /// <param name="ds">Истинно, если следует не запоминать страницу</param>
        /// <param name="f">Истинно, если это следует делать принудительно</param>
        /// <returns></returns>
        public async Task<bool> ChangePage(Controls.SettingsMasterForm page, bool ds = false, bool f = false)
        {
            if (currentpage != null)
            {
                var e = new Controls.SettingsMasterForm.OnPageClosingEventArgs(Simple_RU24.Controls.SettingsMasterForm.OnPageClosingEventArgs.ClosingReason.UserChangePage);
                currentpage.PageClosing(e);
                if (e.Cancel)
                {
                    if (!f)
                    {
                        return false;
                    }
                }
            }
            if (ds == false && currentpage != null)
            {
                oldnesspages.Add(currentpage);
                backBtn.Enabled = true;
                nextpage = null;
            }
            else if (ds == true)
            {
                nextpage = currentpage;
                nextBtn.Enabled = true;
            }
            currentpage = page;

            

            page.Left = page.Width;
            page.Top = 0;

            List<Control> oldnesscontrols = new List<Control>();
            var tr = new Transition(new TransitionType_EaseInEaseOut(1000));
            foreach (Control cont in panel1.Controls)
            {
                tr.add(cont, "Left", 0 - cont.Width - 200);
            }
            tr.run();

            await Task.Factory.StartNew(() => System.Threading.Thread.Sleep(300));

            this.Text = page.Title;
            this.panel1.Controls.Add(page);
            var t = new Transition(new TransitionType_EaseInEaseOut(1000));
            t.add(page, "Left", 0);
            t.add(page, "Top", 0);
            t.add(this, "Left", this.Left - (page.Width - panel1.Width));
            t.add(this, "Top", this.Top - (page.Height - panel1.Height));
            t.add(panel1, "Width", page.Width);
            t.add(panel1, "Height", page.Height);
            t.add(this, "Width", this.Width + (page.Width - panel1.Width));
            t.add(this, "Height", this.Height + (page.Height - panel1.Height));

            t.run();
            t.TransitionCompletedEvent += delegate
            {
                foreach (var cont in oldnesscontrols)
                {
                    panel1.Controls.Remove(cont);
                }
            };

            page.Parent = this;

            if (page.NextPage != null) { nextBtn.Enabled = true; nextpage = page.NextPage; }
            else if (this.nextpage == null) { this.nextBtn.Enabled = false; }
            else { this.nextBtn.Enabled = true; }

            return true;
        }

        public async void Back()
        {
            if (oldnesspages.Count == 0)
            {
                //Console.WriteLine("oldnesspages is empty!");
                backBtn.Enabled = false;
                return;
            }

            var page = oldnesspages[oldnesspages.Count - 1];


            if (!await ChangePage(page, true)) {
                return;
            }

            oldnesspages.Remove(oldnesspages[oldnesspages.Count - 1]);
            panel1.Controls.Add(page);

            if (oldnesspages.Count == 0) { backBtn.Enabled = false; }
        }

        private Controls.SettingsMasterForm nextpage = null;
        private Controls.SettingsMasterForm currentpage = null;
        private List<Controls.SettingsMasterForm> oldnesspages = new List<Controls.SettingsMasterForm>();

        private void button3_Click(object sender, EventArgs e)
        {
            Back();
        }

        private async void nextBtn_Click(object sender, EventArgs e)
        {
            Next();
        }

        public async void Next()
        {
            if (nextpage != null)
            {

                if (!await ChangePage(nextpage))
                {
                    return;
                }
                nextpage = null;
                nextBtn.Enabled = false;
            }
            else
            {
                nextBtn.Enabled = false;
            }
        }

        private void nextBtn_EnabledChanged(object sender, EventArgs e)
        {
            if (nextpage == null) nextBtn.Enabled = false;
            else nextBtn.Enabled = true;
        }
    }
}