﻿using System;
using System.Linq;

namespace Simple_RU24.Forms
{
    public partial class SettingsMaster_Repositories : Controls.SettingsMasterForm
    {
        public SettingsMaster_Repositories()
        {
            InitializeComponent();
        }

        private void SettingsMaster_Repositories_Load(object sender, EventArgs e)
        {
            UpdateList();
        }

        private void UpdateList()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            foreach (var item in Engine.Repositories)
            {
                listBox1.Items.Add(item.Name);
            }
            foreach (var item in Engine.ModsRepositories)
            {
                listBox2.Items.Add(item.Name);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var rep_add_form = new Forms.Repositories_Add();
            var item = rep_add_form.Show();

            if (item != null)
            {
                Engine.AddServerRepository(item.ToArmA2ServerRepository());
            }
            UpdateList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var rep_add_form = new Forms.Repositories_Add();
            var item = rep_add_form.Show();

            if (item != null)
            {
                Engine.AddModificationsRepository(item.ToArmA2ModificationsRepository());
            }
            UpdateList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var rep_edit_form = new Forms.Repositories_Edit();
            var item = rep_edit_form.Show(Engine.Repositories[listBox1.SelectedIndex]);

            if (item != null)
            {
                Engine.ModsRepositories[listBox1.SelectedIndex] = item.ToArmA2ModificationsRepository();
            }
            UpdateList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var rep_edit_form = new Forms.Repositories_Edit();
            var item = rep_edit_form.Show(Engine.Repositories[listBox1.SelectedIndex]);

            if (item != null)
            {
                Engine.Repositories[listBox1.SelectedIndex] = item.ToArmA2ServerRepository();
            }
            UpdateList();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Engine.RemoveServerRepository(Engine.Repositories[listBox1.SelectedIndex]);
            UpdateList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Engine.RemoveModificationsRepository(Engine.ModsRepositories[listBox1.SelectedIndex]);
            UpdateList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button4.Enabled = true;
                button5.Enabled = true;
            }
            else
            {
                button4.Enabled = false;
                button5.Enabled = false;
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                button2.Enabled = true;
                button3.Enabled = true;
            }
            else
            {
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }
    }
}
