﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Simple_RU24.Forms
{
    public partial class StartingGame : Windows.AeroWindow
    {
        public StartingGame()
        {
            InitializeComponent();
        }

        public void SetState(int progress, string text, ProgressBarStyle style = ProgressBarStyle.Continuous)
        {
            this.progressBar1.Value = progress;
            this.progressBar1.Style = style;
            this.label1.Text = text;
        }
    }
}
