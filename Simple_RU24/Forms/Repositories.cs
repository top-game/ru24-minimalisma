﻿using System;
using System.Windows.Forms;

namespace Simple_RU24.Forms
{
    public partial class Repositories : Form
    {
        private How wta;

        public Repositories(How WhatToAdd)
        {
            InitializeComponent();
            this.wta = WhatToAdd;


            if (WhatToAdd == How.Mods)
            {
                this.Text = "Управление репозиториями модов";
            }
            else
            {
                this.Text = "Управление репозиториями серверов";
            }

            UpdateList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Repositories_Add repoadd = new Repositories_Add();

            var Repository = repoadd.Show();
            if (Repository != null)
            {
                if (wta == How.Mods)
                {
                    Console.WriteLine("+| " + Repository.Name + ": " + Repository.Info.LoadUri);
                    Engine.AddModificationsRepository(Repository.ToArmA2ModificationsRepository());
                }
                else if (wta == How.Servers)
                {
                    Console.WriteLine("+| " + Repository.Name + ": " + Repository.Info.LoadUri);
                    Engine.AddServerRepository(Repository.ToArmA2ServerRepository());
                }
            }
            else
            {
                return;
            }

            UpdateList();
        }

        private void UpdateList()
        {
            button2.Enabled = false;
            button3.Enabled = false;
            listBox1.Items.Clear();
            if (wta == How.Servers)
            {
                foreach (var repo in Engine.Repositories)
                {
                    listBox1.Items.Add(repo.Name);
                }
            }
            else
            {
                foreach (var repo in Engine.ModsRepositories)
                {
                    listBox1.Items.Add(repo.Name);
                }
            }
        }

        public enum How
        {
            Mods,
            Servers
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button2.Enabled = true;
                button3.Enabled = true;
            }
            else
            {
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (wta == How.Mods)
            {
                var rep = Engine.ModsRepositories[listBox1.SelectedIndex];
                Console.WriteLine("-| " + rep.Name + ":" + rep.Info.LoadUri);
                Engine.RemoveModificationsRepository(rep);
            }
            else if (wta == How.Servers)
            {
                var rep = Engine.Repositories[listBox1.SelectedIndex];
                Console.WriteLine("-| " + rep.Name + ":" + rep.Info.LoadUri);
                Engine.RemoveServerRepository(rep);
            }
            else
            {
                return;
            }

            UpdateList();
        }

        private void Repositories_Load(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //RU24Lib.Types.BaseRepository new_br =
            var repo = Engine.Repositories[listBox1.SelectedIndex];
            if (repo.Info.MustBeProtected)
            {
                MessageBox.Show("Данный репозиторий защищен от редактирования!", "Защита", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            var Edited = (new Repositories_Edit()).Show((new RU24Lib.Types.BaseRepository(repo)));
            if(Edited != null)
            Engine.Repositories[listBox1.SelectedIndex] = Edited.ToArmA2ServerRepository();
            //RU24Lib.Types.ArmA2ServerRepository 

        }
    }
}