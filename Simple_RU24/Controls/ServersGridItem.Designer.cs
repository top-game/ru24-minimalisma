﻿namespace Simple_RU24.Controls
{
    partial class ServersGridItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServerName = new System.Windows.Forms.Label();
            this.playIcon = new System.Windows.Forms.Label();
            this.ServerMotd = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.playersIcon = new System.Windows.Forms.Label();
            this.currentPlayers = new System.Windows.Forms.Label();
            this.maxPlayers = new System.Windows.Forms.Label();
            this.playersLabel = new System.Windows.Forms.Label();
            this.pingLabel = new System.Windows.Forms.Label();
            this.pingIcon = new System.Windows.Forms.Label();
            this.modsLabel = new System.Windows.Forms.Label();
            this.modsIcon = new System.Windows.Forms.Label();
            this.pingComLabel = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.modsLinkLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // ServerName
            // 
            this.ServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ServerName.AutoEllipsis = true;
            this.ServerName.Font = new System.Drawing.Font("Monaco", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServerName.Location = new System.Drawing.Point(3, 3);
            this.ServerName.Name = "ServerName";
            this.ServerName.Size = new System.Drawing.Size(594, 34);
            this.ServerName.TabIndex = 3;
            this.ServerName.Text = "Сервер недоступен";
            // 
            // playIcon
            // 
            this.playIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.playIcon.AutoSize = true;
            this.playIcon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.playIcon.Font = new System.Drawing.Font("Ionicons", 63F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playIcon.ForeColor = System.Drawing.Color.YellowGreen;
            this.playIcon.Location = new System.Drawing.Point(506, 113);
            this.playIcon.Name = "playIcon";
            this.playIcon.Size = new System.Drawing.Size(90, 85);
            this.playIcon.TabIndex = 2;
            this.playIcon.Text = "";
            this.playIcon.Click += new System.EventHandler(this.playIcon_Click);
            // 
            // ServerMotd
            // 
            this.ServerMotd.Location = new System.Drawing.Point(6, 37);
            this.ServerMotd.Name = "ServerMotd";
            this.ServerMotd.Size = new System.Drawing.Size(590, 30);
            this.ServerMotd.TabIndex = 4;
            this.ServerMotd.Text = "Сообщение дня недоступно";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(117, 70);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 19);
            this.progressBar1.TabIndex = 5;
            // 
            // playersIcon
            // 
            this.playersIcon.AutoSize = true;
            this.playersIcon.Font = new System.Drawing.Font("Ionicons", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playersIcon.Location = new System.Drawing.Point(5, 70);
            this.playersIcon.Name = "playersIcon";
            this.playersIcon.Size = new System.Drawing.Size(30, 19);
            this.playersIcon.TabIndex = 6;
            this.playersIcon.Text = " ";
            // 
            // currentPlayers
            // 
            this.currentPlayers.AutoSize = true;
            this.currentPlayers.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentPlayers.Location = new System.Drawing.Point(96, 71);
            this.currentPlayers.Name = "currentPlayers";
            this.currentPlayers.Size = new System.Drawing.Size(15, 17);
            this.currentPlayers.TabIndex = 7;
            this.currentPlayers.Text = "0";
            // 
            // maxPlayers
            // 
            this.maxPlayers.AutoSize = true;
            this.maxPlayers.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxPlayers.Location = new System.Drawing.Point(223, 72);
            this.maxPlayers.Name = "maxPlayers";
            this.maxPlayers.Size = new System.Drawing.Size(29, 17);
            this.maxPlayers.TabIndex = 8;
            this.maxPlayers.Text = "100";
            // 
            // playersLabel
            // 
            this.playersLabel.AutoSize = true;
            this.playersLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playersLabel.Location = new System.Drawing.Point(29, 70);
            this.playersLabel.Name = "playersLabel";
            this.playersLabel.Size = new System.Drawing.Size(58, 19);
            this.playersLabel.TabIndex = 9;
            this.playersLabel.Text = "Игроки:";
            // 
            // pingLabel
            // 
            this.pingLabel.AutoSize = true;
            this.pingLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pingLabel.Location = new System.Drawing.Point(29, 91);
            this.pingLabel.Name = "pingLabel";
            this.pingLabel.Size = new System.Drawing.Size(92, 19);
            this.pingLabel.TabIndex = 11;
            this.pingLabel.Text = "Доступность:";
            // 
            // pingIcon
            // 
            this.pingIcon.AutoSize = true;
            this.pingIcon.Font = new System.Drawing.Font("Ionicons", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pingIcon.Location = new System.Drawing.Point(5, 91);
            this.pingIcon.Name = "pingIcon";
            this.pingIcon.Size = new System.Drawing.Size(26, 19);
            this.pingIcon.TabIndex = 10;
            this.pingIcon.Text = "";
            // 
            // modsLabel
            // 
            this.modsLabel.AutoSize = true;
            this.modsLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modsLabel.Location = new System.Drawing.Point(29, 112);
            this.modsLabel.Name = "modsLabel";
            this.modsLabel.Size = new System.Drawing.Size(105, 19);
            this.modsLabel.TabIndex = 13;
            this.modsLabel.Text = "Модификации:";
            // 
            // modsIcon
            // 
            this.modsIcon.AutoSize = true;
            this.modsIcon.Font = new System.Drawing.Font("Ionicons", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modsIcon.Location = new System.Drawing.Point(5, 112);
            this.modsIcon.Name = "modsIcon";
            this.modsIcon.Size = new System.Drawing.Size(21, 19);
            this.modsIcon.TabIndex = 12;
            this.modsIcon.Text = "";
            // 
            // pingComLabel
            // 
            this.pingComLabel.AutoSize = true;
            this.pingComLabel.Location = new System.Drawing.Point(233, 94);
            this.pingComLabel.Name = "pingComLabel";
            this.pingComLabel.Size = new System.Drawing.Size(29, 13);
            this.pingComLabel.TabIndex = 14;
            this.pingComLabel.Text = "0 ms";
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(127, 91);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(100, 19);
            this.progressBar2.TabIndex = 15;
            // 
            // modsLinkLabel
            // 
            this.modsLinkLabel.AutoSize = true;
            this.modsLinkLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.modsLinkLabel.LinkColor = System.Drawing.Color.Black;
            this.modsLinkLabel.Location = new System.Drawing.Point(133, 112);
            this.modsLinkLabel.Name = "modsLinkLabel";
            this.modsLinkLabel.Size = new System.Drawing.Size(32, 19);
            this.modsLinkLabel.TabIndex = 16;
            this.modsLinkLabel.TabStop = true;
            this.modsLinkLabel.Text = "Нет";
            this.modsLinkLabel.VisitedLinkColor = System.Drawing.Color.Gray;
            // 
            // ServersGridItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.modsLinkLabel);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.pingComLabel);
            this.Controls.Add(this.modsLabel);
            this.Controls.Add(this.modsIcon);
            this.Controls.Add(this.pingLabel);
            this.Controls.Add(this.pingIcon);
            this.Controls.Add(this.playersLabel);
            this.Controls.Add(this.maxPlayers);
            this.Controls.Add(this.currentPlayers);
            this.Controls.Add(this.playersIcon);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ServerMotd);
            this.Controls.Add(this.ServerName);
            this.Controls.Add(this.playIcon);
            this.Name = "ServersGridItem";
            this.Size = new System.Drawing.Size(600, 200);
            this.Load += new System.EventHandler(this.ServersGridItem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ServerName;
        private System.Windows.Forms.Label playIcon;
        private System.Windows.Forms.Label ServerMotd;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label playersIcon;
        private System.Windows.Forms.Label currentPlayers;
        private System.Windows.Forms.Label maxPlayers;
        private System.Windows.Forms.Label playersLabel;
        private System.Windows.Forms.Label pingLabel;
        private System.Windows.Forms.Label pingIcon;
        private System.Windows.Forms.Label modsLabel;
        private System.Windows.Forms.Label modsIcon;
        private System.Windows.Forms.Label pingComLabel;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.LinkLabel modsLinkLabel;
    }
}
