﻿namespace Simple_RU24.Controls
{
    partial class ModsGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.modsTable = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // modsTable
            // 
            this.modsTable.AutoScroll = true;
            this.modsTable.BackColor = System.Drawing.Color.White;
            this.modsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modsTable.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.modsTable.Location = new System.Drawing.Point(0, 0);
            this.modsTable.Name = "modsTable";
            this.modsTable.Size = new System.Drawing.Size(600, 500);
            this.modsTable.TabIndex = 0;
            this.modsTable.WrapContents = false;
            // 
            // ModsGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.modsTable);
            this.Name = "ModsGrid";
            this.Size = new System.Drawing.Size(600, 500);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel modsTable;
    }
}
