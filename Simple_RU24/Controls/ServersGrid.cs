﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Simple_RU24.Controls
{
    public partial class ServersGrid : UserControl
    {
        public ServersGrid()
        {
            InitializeComponent();
            this.Items.ListChanged += Items_ListChanged;
        }

        void Items_ListChanged(object sender, RU24Lib.Types.AdvancedList<ServersGridItem>.ListChangedEventArgs e)
        {
            if (e.Action == RU24Lib.Types.AdvancedList<ServersGridItem>.ListChangedEventArgs.ListChangedAction.AddedNewItems)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    foreach (var item in e.NewItems)
                    {
                        if (!this.ServersTable.Controls.Contains(item))
                        {
                            this.ServersTable.Controls.Add(item);
                        }
                    }
                });
            }
            else if (e.Action == RU24Lib.Types.AdvancedList<ServersGridItem>.ListChangedEventArgs.ListChangedAction.RemovedItems)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    foreach (var item in e.RemovedItems)
                    {
                        if (this.ServersTable.Controls.Contains(item))
                        {
                            this.ServersTable.Controls.Remove(item);
                        }
                    }
                });
            }
        }

        public RU24Lib.Types.AdvancedList<ServersGridItem> Items = new RU24Lib.Types.AdvancedList<ServersGridItem>();
    }
}
