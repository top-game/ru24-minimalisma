﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Simple_RU24.Controls
{
    public partial class ModsGrid : UserControl
    {
        public RU24Lib.Types.AdvancedList<ModsGridItem> Items = new RU24Lib.Types.AdvancedList<ModsGridItem>();

        public ModsGrid()
        {
            InitializeComponent();
            Items.ListChanged += Items_ListChanged;
            Items.ListCleared += Items_ListCleared;
        }

        public ModsGridItem[] Checked
        {
            get
            {
                System.Collections.Generic.List<ModsGridItem> toret = new System.Collections.Generic.List<ModsGridItem>();
                foreach (Control item in modsTable.Controls)
                {
                    try
                    {
                        ModsGridItem mi = (ModsGridItem)item;
                        if (mi.Checked)
                        {
                            toret.Add(mi);
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
                return toret.ToArray();
            }
        }

        void Items_ListCleared(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.modsTable.Controls.Clear();
            });
        }

        void Items_ListChanged(object sender, RU24Lib.Types.AdvancedList<ModsGridItem>.ListChangedEventArgs e)
        {
            if (e.Action == RU24Lib.Types.AdvancedList<ModsGridItem>.ListChangedEventArgs.ListChangedAction.AddedNewItems)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    foreach (var item in e.NewItems)
                    {
                        if (!this.modsTable.Controls.Contains(item))
                        {
                            this.modsTable.Controls.Add(item);
                        }
                    }
                });
            }
            else if (e.Action == RU24Lib.Types.AdvancedList<ModsGridItem>.ListChangedEventArgs.ListChangedAction.RemovedItems)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    foreach (var item in e.RemovedItems)
                    {
                        if (this.modsTable.Controls.Contains(item))
                        {
                            this.modsTable.Controls.Remove(item);
                        }
                    }
                });
            }
        }
    }
}
