﻿namespace Simple_RU24.Controls
{
    partial class ModsGridItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.installIcon = new System.Windows.Forms.Label();
            this.ModName = new System.Windows.Forms.Label();
            this.ModDesc = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.ModImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ModImage)).BeginInit();
            this.SuspendLayout();
            // 
            // installIcon
            // 
            this.installIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.installIcon.AutoSize = true;
            this.installIcon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.installIcon.Font = new System.Drawing.Font("Ionicons", 63F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installIcon.ForeColor = System.Drawing.Color.MediumSlateBlue;
            this.installIcon.Location = new System.Drawing.Point(497, 113);
            this.installIcon.Name = "installIcon";
            this.installIcon.Size = new System.Drawing.Size(100, 85);
            this.installIcon.TabIndex = 0;
            this.installIcon.Text = "";
            this.installIcon.Click += new System.EventHandler(this.installIcon_Click);
            // 
            // ModName
            // 
            this.ModName.AutoEllipsis = true;
            this.ModName.Font = new System.Drawing.Font("Monaco", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModName.Location = new System.Drawing.Point(3, 3);
            this.ModName.Name = "ModName";
            this.ModName.Size = new System.Drawing.Size(594, 34);
            this.ModName.TabIndex = 1;
            this.ModName.Text = "Мод недоступен";
            // 
            // ModDesc
            // 
            this.ModDesc.BackColor = System.Drawing.Color.White;
            this.ModDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ModDesc.Location = new System.Drawing.Point(143, 44);
            this.ModDesc.Multiline = true;
            this.ModDesc.Name = "ModDesc";
            this.ModDesc.ReadOnly = true;
            this.ModDesc.Size = new System.Drawing.Size(342, 128);
            this.ModDesc.TabIndex = 3;
            this.ModDesc.Text = "Описание мода не предоставлено.";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(0, 191);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(600, 10);
            this.progressBar1.TabIndex = 4;
            // 
            // ModImage
            // 
            this.ModImage.Image = global::Simple_RU24.Properties.Resources.ru24_2;
            this.ModImage.Location = new System.Drawing.Point(9, 44);
            this.ModImage.Name = "ModImage";
            this.ModImage.Size = new System.Drawing.Size(128, 128);
            this.ModImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ModImage.TabIndex = 2;
            this.ModImage.TabStop = false;
            // 
            // ModsGridItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ModDesc);
            this.Controls.Add(this.ModImage);
            this.Controls.Add(this.ModName);
            this.Controls.Add(this.installIcon);
            this.Name = "ModsGridItem";
            this.Size = new System.Drawing.Size(600, 200);
            this.Load += new System.EventHandler(this.ModsGridItem_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ModsGridItem_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.ModImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label installIcon;
        private System.Windows.Forms.Label ModName;
        private System.Windows.Forms.PictureBox ModImage;
        private System.Windows.Forms.TextBox ModDesc;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}
