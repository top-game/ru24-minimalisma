﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace Simple_RU24.Controls
{
    public partial class ServersGridItem : UserControl
    {

        RU24Lib.Types.ArmA2Server server = null;

        public ServersGridItem(RU24Lib.Types.ArmA2Server server)
        {
            InitializeComponent();
            foreach (Control item in this.Controls)
            {
                var col = Visuality.Main.GetRandColor();
                if (item.Name.Contains("Icon"))
                {
                    item.ForeColor = col;
                }
            }
            this.ServerName.Text = server.Name;
            if (server.MOTD != null)
            {
                this.ServerMotd.Text = server.MOTD;
            }
            this.server = server;
        }

        public event EventHandler PlayRequest = delegate { };

        private void playIcon_Click(object sender, EventArgs e)
        {
            PlayRequest(this, EventArgs.Empty);
        }

        private void ServersGridItem_Load(object sender, EventArgs e)
        {
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += bg_DoWork;
            bg.RunWorkerAsync();
        }

        void bg_DoWork(object sender, DoWorkEventArgs e)
        {
            Ping p = new Ping();
            var pr = p.Send(server.ConnectInformation.Ip);
            if (pr.Status != IPStatus.Success)
            {
                this.pingComLabel.Text = "Недоступен";
                this.progressBar2.Value = 0;
            }
            else
            {
                this.progressBar2.Value = ((int)pr.RoundtripTime * 100) / 400;
                this.pingComLabel.Text = pr.RoundtripTime + " ms";
            }
            p.Dispose();
        }
    }
}
