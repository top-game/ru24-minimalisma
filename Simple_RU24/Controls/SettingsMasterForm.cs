﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_RU24.Controls
{
    /// <summary>
    /// Класс для построения страницы настроек
    /// </summary>
    public class SettingsMasterForm : UserControl
    {
        /// <summary>
        /// Название страницы (будет отображатся вверху)
        /// </summary>
        [System.ComponentModel.DefaultValue("Настройки")]
        public string Title { get; set; }


        private SettingsMasterForm nextpage = null;
        /// <summary>
        /// Следующая страница
        /// </summary>
        [System.ComponentModel.DefaultValue(null)]
        public SettingsMasterForm NextPage
        {
            get
            {
                return nextpage;
            }
            set
            {
                nextpage = value;
                if (value != null)
                {
                    Parent.nextBtn.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Создает новую страницу настроек
        /// </summary>
        public SettingsMasterForm()
        {
            this.Size = new System.Drawing.Size(629, 370);
        }

        /// <summary>
        /// Родитель объекта, а т.ё. форма настроек
        /// </summary>
        public Forms.SettingsMaster Parent { get; set; }


        /// <summary>
        /// Страница закрывается
        /// </summary>
        /// <param name="e">Ар</param>
        public virtual void PageClosing(OnPageClosingEventArgs e)
        {
            OnPageClosing(Parent, e);
        }

        /// <summary>
        /// Аргуметны закрытия страницы
        /// </summary>
        public class OnPageClosingEventArgs
        {
            public OnPageClosingEventArgs(ClosingReason reason)
            {
                this.Reason = reason ;
            }
            /// <summary>
            /// Указывает на то, следует ли переходить на другую страницу
            /// </summary>
            public bool Cancel { get; set; }
            /// <summary>
            /// Причина ухода со страницы
            /// </summary>
            public ClosingReason Reason { get; private set; }
            
            /// <summary>
            /// Причина выхода
            /// </summary>
            public enum ClosingReason
            {
                /// <summary>
                /// Пользователь переходит на другую страницу
                /// </summary>
                UserChangePage,
                /// <summary>
                /// Мастер закрывается
                /// </summary>
                MasterClosing,
                /// <summary>
                /// Страница изменяется принудительно, в этом случае Cancel ничего не сделает
                /// </summary>
                Forced
            }
        }

        public delegate void OnPageClosingEventHandler(object sender, OnPageClosingEventArgs e);

        /// <summary>
        /// Выполняется, когда страница мастера закрывается
        /// </summary>
        public event OnPageClosingEventHandler OnPageClosing = delegate { };
        
    }
}
