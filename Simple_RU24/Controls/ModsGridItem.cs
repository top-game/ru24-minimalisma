﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Simple_RU24.Controls
{
    public partial class ModsGridItem : UserControl
    {
        RU24Lib.Types.ArmA2Modification _mod = null;
        public ModsGridItem(RU24Lib.Types.ArmA2Modification modification)
        {
            if (modification == null)
            {
                throw new NullReferenceException();
            }
            InitializeComponent();
            this.ModName.Font = new Font("Sertig", 20.25F);
            if (modification.Description != null)
            {
                this.ModDesc.Text = modification.Description;
            }
            this.ModName.Text = modification.Name;
            this.installIcon.ForeColor = Visuality.Main.GetRandColor();
        }

        Bitmap _BluredBitmap = null;

        bool _checked = false;
        public bool Checked
        {
            get
            {
                return _checked;
            }
            set
            {
                _checked = value;
                ApplyStyles();
            }
        }

        private void ModsGridItem_Load(object sender, EventArgs e)
        {
            if (/* AllSettings.ModItemBlurBackGround */ true)
            {
                this.ModImage.LoadCompleted += (o, ev) =>
                {
                    if (this.ModImage.Image != null)
                    {
                        this._BluredBitmap =  new Bitmap(this.ModImage.Image);
                        Visuality.BitmapFilter.GaussianBlur(this._BluredBitmap, 6);
                        this.BackgroundImage = this._BluredBitmap;
                    }
                };
            } 
            this.ModImage.LoadAsync(_mod.PictureUrl);
        }

        public event EventHandler PlayRequest = delegate { };
        public event EventHandler LoadRequest = delegate { };

        bool _selected = false;

        public bool Selected { get { return _selected; } set { _selected = value; ApplyStyles(); } }

        private void ApplyStyles()
        {
            switch (lstate)
            {
                default:
                    {
                        this.installIcon.Text = "";
                        break;
                    }
                case LocalTypes.ModLoadedState.NonLoaded:
                    {
                        this.installIcon.Text = "";
                        break;
                    }
                case LocalTypes.ModLoadedState.Loading:
                    {
                        this.installIcon.Text = "";
                        this.installIcon.Enabled = false;
                        break;
                    }
                case LocalTypes.ModLoadedState.Loaded:
                    {
                        if (this.Checked) 
                        {
                            this.installIcon.Text = "";
                        }
                        else
                        {
                            this.installIcon.Text = "";
                        }
                        break;
                    }
                case LocalTypes.ModLoadedState.Errored:
                    {
                        this.installIcon.Text = "";
                        break;
                    }
            }

            if (_selected || _checked)
            {
                if (_selected) { this.Focus(); }
                this.BackColor = SystemColors.Highlight;
                this.BackgroundImage = null;
            }
            else
            {
                this.BackColor = Color.White;
                if(/* AllSettings.Default.ModItemBlurBackground */ true)
                {
                    this.BackgroundImage = _BluredBitmap;
                }
            }
        }

        LocalTypes.ModLoadedState lstate = LocalTypes.ModLoadedState.NonLoaded;

        public LocalTypes.ModLoadedState LoadedState { 
            get { return lstate; }
            set { lstate = value; ApplyStyles(); }
        }


        private void ModsGridItem_MouseUp(object sender, MouseEventArgs e)
        {
            this.Focus();
            if (this.Focused)
            {
                this.Selected = true;
            }
        }

        private void installIcon_Click(object sender, EventArgs e)
        {
            if (this.LoadedState == LocalTypes.ModLoadedState.Loaded)
            {
                if (this.Checked)
                {
                    this.Checked = false;
                }
                else
                {
                    this.Checked = true;
                }
                //this.PlayRequest(this, EventArgs.Empty);
            }
            else if (this.LoadedState == LocalTypes.ModLoadedState.NonLoaded)
            {
                this.LoadRequest(this, EventArgs.Empty);
            }
            else if (this.LoadedState == LocalTypes.ModLoadedState.Errored)
            {
                this.LoadRequest(this, EventArgs.Empty);
            }
        }

        public void SetProgressStyle(ProgressBarStyle style)
        {
            this.progressBar1.Style = style;
        }

        public void SetProgress(int progress)
        {
            this.progressBar1.Value = progress;
        }
    }
}
