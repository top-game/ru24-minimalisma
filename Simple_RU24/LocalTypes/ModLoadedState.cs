﻿using System;
using System.Linq;

namespace Simple_RU24.LocalTypes
{
    /// <summary>
    /// Состояние загрузки мода
    /// </summary>
    public enum ModLoadedState
    {
        /// <summary>
        /// Загружен
        /// </summary>
        Loaded,
        /// <summary>
        /// Загружается
        /// </summary>
        Loading,
        /// <summary>
        /// Ошибка загрузки или мод поврежден
        /// </summary>
        Errored,
        /// <summary>
        /// Не загружен
        /// </summary>
        NonLoaded
    }
}
