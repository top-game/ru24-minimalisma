﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RU24Lib.Types
{
    public class UserInterfaceLoadController
    {
        /// <summary>
        /// Истинно, если следует заблокировать интерфейс
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        public bool Block { get; set; }

        /// <summary>
        /// Истинно, если следует отображать диалог
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        public bool Dialog { get; set; }

        /// <summary>
        /// Истинно, если стоит показать диалоговое окно с процессом выполнения
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public bool ProgressDialog { get; set; }

        /// <summary>
        /// Текст в диалоговом окне, в процессе выполнения
        /// </summary>
        [System.ComponentModel.DefaultValue("RU24 API LIBARY")]
        public string ProcessDialogCurrentText { get; set; }

        /// <summary>
        /// Текущая операция, которая отображается в диалогом окне
        /// </summary>
        public string ProcessDialogCurrentOperation { get; set; }

        /// <summary>
        /// Заголовок диалогового окна
        /// </summary>
        [System.ComponentModel.DefaultValue("RU24 API LIBARY")]
        public string DialogTitle { get; set; }

        /// <summary>
        /// Текст в диалогом окне
        /// </summary>
        [System.ComponentModel.DefaultValue("RU24 API LIBARY")]
        public string DialogText { get; set; }

        /// <summary>
        /// Текущий процесс выполнения (макс. 100)
        /// </summary>
        [System.ComponentModel.DefaultValue(0)]
        public int ProcessDialogValue { get; set; }

        /// <summary>
        /// Истинно, если следует отображать кнопку отмены, по её нажатию будет произведен void OnCancelButtonClick()
        /// </summary>
        [System.ComponentModel.DefaultValue(0)]
        public bool DialogCancelButton { get; set; }

        /// <summary>
        /// По нажатию на кнопки отмены производится данный void
        /// </summary>
        public virtual void OnCancelButtonClick()
        {
        }
            
    }
}
