namespace RU24Lib.Types
{
    /// <summary>
    /// Тип загрузки из репозитория модификаций
    /// </summary>
    public enum ArmA2ModificationsRepositoryLoadType
    {
        /// <summary>
        /// XML формат
        /// </summary>
        XML,
        /// <summary>
        /// JSON формат
        /// </summary>
        JSON
    }
}