﻿using System;
using System.Linq;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру репозитория модификаций к игре ArmA II
    /// </summary>
    public class ArmA2ModificationsRepository
    {
        /// <summary>
        /// Название репозитория
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Версия репозитория
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Сетевая информация репозитория
        /// </summary>
        public BaseRepositoryInfo Info { get; set; }

        /// <summary>
        /// Модификации (дополнения) из репозитория
        /// </summary>
        public ArmA2Modification[] Modifications { get; set; }
    }
}
