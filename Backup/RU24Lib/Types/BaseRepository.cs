﻿using System;
using System.Linq;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру, содержащую базовые параметры репозитория
    /// </summary>
    public class BaseRepository
    {
        /// <summary>
        /// Создание базового (простого) репозитория
        /// </summary>
        public BaseRepository()
        {

        }

        /// <summary>
        /// Преобразование в базовый репозиторий из репозитория модификаций
        /// </summary>
        /// <param name="repository"></param>
        public BaseRepository(ArmA2ModificationsRepository repository)
        {
            this.Name = repository.Name;
            this.Version = repository.Version;
            this.Info = repository.Info;
        }

        /// <summary>
        /// Преобразование в базовый репозиторий из репозитория серверов
        /// </summary>
        /// <param name="repository"></param>
        public BaseRepository(ArmA2ServerRepository repository)
        {
            this.Name = repository.Name;
            this.Version = repository.Version;
            this.Info = repository.Info;
        }

        /// <summary>
        /// Имя репозитория
        /// </summary>
        /// <example>Simple repository</example>
        public string Name { get; set; }
        /// <summary>
        /// Версия репозитория
        /// </summary>
        /// <example>0.0.0.9 от 14.02.2015</example>
        public string Version { get; set; }

        /// <summary>
        /// Сетевая информация репозитория
        /// </summary>
        public BaseRepositoryInfo Info { get; set; }


        /// <summary>
        /// Преобразует базовый репозиторий к репозиторию модификаций для ArmA II
        /// </summary>
        /// <returns></returns>
        public ArmA2ModificationsRepository ToArmA2ModificationsRepository()
        {
            return new ArmA2ModificationsRepository()
            {
                Info = this.Info,
                Name = this.Name,
                Version = this.Version
            };
        }

        /// <summary>
        /// Преобразует базовый репозиторий к репозиторию серверов для ArmA II
        /// </summary>
        /// <returns></returns>
        public ArmA2ServerRepository ToArmA2ServerRepository()
        {
            return new ArmA2ServerRepository()
            {
                Info = this.Info,
                Name = this.Name,
                Version = this.Version
            };
        }
    }
}
