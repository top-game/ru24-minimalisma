namespace RU24Lib.Types
{
    /// <summary>
    /// Тип загрузки из серверного репозитория
    /// 
    /// </summary>
    public enum BaseRepositoryLoadType
    {
        /// <summary>
        /// Загрузка XML и его парсинг
        /// </summary>
        XML,
        /// <summary>
        /// Загрузка JSON и его парсинг
        /// </summary>
        JSON
    }
}