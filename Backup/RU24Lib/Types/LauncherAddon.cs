﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RU24Lib.Types
{
    /// <summary>
    /// Дополнение для лаунчера RU24 Minimalisma
    /// Если вы собираетесь создать дополнение для WPF_RU24, используйте LauncherAddonWR
    /// </summary>
    public class LauncherAddon
    {
        /// <summary>
        /// Действие - инициализация 
        /// </summary>
        public virtual void Init()
        {
            Console.WriteLine("---");
            Console.WriteLine("Hello! I'm RU24 API Lib.");
            Console.WriteLine("I see you trying to create module. It's good");
            Console.WriteLine("But you not setup Init() - it a problem!");
            Console.WriteLine("---");
            Console.WriteLine("Addon " + Name + " start loading now at version " + Version);
            // Do some there. 
            // Negusta :D
        }

        /// <summary>
        /// Версия дополнения
        /// </summary>
        public string Version = "(NO VERSION DEFINED!)";

        /// <summary>
        /// Название дополнения
        /// </summary>
        public string Name = "(NO NAME DEFINED!)";
    }

    
}
