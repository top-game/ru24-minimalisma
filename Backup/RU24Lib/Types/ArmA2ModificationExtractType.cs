namespace RU24Lib.Types
{
    /// <summary>
    /// Тип извлечения, скачивания
    /// </summary>
    /// <example>
    /// ZIP будет извлекать из архива, загруженного через HTTP.
    /// TorrentZIP извлекать из архива, загруженного через торрент
    /// </example>
    public enum ArmA2ModificationExtractType
    {
        /// <summary>
        /// Сначала архив будет загружен через HTTP, а затем ZIP (RAR) архив будет извлекаться в папку, заданную FolderName
        /// </summary>
        ZIP,
        /// <summary>
        /// Сначала скачивается торрент, а затем файлы извлекаются из ZIP (RAR) архива в папку, заданную FolderName
        /// </summary>
        TorrentZIP,
        /// <summary>
        /// Сначала скачивается торрент, а затем файлы перемещаются в папку, заданную FolderName
        /// </summary>
        Torrent,
        /// <summary>
        /// Расширенный режим в котором извлечение предоставляется методу OnLoad()
        /// Этот режим должен использоватся только если сервер создает библиотека!
        /// В противном случае, он будет пропущен.
        /// </summary>
        ExtendedMode
    }
}