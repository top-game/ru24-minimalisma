﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет базовый класс конфигурации игры
    /// </summary>
    public class BaseGameConfiguration
    {
        /// <summary>
        /// Представляет базовый класс конфигурации игры
        /// </summary>
        public BaseGameConfiguration()
        {
            this.Window = new BaseWindowConfiguration();
        }

        /// <summary>
        /// Количество памяти в мегабайтах
        /// </summary>
        public int Memory { get; set; }

        /// <summary>
        /// Представляет класс для управления оконным режимом
        /// </summary>
        public BaseWindowConfiguration Window { get; set; }

        /// <summary>
        /// Исполняемый файл игры
        /// </summary>
        public string ExecutableFile { get; set; }

        /// <summary>
        /// Аргументы (параметры) запуска игры
        /// </summary>
        public string Arguments { get; set; }


    }
    /// <summary>
    /// Представляет класс управления оконным режимом
    /// </summary>
    public class BaseWindowConfiguration
    {
        /// <summary>
        /// Истинно, если следует использовать оконный режим
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        public bool UseWindow { get; set; }

        /// <summary>
        /// Ширина окна
        /// </summary>
        [System.ComponentModel.DefaultValue(1280)]
        public int Width { get; set; }

        /// <summary>
        /// Высота окна
        /// </summary>
        [System.ComponentModel.DefaultValue(720)]
        public int Height { get; set; }

        /// <summary>
        /// Заголовок окна, __winname__ будет заменено на текущий заголовок окна. %, если не нужно заменять заголовок окна.
        /// </summary>
        [System.ComponentModel.DefaultValue("__winname__ - TOP-GAME RU24")]
        public string Title { get; set; }

        /// <summary>
        /// Представляет расширеные функции управления окнами
        /// </summary>
        public static class ExtendedWindowFunctions
        {
            /// <summary>
            /// Позволяет задать позицию окна на экране
            /// </summary>
            /// <param name="hWnd">Дескриптор окна</param>
            /// <param name="hWndInsertAfter">A handle to the window to precede the positioned window in the Z order. This parameter must be a window handle</param>
            /// <param name="X">Расположение окна по кординате X</param>
            /// <param name="Y">Расположение окна по кординате Y</param>
            /// <param name="cx">Ширина окна</param>
            /// <param name="cy">Высота окна</param>
            /// <param name="uFlags">Флаги</param>
            /// <returns></returns>
            /// <see href="https://msdn.microsoft.com/en-us/library/ms633545.aspx"/>
            [System.Runtime.InteropServices.DllImport("user32.dll")]
            public extern static bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);

            /// <summary>
            /// Задает заголовок окна
            /// </summary>
            /// <param name="hWnd">Дескриптор окна</param>
            /// <param name="text">Заголовок окна</param>
            /// <returns></returns>
            [System.Runtime.InteropServices.DllImport("user32.dll")]
            public static extern int SetWindowText(IntPtr hWnd, string text);
        }
    }
    
}
