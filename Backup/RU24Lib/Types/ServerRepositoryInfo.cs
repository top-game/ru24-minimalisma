using System;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру, описывающую репозиторий, к которому идет обращение за данными о серверах
    /// </summary>
    public class ServerRepositoryInfo
    {
        /// <summary>
        /// Ссылка, по которой распологается информация о серверах (ссылка на репозиторий)
        /// </summary>
        public Uri LoadUri { get; set; }

        /// <summary>
        /// Тип загрузки из репозитория
        /// </summary>
        public BaseRepositoryLoadType LoadType { get; set; }
    }
}