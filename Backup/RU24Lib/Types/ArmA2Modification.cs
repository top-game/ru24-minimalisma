﻿using System;
using System.Linq;

namespace RU24Lib.Types
{
    /// <summary>
    /// Предстваляет собой структуру, описывающую модификацию (дополнение) к игре ArmA II
    /// </summary>
    public class ArmA2Modification
    {
        /// <summary>
        /// UID модификации (дополнения)
        /// Можно не описывать модификацию, если такой UID зарегистрирован на репозиториях модов к игре ArmA II
        /// </summary>
        public string UID { get; set; }

        /// <summary>
        /// Название модификации (дополнения)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Версия модификации
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Описание мода
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Название папки
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// Структура, настраивающая способ извлечения архива и скачивания
        /// </summary>
        public ArmA2ModificationExtract Extract { get; set; }

        /// <summary>
        /// Ссылка к библиотеке модификации
        /// </summary>
        public Uri ModificationLibaryUri { get; set; }

        /// <summary>
        /// Ссылка к картинке модификации
        /// </summary>
        public String PictureUrl { get; set; }

        /// <summary>
        /// Вернет строку с именем и версией мода
        /// </summary>
        /// <returns></returns>
        /// <example>SoW 1.2</example>
        public override string ToString()
        {
            return Name + " " + Version;
        }


        /// <summary>
        /// Лаунчер запрашивает разрешение на загрузку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLoadRequest(object sender, ArmA2ModificationLoadRequestEventArgs e)
        {
            this.LoadRequest(sender, e);
        }

        /// <summary>
        /// Представляет аргументы запроса на загрузку
        /// </summary>
        public class ArmA2ModificationLoadRequestEventArgs
        {
            /// <summary>
            /// Истинно, если следует отвергнуть приглашение на загрузку мода. Лаунчер покажет пользователю сообщение
            /// </summary>
            [System.ComponentModel.DefaultValue(false)]
            public bool Cancel { get; set; }
            
        }

        /// <summary>
        /// ГАДЗИЛА
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void LoadRequestEventHandler(object sender, ArmA2ModificationLoadRequestEventArgs e);

        /// <summary>
        /// Лаунчер запрашивает разрешение на загрузку.
        /// </summary>
        public event LoadRequestEventHandler LoadRequest;

        /// <summary>
        /// Аргументы загрузки
        /// </summary>
        public class ArmA2ModificationLoadEventArgs
        {
            /// <summary>
            /// Истинно, если следует заблокировать интерфейс для пользователя
            /// </summary>
            [System.ComponentModel.DefaultValue(true)]
            public bool BlockUserInterface { get; set; }

            /// <summary>
            /// Интерфейс для пользователя
            /// </summary>
            public UserInterfaceLoadController UserInterfaceController { get; set; }
        }
    }
}
