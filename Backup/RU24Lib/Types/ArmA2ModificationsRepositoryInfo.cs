using System;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру, описывающую репозиторий модификаций к игре ArmA II
    /// </summary>
    public class ArmA2ModificationsRepositoryInfo
    {
        /// <summary>
        /// Ссылка по которой распологается информация о модификациях (дополнениях) к игре ArmA II
        /// </summary>
        public Uri LoadUri { get; set; }

        /// <summary>
        /// Тип загрузки из репозитория
        /// </summary>
        public ArmA2ModificationsRepositoryLoadType LoadType { get; set; }
    }
}