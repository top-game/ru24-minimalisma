namespace RU24Lib.Types
{
    /// <summary>
    /// Информация для соединения с сервером
    /// </summary>
    public class ArmA2ServerConnectInformation
    {
        /// <summary>
        /// IP адрес сервера
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// Порт сервера
        /// </summary>
        public int Port { get; set; }
    }
}