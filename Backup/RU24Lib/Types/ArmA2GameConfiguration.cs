﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет класс конфигурации 
    /// </summary>
    public class ArmA2GameConfiguration : BaseGameConfiguration
    {
        /// <summary>
        /// Модификации игры
        /// </summary>
        public ArmA2Modification[] Modifications { get; set; }

        /// <summary>
        /// Истинно, если следует использовать бета-версию
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        public bool UseBeta { get; set; }

        /// <summary>
        /// Путь к бета-версии. (Например, Expansions/beta/) __dir__ заменяется на директорию игры
        /// </summary>
        [System.ComponentModel.DefaultValue("__dir__/Expansions/beta/")]
        public string BetaPath { get; set; }

        /// <summary>
        /// Исполняемый файл ArmA2
        /// </summary>
        public string ExecutableFile { get; set; }

        public ArmA2ArgumentsBuilder Arguments;

        /// <summary>
        /// Представляет собой конструктор аргументов
        /// </summary>
    }

    public class ArmA2ArgumentsBuilder
    {
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            if (this.window)
            {
                str.Append("-window");
                str.Append(" ");
            }

            if (this.world != "%")
            {
                str.Append("-world=" + this.world);
                str.Append(" ");
            }

            if (this.skipIntro)
            {
                str.Append("-skipIntro");
                str.Append(" ");
            }

            if (this.nosplash)
            {
                str.Append("-nospash");
                str.Append(" ");
            }

            if (this.cpuCount != -1)
            {
                str.Append("-cpuCount=" + this.cpuCount);
                str.Append(" ");
            }

            if (this.exThreads != -1 && this.exThreads > 0)
            {
                str.Append("-exThreads=" + this.exThreads);
                str.Append(" ");
            }

            if (this.maxMem != -1 && this.maxMem > 256)
            {
                // такс такс такс, што тут у нас мемчеки, мемчики ахахахахах
                str.Append("-maxMem=" + maxMem);
                str.Append(" ");
            }

            if (this.noCB)
            {
                str.Append("-noCB");
                str.Append(" ");
            }

            if (this.beta != "%")
            {
                str.Append("-beta=" + this.beta);
                str.Append(" ");
            }

            if (this.profiles != "%")
            {
                str.Append("-profiles=\"" + this.profiles + "\t");
                str.Append(" ");
            }

            if (this.name != "%")
            {
                str.Append("-name=" + this.name);
                str.Append(" ");
            }

            if (this.noLogs)
            {
                str.Append("-noLogs");
                str.Append(" ");
            }



            return str.ToString();
        }

        /// <summary>
        /// Истинно, если следует использовать оконный режим.
        /// </summary>
        [Obsolete("Не рекомендуется строить этот аргумент. Лучше использовать его в конфигурации запуска")]
        [System.ComponentModel.DefaultValue(true)]
        public bool window { get; set; }

        /// <summary>
        /// Истинно, если следует отключить заставку.
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        public bool nosplash { get; set; }

        /// <summary>
        /// Название карты, которая будет отображатся в главном меню. Присвойте значение %, если не следует использовать этот параметр.
        /// </summary>
        [System.ComponentModel.DefaultValue("%")]
        public string world { get; set; }

        /// <summary>
        /// Начиная с 1.55 - отключает заставки мира в главном меню.
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        public bool skipIntro { get; set; }

        /// <summary>
        /// Определяет предел выделения памяти (в мегабайтах). 256 - жестокий минимум, 2047 - жестокий максимум. Задайте -1, чтобы игра автоматически выставляла значения (512-1536 МБ) параметра maxMem.
        /// </summary>
        [System.ComponentModel.DefaultValue(-1)]
        public int maxMem { get; set; }

        /// <summary>
        /// Определяет предел выделения видеопамяти (в мегабайтах). 128 - жестокий минимум, 2047 - жестокий максимум, любое значение более может привести к непредвиденным событиям.
        /// </summary>
        [System.ComponentModel.DefaultValue(-1)]
        public int maxVRAM { get; set; }

        /// <summary>
        /// Заставляет игру использовать Direct3D версии 9. Однако не работает на Windows Vista, 7 и более новых версиях.
        /// </summary>
        [System.ComponentModel.DefaultValue("False")]
        [Obsolete("Поддержка Windows XP прекращена 8 апреля 2014 года. Не работает на Vista, 7, 8.1!")]
        public bool winxp { get; set; }

        /// <summary>
        /// Истинно, если следует отключить использование многоядерных процессоров. Это замедляет рендеринг, но может решить проблему визуальных глюков.
        /// </summary>
        [System.ComponentModel.DefaultValue("")]
        public bool noCB { get; set; }

        /// <summary>
        /// Задает количество используемых ядер. Задайте -1, если не хотите переопределять это значение. (оно будет опеределенно игрой, как количество ядер ПК)
        /// </summary>
        /// <see cref="https://community.bistudio.com/wiki/Arma2:_Startup_Parameters#cpuCount"/>
        [System.ComponentModel.DefaultValue(-1)]
        public int cpuCount { get; set; }

        /// <summary>
        /// Задает количество дополнительных потоков. Это отменит автоматическое обнаружение (которые используют двухъядерные за 3 и за 7 четырехъядерный).
        /// </summary>
        /// <see cref="https://community.bistudio.com/wiki/Arma2:_Startup_Parameters#exThreads"/>
        public int exThreads { get; set; }

        /// <summary>
        /// Задает определенный аллокатор, который будет использоваться. Значительно влияет на производительность и стабильность игры.
        /// </summary>
        [Obsolete("Данный параметр был заблокирован из-за большинства читов работающих с помощью него. Также этот параметр не работает на BattlEye серверах", true)]
        public string malloc { get; set; }

        /// <summary>
        /// Истинно, если не следует сохранять логи. (Логи могут пригодится для определения ошибки)
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public bool noLogs { get; set; }

        /// <summary>
        /// Задает имя профиля
        /// </summary>
        /// <value>По умолчанию имя: changeme, это не самый красивый ник, и скорей всего к игроку пребежит огромный петух и откусит голову, а дальше пойдет сжигать своим лазерным зрением здания, машины и всё на своём пути. Так что замените его на % чтобы не задавать этот параметр. Спасибо, что вы так добры!</value>
        [System.ComponentModel.DefaultValue("changeme")]
        public string name { get; set; }
        // Отличный комментарий к отличному свойству.


        /// <summary>
        /// Задает локацию папки пользователей ArmA II. Задайте %, чтобы пропустить этот аргумент.
        /// </summary>
        public string profiles { get; set; }

        /// <summary>
        /// Задает IP адрес для подключения. %, чтобы пропустить параметр.
        /// </summary>
        [Obsolete("Данный параметр будет автоматически задан лаунчером RU24")]
        public string connect { get; set; }

        /// <summary>
        /// Задает порт для подключения, -1 чтобы пропустить параметр.
        /// </summary>
        [Obsolete("Данный параметр будет автоматически задан лаунчером RU24")]
        [System.ComponentModel.DefaultValue(-1)]
        public int port { get; set; }

        /// <summary>
        /// Задайте для загрузки указанных бета-суб-папок. %, чтобы пропустить аргумент.
        /// </summary>
        [System.ComponentModel.DefaultValue("%")]
        public string beta { get; set; }
    } 
}
