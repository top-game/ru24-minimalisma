using System;

namespace RU24Lib.Types
{
    /// <summary>
    /// Предстваляет собой структуру, описывающую способ извлечения и скачивания
    /// </summary>
    public class ArmA2ModificationExtract
    {
        /// <summary>
        /// Тип извлечения архива и скачивания
        /// </summary>
        public ArmA2ModificationExtractType ExtractType { get; set; }

        /// <summary>
        /// Действие, когда потребуется извлечь файлы
        /// Выполняется только когда тип извлечения или скачивания задан на ExtendedMode
        /// </summary>
        /// <example>
        /// Загрузите через торрент архив и извлеките его с помощью утилиты, а затем переместите файлы, как нужно. Keys в ..\..\..\Keys
        /// </example>
        public Action OnLoad { get; set; }
    }
}