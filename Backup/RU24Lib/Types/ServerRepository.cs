﻿using System;
using System.Linq;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру репозитория
    /// </summary>
    public class ServerRepository
    {
        /// <summary>
        /// Название репозитория
        /// </summary>
        /// <example>ServExample</example>
        public string Name { get; set; }
        /// <summary>
        /// Версия репозитория
        /// </summary>
        /// <example>0.0.0.1</example>
        public string Version { get; set; }
        /// <summary>
        /// Информация о репозитории
        /// </summary>
        public ServerRepositoryInfo Info { get; set; }

        //public Servers[] Servers;
 
    }
}
