﻿using System;
using System.Linq;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру, содержащую базовые параметры соеденения с репозиторием
    /// </summary>
    public class BaseRepositoryInfo
    {
        /// <summary>
        /// Ссылка на репозиторий
        /// </summary>
        public Uri LoadUri { get; set; }

        /// <summary>
        /// Тип загрузки данных из репозитория
        /// </summary>
        /// <example>JSON</example>
        public BaseRepositoryLoadType LoadType { get; set; }

        /// <summary>
        /// Ветвь репозитория
        /// </summary>
        public string Node { get; set; }

        /// <summary>
        /// Устанавливает защиту на репозиторий, лаунчер не позволит его изменить
        /// </summary>
        public bool MustBeProtected { get; set; }
    }
}
