﻿using System;
using System.Linq;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру репозитория серверов для игры ArmA II
    /// </summary>
    public class ArmA2ServerRepository
    {
        /// <summary>
        /// Название репозитория
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Версия репозитория
        /// </summary>
        /// <example>JUNE262015</example>
        public string Version { get; set; }

        /// <summary>
        /// Сетевая информация репозитория
        /// </summary>
        public BaseRepositoryInfo Info { get; set; }

        /// <summary>
        /// Сервера из репозитория
        /// </summary>
        public ArmA2Server[] Servers { get; set; }
    }
}
