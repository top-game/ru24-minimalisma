﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет класс, содержащий информацию о сервере (Текущее/Максимальное количество игроков/Модификации)
    /// </summary>
    public class ArmA2ServerInformation_GamerLabs
    {
        /// <summary>
        /// Конструктор класса A2SI_GL
        /// </summary>
        /// <param name="server">Сервер</param>
        /// <param name="LoadInformation">Ложное значение, если вы не хотите загружать информацию сразу, иначе истинно</param>
        public ArmA2ServerInformation_GamerLabs(ArmA2Server server, bool LoadInformation = false)
        {
            this._server = server;

            if(LoadInformation)
            Refresh();
        }

        

        /// <summary>
        /// Название сервера
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Игроки на сервере
        /// </summary>
        public Player[] Players { get; private set; }

        /// <summary>
        /// Максимальное количество игроков на сервере
        /// </summary>
        public int MaxPlayers { get; private set; }

        /// <summary>
        /// Сервер использует пароль. Если да, то истинно, иначе ложно
        /// </summary>
        public bool UsePassword { get; private set; }

        /// <summary>
        /// Последние слова перед смертью
        /// </summary>
        public string LastError { get; private set; }

        /// <summary>
        /// Текущая карта на сервере
        /// </summary>
        public String Map { get; private set; }

        /// <summary>
        /// Версия игры (если точнее, установленного сервера, но версия сервера = версии игры)
        /// </summary>
        public String Version { get; private set; }

        private ArmA2Server _server { get; set; }

        /// <summary>
        /// Заного загружает информацию о сервере
        /// </summary>
        public void Refresh()
        {
            try
            {
                string str = (new WebClient()).DownloadString(String.Format("https://api.gamerlabs.net/?type=arma2&host={0}&port={1}", _server.ConnectInformation.Ip, _server.ConnectInformation.Port));
                // Проверка на ошибки в ответе
                Console.WriteLine(str);
                if (str == "\"UDP Watchdog Timeout\"")
                {
                    throw new Exception("UPDFATALERROR");
                    return;
                }
                var JsonReq = JsonConvert.DeserializeObject<_response>(str);
                this.Name = JsonReq.data.name;
                this.UsePassword = JsonReq.data.password;
                this.Version = JsonReq.data.raw.version;
                this.Players = JsonReq.data.players;
                this.MaxPlayers = JsonReq.data.maxplayers;
                this.LastError = "UPDCOMPLETED";

            }
            catch (Exception exc)
            {
                this.LastError = exc.Message;
            }
            
        }

        // Request format: https://api.gamerlabs.net/?type=arma2&host={ip_adress}&port={port}
        /*
        Ansewer is... JSON! One Love <3
          {
          "data": {
            "name": "{name}",
            "map": "{map}",
            "password": true,
            "raw": {
              "protocol": 17,
              "folder": "{folder}",
              "game": "{game}",
              "steamappid": 0,
              "numplayers": 0,
              "numbots": 0,
              "listentype": "d",
              "environment": "w",
              "secure": 0,
              "version": "{version}",
              "port": 2372,
              "steamid": "90095427100938248",
              "tags": "bf,r144,n0,s7,i1,mf,lf,vf,dt,tcoop,g65545,hac9b03d,c4194303-4194303,pw,e15,j0,k0,",
              "gameid": "107410",
              "rules": {
                "mods:0-4": "Arma 3 Zeus;d39ab57c;Arma 3 Karts;eaf7ba8a;Arma 3 Helicopters;74bdb4bc;Arma 3 Marksmen;a162e35d;@TAW_CONTENT_V1;742972a1;@TAW_I",
                "mods:1-4": "NHOUSE_V1;1e1d862b;@TAW_MAPS_V1;a2e1aec4;@AllInArmaTerrainPack;b7835d2f;Task Force Arrowhead Radio 0.9.7;11fdd19c;@TAW_CORE_TFR",
                "mods:2-4": "_V1;41cff0f2;RHS: United States Forces;2562fd9f;RHS: Armed Forces of the Russian Federation;15d58766;ASR AI3 0.9.19.77;b4fe82b8",
                "mods:3-4": ";@fata;f9012532;@NziwasogoV06;2ffbacaf;"
              }
            },
            "maxplayers": 20,
            "players": [],
            "bots": []
          },
          "lastUpdated": 1430913039625,
          "Source": "https://gamerlabs.net"
        }
        */

        /// <summary>
        /// Игрок
        /// </summary>
        public class Player
        {
            /// <summary>
            /// Имя
            /// </summary>
            public string name;
            /// <summary>
            /// Очки
            /// </summary>
            public int score;
            /// <summary>
            /// Время игры
            /// </summary>
            public double time;
        }

        private class _raw
        {
            public string game;
            public string folder;
            public string version;
        }

        private class _data
        {
            public string name;
            public string map;
            public bool password;
            public _raw raw;
            public int maxplayers;
            public Player[] players;
            
        }
        private class _response
        {
            public _data data;
            public string Source;
        }

        
        // (C) GamerLabs, https://gamerlabs.net/
    }
}

