﻿using System;
using System.Linq;

namespace RU24Lib.Types
{
    /// <summary>
    /// Представляет собой структуру, описывающую сервер для игры ArmA II
    /// </summary>
    public class ArmA2Server
    {
        /// <summary>
        /// Имя сервера
        /// </summary>
        /// <example>ArmA 2 Meat [24/7][NO CD][RU]</example>
        public string Name { get; set; }
        /// <summary>
        /// Версия игры
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Сообщение дня
        /// </summary>
        public string MOTD { get; set; }

        /// <summary>
        /// Страна, где расположен сервер
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Пинг сервера. Будет задан автоматически системой
        /// </summary>
        public int Ping { get; set; }

        /// <summary>
        /// Максимальное количество игроков
        /// </summary>
        public int MaximumPlayers { get; set; }

        /// <summary>
        /// Текущее количество игроков
        /// </summary>
        public int CurrentPlayers { get; set; }

        /// <summary>
        /// Является ли сервер официальным сервером игры?
        /// </summary>
        public bool Official { get; set; }

        /// <summary>
        /// Модификации сервера
        /// </summary>
        /// <example>
        /// SoW, @Lights
        /// </example>
        public ArmA2Modification[] Modifications { get; set; }

        /// <summary>
        /// Вернет строку содержащую имена и версии модов
        /// </summary>
        /// <param name="count">Количество</param>
        /// <returns></returns>
        /// <example>SoW 1.2, Little Birds 1.0</example>
        public string ModificationsString(int count)
        {
            try
            {
                if (Modifications.Length == 0)
                {
                    return "Сервер без модификаций";
                }
                string str = "";
                for (int i = 0; i < Modifications.Length && i <= count; i++)
                {
                    str += Modifications[i].ToString();
                    if (i != count)
                    {
                        str += ", ";
                    }
                }
                return str;
            }
            catch
            {
                return "Не распознано";
            }
        }

        /// <summary>
        /// Информация для соединения с сервером
        /// </summary>
        public ArmA2ServerConnectInformation ConnectInformation { get; set; }
        
    }
}
