﻿using System;
using System.Linq;

namespace RU24Lib.Interfaces
{
    interface IBaseRepository
    {
        string Name { get; set; }
        string Version { get; set; }
    }
}
