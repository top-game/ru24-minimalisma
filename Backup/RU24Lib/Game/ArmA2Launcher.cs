﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RU24Lib.Game
{
    /// <summary>
    /// Представляет класс для запуска ArmA II 
    /// </summary>
    public class ArmA2Launcher
    {
        /// <summary>
        /// Текущая конфигурация
        /// </summary>
        public Types.ArmA2GameConfiguration Configuration { get; set; }

        /// <summary>
        /// Создает класс для запуска ArmA II
        /// </summary>
        /// <param name="configuration"></param>
        public ArmA2Launcher(Types.ArmA2GameConfiguration configuration)
        {
            this.Configuration = configuration;

            if (!File.Exists(configuration.ExecutableFile))
            {
                throw new System.IO.FileNotFoundException("Файл " + this.Configuration.ExecutableFile + " не найден");
            }

            if (configuration.Modifications.Length == 0)
            {
                Console.WriteLine("Warning! No modifications gived");
            }

            Configuration.BetaPath.Replace("__dir__", Path.GetDirectoryName(Configuration.ExecutableFile));
        }

        /// <summary>
        /// Получает строку модификаций
        /// </summary>
        /// <returns>mod1;@mod2;Mods/beta/mod3</returns>
        private string GetModificationsStr()
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < this.Configuration.Modifications.Length; i++)
            {
                str.Append((i == 0 ? "" : ";") + this.Configuration.Modifications[i].FolderName);
            }
            return str.ToString();
        }

        /// <summary>
        /// Запускает игру
        /// </summary>
        /// <param name="waitToExit">Истинно, если следует ждать выхода из процесса</param>
        public void Start(bool waitToExit = true)
        {
            var gameProcInfo = new ProcessStartInfo();
            gameProcInfo.FileName = GetExecutableFile();
            gameProcInfo.ErrorDialog = true;
            if (Configuration.Window.UseWindow)
            {
                // "нафиг велосипед строить" сказал Я и сделал такую фичу:
                Configuration.Arguments.window = true;
                gameProcInfo.WindowStyle = ProcessWindowStyle.Normal;
            }
            else
            {
                gameProcInfo.WindowStyle = ProcessWindowStyle.Maximized;
            }
            gameProcInfo.Arguments = this.Configuration.Arguments.ToString() + " -mods=" + GetModificationsStr();
            var gameProc = Process.Start(gameProcInfo);
            if (Configuration.Window.UseWindow && gameProc.WaitForInputIdle(15000))
            {
                Types.BaseWindowConfiguration.ExtendedWindowFunctions.SetWindowText(gameProc.MainWindowHandle, Configuration.Window.Title.Replace("__winname__", gameProc.MainWindowTitle));
                Types.BaseWindowConfiguration.ExtendedWindowFunctions.SetWindowPos(gameProc.MainWindowHandle, (IntPtr)0, System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width - Configuration.Window.Width / 2, System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height - Configuration.Window.Width / 2, Configuration.Window.Width, Configuration.Window.Height, 0x0040);
            }

            if (waitToExit)
            {
                // ещё фича :D
                gameProc.WaitForExit();
            }
        }

        /// <summary>
        /// Получает путь исполняемого бета-файла или просто исполняемого файла игры.
        /// </summary>
        /// <returns></returns>
        public string GetExecutableFile()
        {
            if (!Configuration.UseBeta)
            {
                return Configuration.ExecutableFile;
            }
            else
            {
                return Configuration.BetaPath + "\\" + Path.GetFileName(Configuration.ExecutableFile);
            }
        }

        /// <summary>
        /// Проверяет исполняемые файлы, выдаст исключение если файлы найдены не будут
        /// </summary>
        public void CheckFiles()
        {
            if (this.Configuration.UseBeta)
            {
                if (!Directory.Exists(Configuration.BetaPath))
                {
                    throw new DirectoryNotFoundException("Директория " + Configuration.BetaPath + " не найдена!");
                }
                else
                {
                    if (!File.Exists(Configuration.BetaPath + "\\" + Path.GetFileName(Configuration.ExecutableFile)))
                    {
                        throw new FileNotFoundException("Файл " + Configuration.BetaPath + "\\" + Path.GetFileName(Configuration.ExecutableFile) + " не найден");
                    }
                }
            }
            else
            {
                if (!Directory.Exists(Path.GetDirectoryName(Configuration.ExecutableFile)))
                {
                    throw new DirectoryNotFoundException("Директория " + Configuration.BetaPath + " не найдена!");
                }
                else
                {
                    if (!File.Exists(Configuration.ExecutableFile))
                    {
                        throw new FileNotFoundException("Файл " + Configuration.ExecutableFile + " не найден");
                    }
                }
            }
        }
    }
}
