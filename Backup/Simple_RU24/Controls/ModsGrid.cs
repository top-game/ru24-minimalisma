﻿using RU24Lib.Types;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Forms;

namespace Simple_RU24.Controls
{
    public partial class ModsGrid : UserControl
    {
        public ModsGrid()
        {
            InitializeComponent();
            this.Items = new ObservableCollection<ModsGridItem>();
            this.Items.CollectionChanged += Items_CollectionChanged;
        }

        private int totalControls = 0;

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var arg = new ReportProgressEventArgs() { Action = ReportProgressEventArgs.ReportProgressAction.Ready};

            if (e.OldItems != null && e.OldItems.Count > 0)
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    foreach (ModsGridItem item in e.OldItems)
                    {
                        tableLayoutPanel1.Controls.Remove(item);
                        totalControls--;
                        ReportProgress(this, new ReportProgressEventArgs() { TotalItems = Items.Count, ItemsAdded = totalControls, Action = ReportProgressEventArgs.ReportProgressAction.Remove });
                    }
                }));
            }

            if (e.NewItems != null && e.NewItems.Count > 0)
            {
                var rem = 0;
                arg.TotalItems = Items.Count;
                arg.TotalItemsToAdd = e.NewItems.Count;
                arg.TotalItemsToRemove = 0;
                this.Invoke(new MethodInvoker(() =>
                {
                    foreach (ModsGridItem item in e.NewItems)
                    {
                        // It's feature:
                        item.Left = 0; // for
                        item.Top = 0; // some geniuses

                        tableLayoutPanel1.Controls.Add(item, 0, totalControls++);

                        arg.ItemsAdded = rem++; // Fea.. FUCKEN ..ture

                        ReportProgress(this, arg);
                    }
                }));
            }
            arg.Action = ReportProgressEventArgs.ReportProgressAction.Done;
        }

        public class ReportProgressEventArgs
        {
            /// <summary>
            /// Всего в коллекции
            /// </summary>
            public int TotalItems { get; set; }

            /// <summary>
            /// Всего предметов на добавление
            /// </summary>
            public int TotalItemsToAdd { get; set; }

            /// <summary>
            /// Всего предметов на удаление
            /// </summary>
            public int TotalItemsToRemove { get; set; }

            /// <summary>
            /// Добавлено предметов
            /// </summary>
            public int ItemsAdded { get; set; }

            /// <summary>
            /// Удалено предметов
            /// </summary>
            public int ItemsRemoved { get; set; }

            /// <summary>
            /// Вернет процент завершения
            /// </summary>
            public int Progress
            {
                get
                {
                    if (Action == ReportProgressAction.Add)
                    {
                        return (ItemsAdded * 100) / TotalItemsToAdd; // Math
                    }
                    else if (Action == ReportProgressAction.Remove)
                    {
                        return (ItemsRemoved * 100) / TotalItemsToRemove; // Math
                    }
                    else if (Action == ReportProgressAction.Done)
                    {
                        return 100; // Feature
                    }
                    else if (Action == ReportProgressAction.Ready)
                    {
                        return 0; // Feature
                    }
                    else if (Action == null)
                    {
                        return -1; // For geniuses 
                    }
                    else
                    {
                        throw new InvalidOperationException(); // Ahahahah. If someone doesn't understand
                    }
                }
            }

            /// <summary>
            /// Текущий статус или действие 
            /// </summary>
            public ReportProgressAction Action { get; set; }

            /// <summary>
            /// Текущий статус или действие
            /// </summary>
            public enum ReportProgressAction
            {
                /// <summary>
                /// Идет удаление
                /// </summary>
                Remove,
                /// <summary>
                /// Идет добавление
                /// </summary>
                Add,
                /// <summary>
                /// Подготовка
                /// </summary>
                Ready,
                /// <summary>
                /// Готово
                /// </summary>
                Done
            }
        }

        public void ReportProgress(object sender, ReportProgressEventArgs e)
        {
            OnReportProgress(sender, e);
        }

        public delegate void ReportProgressEventHandler(object sender, ReportProgressEventArgs e);

        public event ReportProgressEventHandler OnReportProgress = delegate { };


        // Update panel is private now
        // Because this is a dangerous thing
        // If you know what I mean :)
        private void UpdatePanel()
        {
            this.Invoke(new MethodInvoker(() =>
            {
                // I delete this fucken code
                // And i add exception in next time
            }));
        }

        private ObservableCollection<ModsGridItem> Items;

        /// <summary>
        /// Добавит новую панель
        /// </summary>
        /// <param name="modification">Модификация</param>
        /// <returns></returns>
        public LocalTypes.ModsGridItemController AddItem(ArmA2Modification modification)
        {
            // Check arg
            if (modification == null) { throw new ArgumentNullException(); }
            
            // Create new object
            var obj = new LocalTypes.ModsGridItemController(modification); // Item controller

            // Add new item from inner item created by obj
            this.Items.Add(obj.Item);

            return obj; // Return ItemController
        }

        public LocalTypes.ModsGridItemController AddItem(LocalTypes.ModsGridItemController _controller)
        {
            if (_controller == null) { throw new ArgumentNullException(); }

            this.Items.Add(_controller.Item);

            return _controller;
        }

        /// <summary>
        /// Добавит новую панель
        /// </summary>
        /// <param name="modifications">Модификации</param>
        /// <returns></returns>
        public LocalTypes.ModsGridItemController[] AddItems(ArmA2Modification[] modifications)
        {
            // Check arg
            if (modifications == null) { throw new ArgumentNullException(); }

            // Objects to return:
            var retobjs = new List<LocalTypes.ModsGridItemController>();

            // List of modifications
            var modiflist = new List<ArmA2Modification>(modifications);

            // Go
            modiflist.ForEach(delegate(ArmA2Modification o)
            {
                // Creaty new LocalTypes.ModDownloadItem
                var obj = new LocalTypes.ModsGridItemController(o);
                
                this.Items.Add(obj.Item); // This have inner ModDownloadsItem
                retobjs.Add(obj); // Add object to return list
            });
            
            
            return retobjs.ToArray();
        }

        /// <summary>
        /// Удаляет панель модификации
        /// </summary>
        /// <param name="item"></param>
        public void RemoveItem(LocalTypes.ModsGridItemController item)
        {
            // Remove panel
            this.Items.Remove(item.Item);

            // It's all...
        }

        private void tableLayoutPanel1_Click(object sender, EventArgs e)
        {
            // Focus on panel when click if this not focused
            if (!tableLayoutPanel1.Focused)
                tableLayoutPanel1.Focus();
        }



        private void ModDownloadsGrid_Load(object sender, EventArgs e)
        {
            // Change size of this grid...................................................................
            this.tableLayoutPanel1.Width = this.Width;
            // Gridd......
            this.tableLayoutPanel1.Height = this.Height;

            // FakeGrid, since WinForms doesn't have normal controls! 
        }
    }
}