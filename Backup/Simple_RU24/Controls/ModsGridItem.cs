﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Simple_RU24.WindowsFunctions;
using RU24Lib.Types;

namespace Simple_RU24.Controls
{
    /// <summary>
    /// ModDownloadItem
    /// </summary>
    public partial class ModsGridItem : UserControl
    {
        public ModsGridItem(RU24Lib.Types.ArmA2Modification modification)
        {
            InitializeComponent();
            OnInitialized(modification);
            this.label1.Parent = progressBar1;
        }

        /// <summary>
        /// Производит инициализацию
        /// </summary>
        /// <param name="mod"></param>
        public void OnInitialized(ArmA2Modification mod)
        {
            this.modNameLabel.Text = mod.Name;
            //if(mod.PictureUrl != null) { this.pictureBox1.Image = new Bitmap(mod.PictureUrl); }
            
            this.modDescriptionLabel.Text = mod.Description;
        }


        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void label1_TextChanged(object sender, EventArgs e)
        {
            ((Label)sender).Left = (progressBar1.Width - label1.Width / 2);
        }

        private void downloadButton_Click(object sender, EventArgs e)
        {
            Parent.DownloadRequest(sender, EventArgs.Empty);
        }



        private void infoButton_Click(object sender, EventArgs e)
        {
            Parent.InfoRequest(sender, EventArgs.Empty);
        }

        public LocalTypes.ModsGridItemController Parent = null;

        private void ModsGridItem_Load(object sender, EventArgs e)
        {
            if (AllSettings.LauncherSettings.Default.FirstRun)
            {
                this.Enabled = false;
            }
        }

        [System.ComponentModel.DefaultValue(false)]
        public bool LockDownloadButton
        {
            get
            {
                return downloadButton.Enabled;
            }

            set
            {
                if (value != null)
                    downloadButton.Enabled = value;
                else
                    throw new ArgumentNullException();
            }
        }


        
    }
}
