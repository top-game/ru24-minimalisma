﻿namespace Simple_RU24.Controls
{
    partial class ModsGridItem
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.downloadButton = new System.Windows.Forms.Button();
            this.modNameLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.infoButton = new System.Windows.Forms.Button();
            this.SSWMButton = new System.Windows.Forms.Button();
            this.modDescriptionLabel = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // downloadButton
            // 
            this.downloadButton.Location = new System.Drawing.Point(141, 108);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(75, 23);
            this.downloadButton.TabIndex = 1;
            this.downloadButton.Text = "Загрузить";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.downloadButton_Click);
            // 
            // modNameLabel
            // 
            this.modNameLabel.AutoEllipsis = true;
            this.modNameLabel.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.modNameLabel.Location = new System.Drawing.Point(137, 3);
            this.modNameLabel.Name = "modNameLabel";
            this.modNameLabel.Size = new System.Drawing.Size(370, 23);
            this.modNameLabel.TabIndex = 2;
            this.modNameLabel.Text = "Название мода";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Simple_RU24.Properties.Resources.close_button_simple;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // infoButton
            // 
            this.infoButton.Location = new System.Drawing.Point(222, 108);
            this.infoButton.Name = "infoButton";
            this.infoButton.Size = new System.Drawing.Size(85, 23);
            this.infoButton.TabIndex = 3;
            this.infoButton.Text = "Информация";
            this.infoButton.UseVisualStyleBackColor = true;
            this.infoButton.Click += new System.EventHandler(this.infoButton_Click);
            // 
            // SSWMButton
            // 
            this.SSWMButton.Enabled = false;
            this.SSWMButton.Location = new System.Drawing.Point(313, 108);
            this.SSWMButton.Name = "SSWMButton";
            this.SSWMButton.Size = new System.Drawing.Size(194, 23);
            this.SSWMButton.TabIndex = 4;
            this.SSWMButton.Text = "Поиск серверов с этим модом";
            this.SSWMButton.UseVisualStyleBackColor = true;
            // 
            // modDescriptionLabel
            // 
            this.modDescriptionLabel.AutoEllipsis = true;
            this.modDescriptionLabel.Location = new System.Drawing.Point(140, 26);
            this.modDescriptionLabel.Name = "modDescriptionLabel";
            this.modDescriptionLabel.Size = new System.Drawing.Size(367, 44);
            this.modDescriptionLabel.TabIndex = 5;
            this.modDescriptionLabel.Text = "Описание мода";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(141, 88);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(366, 14);
            this.progressBar1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(310, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "0%";
            this.label1.TextChanged += new System.EventHandler(this.label1_TextChanged);
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ModsGridItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.modDescriptionLabel);
            this.Controls.Add(this.SSWMButton);
            this.Controls.Add(this.infoButton);
            this.Controls.Add(this.modNameLabel);
            this.Controls.Add(this.downloadButton);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ModsGridItem";
            this.Size = new System.Drawing.Size(515, 135);
            this.Load += new System.EventHandler(this.ModsGridItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Button downloadButton;
        public System.Windows.Forms.Label modNameLabel;
        public System.Windows.Forms.Button infoButton;
        public System.Windows.Forms.Button SSWMButton;
        public System.Windows.Forms.Label modDescriptionLabel;
        public System.Windows.Forms.ProgressBar progressBar1;
        public System.Windows.Forms.Label label1;
    }
}
