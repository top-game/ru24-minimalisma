﻿using System.Linq;
using Newtonsoft.Json;
using RU24Lib.Types;
using RU24Lib;
using Simple_RU24.Properties;
// RU24 Minimalisma
// Author: DaFri_Nochiterov
// Description: Core of Launcher
// Latest update: 26.04.2015
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Simple_RU24
{
    //Движок лаунчера RU24 Minimalisma
    // (!) В данный раздел следует обращатся только с соответствующими методами! Иначе велика вероятность повредить что-либо!
    /// <summary>
    /// Core of RU24 Minimalisma Launcher
    /// </summary>
    public class Engine
    {
        /// <summary>
        /// Main Window of this Application
        /// </summary>
        public static MainWindow MainWindow { get; private set; }

        /// <summary>
        /// Repositories of a servers.
        /// Do not edit that, else you can break something!
        /// </summary>
        [Obsolete("DO NOT EDIT THAT, ELSE YOU CAN BREAK SOMETHING!")]
        internal static ArmA2ServerRepository[] Repositories;

        /// <summary>
        /// Repositories of a mods.
        /// Do not edit that, else you can break something!
        /// </summary>
        [Obsolete("DO NOT EDIT THAT, ELSE YOU CAN BREAK SOMETHING!")]
        internal static ArmA2ModificationsRepository[] ModsRepositories;

        /// <summary>
        /// Add ArmA 2 servers repository
        /// </summary>
        /// <param name="repository">Servers repository</param>
        public static void AddServerRepository(ArmA2ServerRepository repository)
        {
            if (CheckExists<ArmA2ServerRepository>(Repositories, repository)) { return; }
            List<ArmA2ServerRepository> repo = new List<ArmA2ServerRepository>(Repositories);
            repo.Add(repository);
            Repositories = repo.ToArray();
        }


        /// <summary>
        /// Add ArmA 2 modifications repository
        /// </summary>
        /// <param name="repository">Modifications repository</param>
        public static void AddModificationsRepository(ArmA2ModificationsRepository repository)
        {
            if (CheckExists<ArmA2ModificationsRepository>(ModsRepositories, repository)) { return; }
            List<ArmA2ModificationsRepository> repo = new List<ArmA2ModificationsRepository>(ModsRepositories);
            repo.Add(repository);
            ModsRepositories = repo.ToArray();
        }

        /// <summary>
        /// Remove ArmA 2 servers repository
        /// </summary>
        /// <param name="repository"></param>
        public static void RemoveServerRepository(ArmA2ServerRepository repository)
        {
            List<ArmA2ServerRepository> repo = new List<ArmA2ServerRepository>(Repositories);
            repo.Remove(repository);
            Repositories = repo.ToArray();
        }

        /// <summary>
        /// Remove ArmA 2 modifications repository
        /// </summary>
        /// <param name="repository"></param>
        public static void RemoveModificationsRepository(ArmA2ModificationsRepository repository)
        {
            List<ArmA2ModificationsRepository> repo = new List<ArmA2ModificationsRepository>(ModsRepositories);
            repo.Remove(repository);
            ModsRepositories = repo.ToArray();
        }

        /// <summary>
        /// Ищет объект в массиве
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="from">Массив объектов</param>
        /// <param name="search">Искомый объект</param>
        /// <returns>Истинно - если найдено, иначе - ложно</returns>
        public static bool CheckExists<T>(T[] from, T search)
        {
                foreach (var v in from)
                {
                    if (v.Equals(search))
                    {
                        return true;
                    }
                }
                return false;
            
            
        }

#if (DEBUG)
        private Stopwatch Timer = new Stopwatch();
#endif

        [STAThread]
        public void Main(string[] args)
        {
            Console.WriteLine("TOP-GAME");
            Console.WriteLine("- RU24 TEAM");
            Console.WriteLine("-- RU24 Minimalisma Launcher");
#if (DEBUG)
            Timer.Start();
#endif

            Console.WriteLine("Engine started");

            
            if (args.Length == 1 && args[0] == "destroy")
            {
                Console.WriteLine("(!) Engine was destroy signatures...");

                Console.WriteLine("(!) You have 10 to cancel destroy!");

                System.Threading.Thread.Sleep(10000);

                Console.WriteLine("(i) Destoying...");

                Console.WriteLine("(i) Destoy signature for \"REPOSITORIES_MODS\"");
                Properties.Settings.Default.ModRepositoriesJSON = "[]";

                Console.WriteLine("(i) Destoy signature for \"REPOSITORIES_SERVERS\"");
                Properties.Settings.Default.ServerRepositoriesJSON = "[]";

                Console.WriteLine("(v) Completed");
                Properties.Settings.Default.Save();

                Application.Exit();
            }

            if (args.Length >= 2 && args.Length == 3 && args[0] == "info" && args[1] == "repos")
            {
                if (args[2] == "servers")
                {
                    Info("servrep");
                    Environment.Exit(0);
                }
                else if (args[2] == "mods")
                {
                    Info("modrep");
                    Environment.Exit(0);
                }
            }
            if (args.Length >= 2 && args.Length == 2 && args[0] == "info" && args[1] == "hardware")
            {
                Info("hardinfo");
                Environment.Exit(0);
            }
            if (args.Length >= 2 && args.Length == 3 && args[0] == "info" && args[1] == "hardware" && args[2] == "json")
            {
                Info("hardinfo", true);
                Environment.Exit(0);
            }


            if (Properties.Settings.Default.ServerRepositoriesJSON != "")
            {
                Console.WriteLine("Detected signature for \"REPOSITORIES_SERVERS\". Install it...");
                Repositories = JsonConvert.DeserializeObject<ArmA2ServerRepository[]>(Properties.Settings.Default.ServerRepositoriesJSON);

                Console.WriteLine("Signature installed");
            }
            else
            {
                Console.WriteLine("Detected errored signature of \"REPOSITORIES_SERVERS\"");
                Repositories = new ArmA2ServerRepository[0];
                Console.WriteLine("Signature fixed");
            }
           

            if (Properties.Settings.Default.ModRepositoriesJSON != "")
            {
                Console.WriteLine("Detected signature for \"REPOSITORIES_MODS\". Install it...");
                ModsRepositories = JsonConvert.DeserializeObject<ArmA2ModificationsRepository[]>(Properties.Settings.Default.ModRepositoriesJSON);

                Console.WriteLine("Signature installed");
            }
            else
            {
                Console.WriteLine("Detected errored signature of \"REPOSITORIES_MODS\"");
                ModsRepositories = new ArmA2ModificationsRepository[0];
                Console.WriteLine("Signature fixed");
            }

            Console.WriteLine("Enabling visual styles for forms");
            Application.EnableVisualStyles();

            Console.WriteLine("Set compatible text rendering to false");
            Application.SetCompatibleTextRenderingDefault(false);

            // New MainWindow
            Console.WriteLine("Creating MainWindow");
            var MainWindow = new MainWindow();

            Console.WriteLine("MainWindow created, set up events");
            MainWindow.FormClosing += delegate { Exiting(); };

            Console.WriteLine("Events setted");

            Console.WriteLine("Set up events for Application");
            Application.ApplicationExit += delegate { Exiting(); };

            Console.WriteLine("Events setted");

            Console.WriteLine("Loading Plugins/Addons");

            Engine.MainWindow = MainWindow;

            LoadPlugins();
#if (DEBUG)
            Console.WriteLine("Loading completed on " + Timer.Elapsed.Seconds + " " + Timer.Elapsed.Milliseconds);
#endif
            Console.WriteLine("Running MainWindow with Application base");


            Application.Run(MainWindow);

#if (DEBUG)
            Console.WriteLine("Exited on " + Timer.Elapsed.Seconds + " " + Timer.Elapsed.Milliseconds);
#endif
        }

        /// <summary>
        /// Выполняется при выходе. Осуществляет сохранение всех параметров
        /// </summary>
        public static void Exiting()
        {
            Console.WriteLine("Exiting command, save parametrs");

            Properties.Settings.Default.ServerRepositoriesJSON = JsonConvert.SerializeObject(Repositories);
            Properties.Settings.Default.ModRepositoriesJSON = JsonConvert.SerializeObject(ModsRepositories);

            Console.WriteLine("Save paramets to localuser");
            Properties.Settings.Default.Save();

            Console.WriteLine("Ready to exit, leave...");
            Environment.Exit(0);
        }

        /// <summary>
        /// Loading plugins
        /// </summary>
        public static void LoadPlugins()
        {
            Console.WriteLine("Plugins loader");
            Console.WriteLine("Warning! Plugins can be reason for application exit with error");
            try
            {
                if (Directory.Exists(Environment.CurrentDirectory + "\\RU24_PLUGINS\\"))
                {
                    List<RU24Lib.Types.LauncherAddon> addons = new List<LauncherAddon>();
                    string[] files = Directory.GetFiles(Environment.CurrentDirectory + "\\RU24_PLUGINS\\", "*.dll");
                    foreach (var file in files)
                    {
                        try
                        {
                            Console.WriteLine(Path.GetFileName(file) + " | Loading...");
                            var As = Assembly.LoadFile(file);
                            var types = As.GetTypes();
                            Console.WriteLine(Path.GetFileName(file) + " | State: Types getted");
                            foreach (var ty in types)
                            {
                                Console.WriteLine(Path.GetFileName(file) + " | Checking...");
                                if (typeof(RU24Lib.Types.LauncherAddon).IsAssignableFrom(ty))
                                {
                                    addons.Add((RU24Lib.Types.LauncherAddon)Activator.CreateInstance(ty));
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine("Error while loading plugin! Filename: " + file);
                            Console.WriteLine("---");
                            Console.WriteLine("EXC: " + exc.ToString() + ". " + exc.Message);
                            continue;
                        }
                        
                    }
                    foreach (var addon in addons)
                    {
                        try
                        {
                            addon.Init();
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine("Error while loading plugin " + exc.Message + addon.Name);
                            continue;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Directory RU24_PLUGINS doesn't exists!");
                }
                
            }
            catch
            {

            }
            finally
            {
                Console.WriteLine("All plugins are loaded");
            }
        }

        public delegate void EngineLoadCompleteEventHandler(object sender, EventArgs e);
        public static event EngineLoadCompleteEventHandler EngineLoadComplete;

        /// <summary>
        /// Run form
        /// </summary>
        /// <param name="mainform">Form</param>
        public static void Run(Form mainform)
        {
            mainform.ShowDialog();
        }

        /// <summary>
        /// Information
        /// </summary>
        /// <param name="par">modrep - ModsRepositories, servrep - ServerRepositories, hardinfo - Hardware information</param>
        public static void Info(string par, bool hardwarejson = false)
        {
            if (par == "modrep")
            {
                Console.WriteLine("Mods repositories: ");
                for (int i = 0; i < ModsRepositories.Length; i++)
                {
                    var rep = ModsRepositories[i];
                    Console.WriteLine("Repository #" + i);
                    Console.WriteLine("\t" + rep.Name);
                    Console.WriteLine("\t" + rep.Version);
                    Console.WriteLine("\tConnectInformation: ");
                    Console.WriteLine("\t\tUri:" + rep.Info.LoadUri);
                    Console.WriteLine("\t\tType:" + rep.Info.LoadType);
                    Console.WriteLine("\t\tNode:" + rep.Info.Node);
                    Console.WriteLine("\t\tProtected:" + rep.Info.MustBeProtected);
                }
            }
            else if (par == "servrep")
            {
                Console.WriteLine("Servers repositories: ");
                Console.WriteLine();
                for (int i = 0; i < Repositories.Length; i++)
                {
                    var rep = Repositories[i];
                    Console.WriteLine("Repository #" + i);
                    Console.WriteLine("\t" + rep.Name);
                    Console.WriteLine("\t" + rep.Version);
                    Console.WriteLine("ConnectInformation: ");
                    Console.WriteLine("\t\tUri:" + rep.Info.LoadUri);
                    Console.WriteLine("\t\tType:" + rep.Info.LoadType);
                    Console.WriteLine("\t\tNode:" + rep.Info.Node);
                    Console.WriteLine("\t\tProtected:" + rep.Info.MustBeProtected);
                }
            }
            else if (par == "hardinfo")
            {
                //System info:
                Console.WriteLine("\tSystem info:");
                Console.WriteLine("\t\tSystemDir: " + Environment.SystemDirectory);
                Console.WriteLine("\t\tSystemVersion: " + Environment.OSVersion.VersionString);
                Console.WriteLine("\t\tSystem6486: " + Environment.Is64BitOperatingSystem + "$" + !Environment.Is64BitOperatingSystem);
                Console.WriteLine("\t\tSystemPageSize: " + Environment.SystemPageSize);
                Console.WriteLine("\t\tSystemRunnedMinSec: " + (Environment.TickCount / 60000) + "$" + (Environment.TickCount / 1000));
                Console.WriteLine("\t\tSystemPhysicalMem: " + Environment.WorkingSet + "$" + HardwareInformation.HardwareInfo.GetPhysicalMemory());
                Console.WriteLine("\t\tComputerName: " + SystemInformation.ComputerName);
                //Processor info:
                Console.WriteLine("\tProcessorInfo: ");
                Console.WriteLine("\t\tInfo: " + HardwareInformation.HardwareInfo.GetProcessorInformation());
                //Cpu info:
                Console.WriteLine("\tCPUInfo:");
                Console.WriteLine("\t\tCurrentClockSpeed: " + HardwareInformation.HardwareInfo.GetCPUCurrentClockSpeed());
                Console.WriteLine("\t\tGHzSpeed: " + HardwareInformation.HardwareInfo.GetCpuSpeedInGHz());
                Console.WriteLine("\t\tManufacturer: " + HardwareInformation.HardwareInfo.GetCPUManufacturer());
                //Login info:
                Console.WriteLine("\tGetLogInfo: ");
                Console.WriteLine("\t\tName: " + SystemInformation.UserName);
                Console.WriteLine("\t\tInteractive: " + Environment.UserInteractive);
                Console.WriteLine("\t\tDomainName: " + Environment.UserDomainName);
                
                //ExtraJson:
                if (hardwarejson)
                {
                    Console.WriteLine(JsonConvert.SerializeObject(new info()));
                }
            }
        }

        public class info
        {
            public class SystemInfo
            {
                public string SystemDir = Environment.SystemDirectory;
                public string SystemVersion = Environment.OSVersion.VersionString;
                public bool System64 = Environment.Is64BitOperatingSystem;
                public int SystemPageSize = Environment.SystemPageSize;
                public int SystemRunnedMin = Environment.TickCount / 60000;
                public int SystemRunnedSec = Environment.TickCount / 1000;
                public long SystemPhysicalMemory = Environment.WorkingSet;
                public string h_SystemPhysicalMemory = HardwareInformation.HardwareInfo.GetPhysicalMemory();
                public string ComputerName = SystemInformation.ComputerName;
            }
            public class ProcessorInfo
            {
                public string Info = HardwareInformation.HardwareInfo.GetProcessorInformation();
            }
            public class CPUInfo
            {
                public int CurrentClockSpeed = HardwareInformation.HardwareInfo.GetCPUCurrentClockSpeed();
                public double? GHzSpeed = HardwareInformation.HardwareInfo.GetCpuSpeedInGHz();
                public string Manufacturer = HardwareInformation.HardwareInfo.GetCPUManufacturer();
            }
            public class LogInfo
            {
                public string Name = SystemInformation.UserName;
                public bool Interactive = SystemInformation.UserInteractive;
                public string DomainName = SystemInformation.UserDomainName;
            }
        }
    }
}