﻿#if (DEBUG)
#define TESTINGENABLED
#else
#define RELEASE
#endif
using RU24Lib.Types;
using Simple_RU24.Windows;
using Simple_RU24.WindowsFunctions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Media;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Simple_RU24
{
#if (RELEASE)
        public partial class MainWindow : AeroWindow
#else
        public partial class MainWindow : Form
#endif
    {
        // Привет, друг
        // Я упростил тебе задание по декомпиляции данной программы.
        // Везде оставленны комментарии, которые мне кажется помогут понять святое говнокодище.
        //
        // (C) DAFRI_NOCHITEROV, RU24 DEVELOPER, OWNER
        public MainWindow()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Opacity = 0;
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            this.serverGrid1.Servers.MouseDoubleClick += Servers_MouseDoubleClick;
        }
        
        private async void Servers_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            
            if (serverGrid1.Servers.SelectedIndex != -1 && serverGrid1.Servers.SelectedItem != null)
            {
                try
                {
                    servStrip.Visible = true;

                    SetServerBarText("Обработка...");
                    SetServerBarProgress(10, false);

                    var FakeServer = (Server)serverGrid1.Servers.SelectedItem;
                    var RealServer = (ArmA2Server)FakeServer.Real;

                    SetServerBarText("Ожидание информации...");
                    SetServerBarProgress(50, false);

                    var Info = new RU24Lib.Types.ArmA2ServerInformation_GamerLabs(RealServer, false);

                    
                    SetServerBarText("Получение информации о сервере...");
                    SetServerBarProgress(65, false);

                    await Task.Factory.StartNew(() => Info.Refresh());

                    SetServerBarText("Проверка...");
                    SetServerBarProgress(60, false);

                    // LastError будет UPDCOMPLETED если всё успешно обновлено
                    if (Info.LastError == "UPDCOMPLETED")
                    {
                        SetServerBarText("Информация о сервере получена");
                        SetServerBarProgress(100, false);

                        Console.WriteLine(Info.MaxPlayers);
                        SetServerPanelInfo(Info.Name, Info.MaxPlayers + "", Info.Players);
                    }
                    //если возникла ошибка
                    else if (Info.LastError == "UPDFATALERROR")
                    {
                        SetServerBarText("Получить информацию не удалось");
                        SetServerBarProgress(0, false);

                        MessageBox.Show("К сожалению, получить данные не получилось. Возможно, сервер не разрешает проверку статуса по querty-методу");
                        SetServerPanelInfo(RealServer.Name, RealServer.MaximumPlayers + "", null);
                    }
                    // может быть ещё передано название исключения!
                    else
                    {
                        SetServerBarText("Неизвестная ошибка!");
                        SetServerBarProgress(0, false);

                        SetServerPanelInfo(RealServer.Name, RealServer.MaximumPlayers + "", null);
                        MessageBox.Show(Info.LastError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }

                    OpenServerPanel();
                    servStrip.Visible = false;
                }
                catch (Exception exc)
                {
#if (DEBUG)
                    MessageBox.Show(exc + " " + exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
#endif
                }
            }
        }

        // Здесь окно превращается в красивое Aero окошко \w

        #region Window styles

        //Add the code directly
        #endregion Window styles

        // В данном регионе происходит загрузка списка серверов
        // и их фильтрация перед добавлением, добавлением

        #region Server Serching

        /// <summary>
        /// Сервера в наборе из репозитория
        /// </summary>
        public ArmA2Server[] servers = null;

        /// <summary>
        /// Текущий, выбранный пользователем репозиторий.
        /// Если он не выбран ссылка будет нулевой
        /// </summary>
        public ArmA2ServerRepository selectedRepository { get; private set; }

        /// <summary>
        /// Добавляет реальный сервер, но прежде конвертирует в "бутафорию"
        /// </summary>
        /// <param name="server"></param>
        public void AddServer(ArmA2Server server)
        {
            serverGrid1.Servers.Items.Add(new Server(server));
        }

        /// <summary>
        /// Фильтрация сервера
        /// </summary>
        /// <param name="server">Сервер</param>
        /// <param name="flags">Флаги запрета</param>
        /// <returns>Истинна, если сервер прошел проверку, иначе ложно</returns>
        public bool CheckServer(ArmA2Server server, ExcFilterFlags flags)
        {
            if (flags.Empty && server.CurrentPlayers == 0)
            {
                return false;
            }
            if (flags.Full && server.CurrentPlayers == server.MaximumPlayers)
            {
                return false;
            }
            if (flags.HighPing && server.Ping > 300)
            {
                return false;
            }
            if (flags.NonOfficial && !server.Official)
            {
                return false;
            }
            //if(flags.NotTrusted && !server.Trusted) { return false; }
            if (flags.HaveModifications && server.Modifications.Length != 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Заполнить таблицу серверов
        /// </summary>
        /// <param name="servers">Сервера</param>
        public void FillServerGrid(ArmA2Server[] servers)
        {
            foreach (ArmA2Server server in servers)
            {
                serverGrid1.Servers.Items.Add(new Server(server));
            }
        }

        /// <summary>
        /// Профильтровать список серверов
        /// </summary>
        /// <param name="servers">Сервера</param>
        /// <param name="flags">Флаги фильтрации</param>
        /// <returns>Отфильтрованные сервера</returns>
        public ArmA2Server[] FilterServerList(ArmA2Server[] servers, ExcFilterFlags flags)
        {
            List<ArmA2Server> completed = new List<ArmA2Server>();
            foreach (ArmA2Server serv in servers)
            {
                try
                {
                    if (CheckServer(serv, flags))
                    {
                        completed.Add(serv);
                    }
                }
                catch (Exception)
                {
                    completed.Add(serv);
                }
            }
            return completed.ToArray();
        }

        /// <summary>
        /// Получить выделенные пользователем флаги
        /// </summary>
        /// <returns></returns>
        public ExcFilterFlags GetFlags()
        {
            return new ExcFilterFlags()
            {
                Empty = srvFilter_Empty.Checked,
                Full = srvFilter_Full.Checked,
                HaveModifications = srvFilter_Modifications.Checked,
                HighPing = srvFilter_HighPing.Checked,
                NonOfficial = srvFilter_NotOfficial.Checked,
                NotTrusted = srvFilter_NotTrusted.Checked
            };
        }

        /// <summary>
        /// Дает команду на обновление серверов с репозитория.
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        public void UpdateServers(ArmA2ServerRepository repository)
        {
            servStrip.Visible = true;
            //Thread thread = new Thread(new ParameterizedThreadStart(UpdateServersAsync));
            selectedRepository = repository;
            ServersUpdater.RunWorkerAsync();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            serverSearch_Button.Text = "Идет загрузка...";
            Main.SetProgressBarState(sbar_progbar, Main.ProgressBarState.Normal);
            serverSearch_Panel.Enabled = false;
            if (comboBox1.SelectedIndex != -1)
            {
                UpdateServers(Engine.Repositories[comboBox1.SelectedIndex]);
            }
            serverSearch_Button.Text = "Искать";
        }


        private void SetServerBarProgress(int value, bool sync = true)
        {
            if (sync)
            {
                serverSearch_PBAR.Value = value;
            }
            sbar_progbar.Value = value;
        }
        private void SetServerBarText(string text)
        {
            sbar_lab.Text = text;
        }


        private async void ServersUpdater_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            
            MainWindow.CheckForIllegalCrossThreadCalls = false;
            // var repository = (ArmA2ServerRepository)_repository;
            SetServerBarProgress(0);
            
            
            this.serverGrid1.Dispatcher.Invoke(delegate
            {
                this.serverGrid1.Servers.Items.Clear();
            });

            SetServerBarProgress(10);
            SetServerBarText("Получение списка серверов");

            ArmA2Server[] temp_servers = Repositories.RepositoryWorker.GetFrom(selectedRepository);
            if (temp_servers == null)
            {
                SetServerBarText("Ошибка: нулевое значение списка серверов [SS3]");
                serverSearch_Button.Text = "Ошибка!";
                SetServerSearchPanelLockState(LockState.Unlocked);
                SetServerSearchControlsLockState(LockState.Unlocked);
                Main.SetProgressBarState(serverSearch_PBAR, Main.ProgressBarState.Errored);
                Main.SetProgressBarState(sbar_progbar, Main.ProgressBarState.Errored);

                WaitBeforeSearchingAgain();

                return;
            }


            for (int i = 0; i < temp_servers.Length; i++ )
            {
                // Здесь фичи
                SetServerBarText(((i + 1) * 100) / temp_servers.Length + " обработано...");
                SetServerBarProgress(((i + 1) * 100) / temp_servers.Length);
                //serverSearch_PBAR.Value = ((i+1) * 100) / temp_servers.Length;
                // Вторая фича
                var serv = temp_servers[i];

                if (CheckServer(serv, GetFlags()))
                {
                    var server = new Server(serv);
                    server.ModificationStr = serv.ModificationsString(5);
                    //server.PingStr = await server.GetPing();
                    //await server.GetInfo();
                    this.serverGrid1.Dispatcher.Invoke(delegate
                    {
                        this.serverGrid1.Servers.Items.Add(server);
                    });
                }
                System.Threading.Thread.Sleep(1);
            }

            SetServerBarText("Поиск завершен");

            //SetServerSearchPanelLockState(LockState.Unlocked);
            //SetServerSearchControlsLockState(LockState.Unlocked);

            Main.SetProgressBarState(serverSearch_PBAR, Main.ProgressBarState.Paused);
            WaitBeforeSearchingAgain();
            Main.SetProgressBarState(serverSearch_PBAR, Main.ProgressBarState.Normal);

            
        }

        

        private void WaitBeforeSearchingAgain()
        {
            //SetProgressBarState(serverSearch_PBAR, ProgressBarState.Paused);
            SetServerSearchPanelLockState(LockState.Locked);

            serverSearch_Button.Enabled = false;
            
            for (int i = 0; i < 10; i++)
            {
                SetServerSearchPanelLockState(LockState.Locked);
                serverSearch_Button.Enabled = false;
                serverSearch_Button.Text = "Подождите " + (10 - i) + "сек.";
                System.Threading.Thread.Sleep(1000);
            }

            SetServerSearchPanelLockState(LockState.Unlocked);
            serverSearch_Button.Enabled = true;
            serverSearch_Button.Text = "Повторить поиск";
            servStrip.Visible = false;
            //SetProgressBarState(serverSearch_PBAR, ProgressBarState.Normal);
        }

        /// <summary>
        /// Исключительные флаги поиска для фильтра серверов
        /// </summary>
        public struct ExcFilterFlags
        {
            /// <summary>
            /// Пустые
            /// </summary>
            public bool Empty { get; set; }

            /// <summary>
            /// Заполненные
            /// </summary>
            public bool Full { get; set; }

            /// <summary>
            /// Имеют модификации
            /// </summary>
            public bool HaveModifications { get; set; }

            /// <summary>
            /// С высоким пингом
            /// </summary>
            public bool HighPing { get; set; }

            /// <summary>
            /// Не официальные
            /// </summary>
            public bool NonOfficial { get; set; }

            /// <summary>
            /// Не подтвержденны
            /// </summary>
            public bool NotTrusted { get; set; }
        }

        /// <summary>
        /// "Бутафория" сервера, содержит реальную копию
        /// </summary>
        public class Server
        {
            /// <summary>
            /// Модификации сервера
            /// </summary>
            public String Modifications;

            /// <summary>
            /// Конструктор из реального сервера в "бутафорию"
            /// </summary>
            /// <param name="server"></param>
            public Server(ArmA2Server server)
            {
                this.Name = server.Name;
                this.Modifications = server.ModificationsString(5);
                this.CurrentPlayersStr = server.CurrentPlayers + "";
                this.MaximumPlayersStr = server.MaximumPlayers + "";
                this.Country = server.Country;
                this.ModificationStr = server.ModificationsString(5);
                this.Real = server;
                this.Reload();
            }

            private async void Reload()
            {
                this.PingStr = await GetPing();
                await this.GetInfo();
            }

            /// <summary>
            /// Страна сервера
            /// </summary>
            public String Country { get; set; }

            /// <summary>
            /// Имя сервера
            /// </summary>
            public String Name { get; set; }

            /// <summary>
            /// Настоящая копия сервера
            /// </summary>
            public ArmA2Server Real { get; set; }

            /// <summary>
            /// Пинг до сервера
            /// </summary>
            public async Task<String> GetPing()
            {
                Console.WriteLine("Try to get ping");
                var pi = await (new Ping()).SendPingAsync(Real.ConnectInformation.Ip, 300);
                return pi.RoundtripTime + "";
            }

            public String PingStr { get; set; }
            
            /// <summary>
            /// Количество игроков на сервере
            /// </summary>
            public String CurrentPlayersStr { get; set; }

            /// <summary>
            /// Максимальное количество игроков на сервере
            /// </summary>
            public String MaximumPlayersStr { get; set; }

            /// <summary>
            /// Модификации на сервере
            /// </summary>
            public String ModificationStr { get; set; }

            public async Task<bool> GetInfo()
            {
                return await Task.Factory.StartNew(() => {
                    try{
                        Console.WriteLine("Try to get information about " + Real.Name + " server.");
                        //BaseServerInfo.GameServer gs = new ArmaServerInfo.GameServer(Real.ConnectInformation.Ip, Real.ConnectInformation.Port);
                        //gs.Update();
                        //this.MaximumPlayersStr = gs.ServerInfo.MaxPlayers + "";
                        //this.ModificationStr = gs.ServerInfo.Mod;
                        //this.Name = gs.Name;
                        //this.CurrentPlayersStr = gs.ServerInfo.NumPlayers + "";
                        return true;
                    }
                    catch
                    {
                        Console.WriteLine("Error");
                        return false;
                    }
                });
            }
        }

        // Конец региона.
        // Все что относится к поиску серверов здесь

        #endregion Server Serching

        // А это коллекция методов для элементов.
        // NOTE: Упор делается на обращение к методам, а не выполнению закрытно

        #region Events


        public delegate void ServersUpdatedEventHandler();
        public event ServersUpdatedEventHandler onServersUpdated;


        private void FirstAnimation()
        {
            SoundPlayer sp = new SoundPlayer(Environment.GetFolderPath(Environment.SpecialFolder.Windows) + "\\Media\\tada.wav");
            sp.Play();
            while (AllSettings.LauncherSettings.Default.FirstRun)
            {
                var to = 10;
                var time = 200;
                var t = new Transition(new TransitionType_EaseInEaseOut(time));
                bool completed = false;

                t.add(commandLink1, "Left", commandLink1.Left - to);
                t.run();

                t.TransitionCompletedEvent += delegate
                {
                    completed = true;
                };

                while (!completed)
                {

                }

                t = new Transition(new TransitionType_EaseInEaseOut(time));
                completed = false;

                t.add(commandLink1, "Left", commandLink1.Left + to);
                t.run();

                t.TransitionCompletedEvent += delegate
                {
                    completed = true;
                };

                while (!completed)
                {

                }

                t = new Transition(new TransitionType_EaseInEaseOut(time));
                completed = false;

                t.add(commandLink1, "Left", commandLink1.Left + to);
                t.run();

                t.TransitionCompletedEvent += delegate
                {
                    completed = true;
                };

                while (!completed)
                {

                }

                t = new Transition(new TransitionType_EaseInEaseOut(time));
                completed = false;

                t.add(commandLink1, "Left", commandLink1.Left - to);
                t.run();

                t.TransitionCompletedEvent += delegate
                {
                    completed = true;
                };

                while (!completed)
                {

                }


                System.Threading.Thread.Sleep(100);
            }
        }

        

        private void MainWindow_Load(object sender, EventArgs e)
        {
            // Make window style better
            if (Simple_RU24.AllSettings.LauncherSettings.Default.FirstRun)
            {
                commandLink1.Note = "Пожалуйста, настройте лаунчер!";
                commandLink1.ForeColor = Color.Red;

                Task.Factory.StartNew(() => FirstAnimation());
            }

            
            CloseServerPanel();

            try
            {
                Transition tr = new Transition(new TransitionType_Acceleration(1000));
                tr.add(this, "Opacity", 1.0);
                tr.TransitionCompletedEvent += delegate
                {
                    this.Opacity = 1.0;
                    ReloadServersRepositories();
                };
                tr.run();

                
            }
            catch (Exception)
            {
                Environment.Exit(100); 
                // INITIALIZATION FAILED
            }

            (new Secure()).Check_Security();
        }

        private class Secure
        {
            private int rlib_size = 25600;
            public void Check_Security()
            {
                {
                    var rlib = new System.IO.FileInfo(Environment.CurrentDirectory + "\\RU24Lib.dll");
                    if (rlib.Length != rlib_size)
                    {
#if (RELEASE)
                         (new Forms.LIB_HACKED()).ShowDialog();
                         Environment.Exit(-957);
#endif
#if (DEBUG)
                        Console.WriteLine("warn_lib_hacked");
#endif
                    }
                    else
                    {
#if (DEBUG)
                        Console.WriteLine("!warn_lib_hacked");
#endif
                    }
                }
            }
        }

        // Кнопка открытия репозиториев
        private void button2_Click(object sender, EventArgs e)
        {
            Forms.Repositories RepositoriesForm = new Forms.Repositories(Forms.Repositories.How.Servers);

            RepositoriesForm.FormClosing += delegate
            {
                ReloadServersRepositories();
            };

            RepositoriesForm.ShowDialog();
            
        }

        // Выбор репозитория
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == -1) SetServerSearchControlsLockState(LockState.Locked); // Если репозиторий не выбран из списка элементы поиска блокируются
            else SetServerSearchControlsLockState(LockState.Unlocked); // Иначе они полностью разблокированы
        }

        #endregion Events

        // Здесь собраны все методы, которые могут быть нужны разработчику дополнений для лаунчера.

        #region Methods for add-on developers





        public LocalTypes.ModsGridItemController AddMod(RU24Lib.Types.ArmA2Modification modification)
        {
            if(modification == null) { throw new ArgumentNullException(); } // Check

            var obj = new LocalTypes.ModsGridItemController(modification); // Create object

            this.AdditionalMods.Add(obj); // Add

            return obj; // And return
        }


        /// <summary>
        /// Состояние блокировки
        /// </summary>
        public enum LockState
        {
            /// <summary>
            /// Элементы заблокированы для управления
            /// </summary>
            Locked,

            /// <summary>
            /// Элементы доступны для управления
            /// </summary>
            Unlocked
        }

        /// <summary>
        /// Установить состояние блокировки для кнопок и фильтров поиска серверов.
        /// Такие как "Поиск", "С высоким пингом"
        /// </summary>
        /// <param name="State">Состояние блокировки</param>
        public void SetServerSearchControlsLockState(LockState State)
        {
            bool state = State == LockState.Locked ? false : true;
            serverSearch_Button.Enabled = state;
            srvFilter_Empty.Enabled = state;
            srvFilter_Full.Enabled = state;
            srvFilter_HighPing.Enabled = state;
            srvFilter_Modifications.Enabled = state;
            srvFilter_NotOfficial.Enabled = state;
            srvFilter_NotTrusted.Enabled = state;
        }

        /// <summary>
        /// Установить состояние блокировки панели поиска серверов.
        /// </summary>
        /// <param name="State"></param>
        public void SetServerSearchPanelLockState(LockState State)
        {
            serverSearch_Panel.Enabled = State == LockState.Locked ? false : true;
        }

        /// <summary>
        /// Выполняет обновление репозиториев серверов
        /// </summary>
        public void ReloadServersRepositories()
        {
            comboBox1.Items.Clear();
            foreach (ArmA2ServerRepository repo in Engine.Repositories)
            {
                comboBox1.Items.Add(repo.Name);
                Console.WriteLine("Added new repository! " + repo.Name);
            }
        }

        #endregion Methods for add-on developers

        #region Server Panel (Info about server)

        private void ServerPanel_Close_Click(object sender, EventArgs e)
        {
            CloseServerPanel();
        }

        private void OpenServerPanel(Action Completed = null)
        {
            CloseServerPanel(delegate
            {
                ServerPanel.Left = this.Width + 1;
                ServerPanel.Visible = true;
                var Trasnition = new Transition(new TransitionType_CriticalDamping(1000));
                Trasnition.add(ServerPanel, "Left", 648);
                Trasnition.run();
                Trasnition.TransitionCompletedEvent += delegate
                {
                    if (Completed != null)
                    {
                        Completed();
                    }
                };
            });
        }

        List<LocalTypes.ModsGridItemController> AdditionalMods = new List<LocalTypes.ModsGridItemController>();
        private void CloseServerPanel(Action Completed = null)
        {
            Console.WriteLine("CLOSING...");
            var Transition = new Transition(new TransitionType_EaseInEaseOut(500));
            Transition.add(ServerPanel, "Left", this.Width + 1);
            Transition.run();
            Transition.TransitionCompletedEvent += delegate
            {
                Console.WriteLine("CLOSED!");
                ServerPanel.Visible = false;
                if (Completed != null)
                {
                    Completed();
                }
            };
        }

        private void SetServerPanelInfo(string ServerName = "...", string MaxPlayers = "...", ArmA2ServerInformation_GamerLabs.Player[] Players = null)
        {
            ServerPanel_NameOfServer.Text = ServerName;
            ServerPanel_MaxPlayersCounter.Text = MaxPlayers;

            ServerPanel_PlayersListBox.Items.Clear();
            
            if (Players != null)
            {
                ServerPanel_NowPlaying.Text = Players.Length + "";
                foreach (var item in Players)
                {
                    ServerPanel_PlayersListBox.Items.Add(item.name);
                }
            }
            else
            {
                ServerPanel_NowPlaying.Text = "Нулевые данные";
                ServerPanel_PlayersListBox.Items.Add("Нулевые данные! :C");
            }
        }

        #endregion Server Panel (Info about server)

        private void tabControl1_TabStopChanged(object sender, EventArgs e)
        {
            
        }

        private void LoadModsTab()
        {
#if(DEBUG)
            modsWarning_Panel.Visible = false;
            modsPage_LoadBar.Visible = false;
            modsPage_LoadLabel.Visible = false;

            Task.Factory.StartNew(() =>
            {
                foreach (var item in AdditionalMods)
                {
                    this.modDownloadsGrid1.AddItem(item);
                }
            });
            
#else
            //modsPage_Grid.Visible = false;
            //modsWarning_Panel.Visible = true;
            //SetProgressBarState(modsPage_LoadBar, ProgressBarState.Errored);
            //modsPage_LoadLabel.Text = "Возникла проблема: обращение за репозиториями не удалось (\"Скоро\")";
            //modsWarning_Text.Text = "Раздел в разработке. Скоро вы увидите прекрасный раздел модификаций";
            //modsWarning_Button.Enabled = false;

            modsWarning_Panel.Visible = false;
            modsPage_LoadBar.Visible = false;
            modsPage_LoadLabel.Visible = false;

            Task.Factory.StartNew(() =>
            {
                foreach (var item in AdditionalMods)
                {
                    this.modDownloadsGrid1.AddItem(item);
                }
            });
#endif
        }


        

        private void LoadModsRepository()
        {
            
        }

        private void LoadMods()
        {
            List<string> auids = new List<string>();
            foreach (var mr in Engine.ModsRepositories)
            {
                var mns = Repositories.RepositoryWorker.GetFrom(mr);
                if (mns == null) { continue; }
                foreach (var mn in mns)
                {
                    if (CheckForExists<string>(auids.ToArray(), mn.UID)) { continue; }
                    //modsGrid1.ModificationsGrid.Items.Add((new LocalTypes.ModInformation(mn)));
                }
            }
        }


        private bool CheckForExists<T>(T[] array, T search)
        {
            foreach (var o in array)
            {
                if (o.Equals(search))
                {
                    return true;
                }
            }
            return false;
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.Action == TabControlAction.Selecting)
            {
                
            }
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void modsWarning_Panel_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (modsWarning_Panel.Visible)
                {
                    int _beginsize = 157;

                    modsWarning_Panel.Height = 0;

                    Transition t = new Transition(new TransitionType_EaseInEaseOut(1000));
                    t.add(modsWarning_Panel, "Height", _beginsize);

                    t.run();

                    t.TransitionCompletedEvent += delegate
                    {
                        modsWarning_Panel.Height = _beginsize;
                    };
                }
                else if (!modsWarning_Panel.Visible)
                {
                    int _beginsize = modsWarning_Panel.Height;

                    //modsWarning_Panel.Height = 0;

                    Transition t = new Transition(new TransitionType_Bounce(10));
                    t.add(modsWarning_Panel, "Height", 0);

                    t.run();

                    t.TransitionCompletedEvent += delegate
                    {
                        modsWarning_Panel.Height = _beginsize;
                    };
                }
            }
            catch
            {

            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == modsPage)
            {
                LoadModsTab();
            }
        }

        private void modsWarning_Button_Click(object sender, EventArgs e)
        {
            Forms.Repositories RepositoriesForm = new Forms.Repositories(Forms.Repositories.How.Mods);
            RepositoriesForm.ShowDialog();
        }

        private void modsPage_Grid_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                //if (modsPage_Grid.Visible)
                //{
                //    modsPage_Grid.Left = modsPage_Grid.Parent.Width + modsPage_Grid.Width;
                //    Transition tr = new Transition(new TransitionType_EaseInEaseOut(1000));
                //    tr.add(modsPage_Grid, "Left", 0);
                //    tr.run();
                //}
                //else
                //{
                //    //modsPage_Grid.Visible = true;
                //    modsPage_Grid.Left = 0;
                //    Transition tr = new Transition(new TransitionType_EaseInEaseOut(1000));
                //    tr.add(modsPage_Grid, "Left", modsPage_Grid.Parent.Width + modsPage_Grid.Width);
                //    tr.run();
                //}
            }
            catch
            {

            }
        }

        private void elementHost1_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }

        private void MainWindow_ControlAdded(object sender, ControlEventArgs e)
        {
            Console.WriteLine("Detected add control to MainWindow: " + e.Control + ". This have name: " + e.Control.Name + "!");
        }

        private void ServerPanel_JoinButton_Click(object sender, EventArgs e)
        {
            var usans = MessageBox.Show("Желаете вступать в игру?", "Вступление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (usans == System.Windows.Forms.DialogResult.Yes)
            {
                servStrip.Visible = true;
                SetServerBarText("Получение списка модификаций");
            }
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void commandLink1_Click(object sender, EventArgs e)
        {
            new Forms.SettingsMaster().ShowDialog();
        }

        private void modDownloadsGrid1_MouseDown(object sender, MouseEventArgs e)
        {
            if (!modDownloadsGrid1.Focused)
                modDownloadsGrid1.Focus();
        }

        private void modDownloadsGrid1_Scroll(object sender, ScrollEventArgs e)
        {
            modDownloadsGrid1.Update();
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            //listView1.(new LocalTypes.ModDownloadsItem(new ArmA2Modification() { Name = "Hello", Description = "Hello", Version = "1.0.0.0" }));
        }

        private void modDownloadsGrid1_DoubleClick(object sender, EventArgs e)
        {
            foreach (var item in AdditionalMods)
            {
                Console.WriteLine("Adding new: " + item.Modification.Name);
                modDownloadsGrid1.AddItem(item);
            }
        }


    }
}