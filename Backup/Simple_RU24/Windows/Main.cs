﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_RU24.WindowsFunctions
{
    public class Main
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);

        /// <summary>
        /// Задать состояние ToolStripProgressBar
        /// </summary>
        /// <param name="pBar">Полоса прогресса (ToolStripProgressBar)</param>
        /// <param name="state">Состояние</param>
        public static void SetProgressBarState(ToolStripProgressBar pBar, ProgressBarState state)
        {
            SendMessage(pBar.Control.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }

        /// <summary>
        /// Задать состояние ProgressBar
        /// </summary>
        /// <param name="pBar">Полоса прогресса (ProgressBar)</param>
        /// <param name="state">Состояние</param>
        public static void SetProgressBarState(ProgressBar pBar, ProgressBarState state)
        {
            Simple_RU24.WindowsFunctions.Main.SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }


        /// <summary>
        /// Состояние бара
        /// </summary>
        public enum ProgressBarState
        {
            /// <summary>
            /// Нормальное (зеленый цвет)
            /// </summary>
            Normal = 1,

            /// <summary>
            /// Приостановлен (желтый цвет)
            /// </summary>
            Paused = 3,

            /// <summary>
            /// Ошибка (красный цвет)
            /// </summary>
            Errored = 2
        }
    }


}
