﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_RU24.Windows
{
    /// <summary>
    /// Aero окно. Не работает в 8.1 и более новых системах
    /// </summary>
    public class AeroWindow : System.Windows.Forms.Form
    {
        [DllImport("dwmapi.dll", PreserveSig = false)]
        private static extern bool DwmIsCompositionEnabled();

        [DllImport("dwmapi.dll", PreserveSig = false)]
        private static extern void DwmExtendFrameIntoClientArea(IntPtr hwnd, ref MARGINS
        margins);

        [StructLayout(LayoutKind.Sequential)]
        public struct MARGINS
        {
            public int Left;
            public int Right;
            public int Top;
            public int Bottom;
        }

        protected override void OnLoad(EventArgs e)
        {
            if (DesignMode)
            {
                base.OnLoad(e);
                return;
            }
            if (DwmIsCompositionEnabled())
            {
                MARGINS margins = new MARGINS();
                margins.Right = margins.Left = margins.Top = margins.Bottom =
                this.Width + this.Height;
                DwmExtendFrameIntoClientArea(this.Handle, ref margins);
            }

            base.OnLoad(e);
        }

        private bool IsWindowsEight()
        {
            Version win8version = new Version(6, 2, 9200, 0);

            if (Environment.OSVersion.Platform == PlatformID.Win32NT &&
                Environment.OSVersion.Version >= win8version)
            {
                return true;
            }
            return false;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (DesignMode)
            {
                base.OnPaintBackground(e);
                return;
            }
            if (!IsWindowsEight() && DwmIsCompositionEnabled())
            {
                e.Graphics.Clear(this.TransparencyKey);
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.TransparencyKey = System.Drawing.Color.LavenderBlush;
            this.ResumeLayout(false);
            
        }
    }
}
