﻿using RU24Lib.Types;
using Simple_RU24.WindowsFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_RU24.LocalTypes
{
    public class ModsGridItemController
    {
        public ModsGridItemController(ArmA2Modification modification)
        {
            this._item = new Controls.ModsGridItem(modification);
            this._item.progressBar1.Value = this.Progress;
            this._item.Parent = this;
            this._mod = modification;
        }

        /// <summary>
        /// Полностью обновляет данные в панели
        /// </summary>
        public void Update()
        {
            this._item.OnInitialized(this._mod);
            this._item.Parent = this;
            this.SetProgress(Progress);
        }

        /// <summary>
        /// Полностью перезагружает панель
        /// </summary>
        public void Reload()
        {
            this._item = new Controls.ModsGridItem(this._mod);
            this._item.Parent = this;
            this.SetProgress(this.Progress);
        }

        private Controls.ModsGridItem _item { get; set; }

        /// <summary>
        /// Задает процент выполнения
        /// </summary>
        /// <param name="progress">Процент выполнения (макс. 100)</param>
        public void SetProgress(int progress)
        {
            _item.progressBar1.Value = progress;
            this.Progress = progress;
        }

        /// <summary>
        /// Текущее процент выполнения
        /// </summary>
        public int Progress { get; private set; }

        /// <summary>
        /// Задает состояние выполнения, а также процент выполнения
        /// </summary>
        /// <param name="state">Состояние выполнения</param>
        /// <param name="progress">Процент выполнения (макс. 100)</param>
        public void SetProgress(Main.ProgressBarState state, int progress)
        {
            if(progress > 100)
            {
                throw new ArgumentOutOfRangeException();
            }
            Main.SetProgressBarState(_item.progressBar1, state);
            _item.progressBar1.Value = progress;
        }

        ArmA2Modification _mod = null;

        public Controls.ModsGridItem Item { get { return _item; } set { this._item = value; } }

        /// <summary>
        /// Текущая модификация
        /// </summary>
        public ArmA2Modification Modification { 
            get { return _mod; }
            private set
            {
                this._mod = value;
            }
        }

        

        public virtual void InfoRequest(object sender,EventArgs e)
        {
            this.OnInfoRequest(sender, e);
        }
        public delegate void OnInfoRequestEventHandler(object sender, EventArgs e);
        public event OnInfoRequestEventHandler OnInfoRequest = delegate { };

        public virtual void DownloadRequest(object sender, EventArgs e)
        {
            this.OnDownloadRequest(sender, e);
        }
        public delegate void OnDownloadRequestEventHandler(object sender, EventArgs e);
        public event OnDownloadRequestEventHandler OnDownloadRequest = delegate { };
    }
}
