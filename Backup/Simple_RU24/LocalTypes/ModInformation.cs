﻿using System;
using System.Linq;
using RU24Lib.Types;
using System.Windows.Media.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Windows.Media;

namespace Simple_RU24.LocalTypes
{
    /// <summary>
    /// Информация о модификации
    /// </summary>
    public class ModInformation : ArmA2Modification
    {
        /// <summary>
        /// Информация о модификации
        /// </summary>
        /// <param name="modification"></param>
        public ModInformation(ArmA2Modification modification)
        {
            this.Name = modification.Name;
            TryLoadImage(modification.PictureUrl);
            this.UID = modification.UID;
            this.Version = modification.Version;
            this.Extract = modification.Extract;
            this.FolderName = modification.FolderName;
            this.ModificationLibaryUri = modification.ModificationLibaryUri;
        }

        

        /// <summary>
        /// Версия
        /// </summary>
        public String Version { get; set; }

        /// <summary>
        /// UID модификации
        /// </summary>
        /// <example>DAYZEPOCH_LUPD</example>
        public String UID { get; set; }

        /// <summary>
        /// Загрузка изображения модификации
        /// </summary>
        /// <param name="url">Ссылка на изображение</param>
        private void TryLoadImage(string url)
        {
            if (url!= null && url != "")
            {
                WebClient wc = new WebClient();
                wc.DownloadFile(url, Environment.CurrentDirectory + "\\cache\\modimages" + _clearname);
            }
        }

   

        /// <summary>
        /// Наичистейшее имя без символов
        /// </summary>
        public string _clearname
        {
            get
            {
                Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                return rgx.Replace(Name + "", "");
            }
        }

        /// <summary>
        /// Изображение
        /// </summary>
        public BitmapImage _img
        {
            get
            {
                if (File.Exists(Environment.CurrentDirectory + "\\cache\\modimages" + _clearname))
                {
                    return new BitmapImage(new Uri(Environment.CurrentDirectory + "\\cache\\modimages" + _clearname));
                }
                else
                {
                    return null;
                }
            }
        }
        
        /// <summary>
        /// ImageSource изображения
        /// </summary>
        public ImageSource _image
        {
            get
            {
                return _img;
            }
        }
    }
}
