﻿namespace Simple_RU24.Forms
{
    partial class SettingsMaster_MainPage
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.commandLink2 = new CommandLink();
            this.commandLink1 = new CommandLink();
            this.SuspendLayout();
            // 
            // commandLink2
            // 
            this.commandLink2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.commandLink2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.commandLink2.Location = new System.Drawing.Point(3, 85);
            this.commandLink2.Name = "commandLink2";
            this.commandLink2.Note = "Настроить параметры лаунчера, такие как его вид, отключение вспывающих диалогов и" +
    " другое.";
            this.commandLink2.Size = new System.Drawing.Size(623, 76);
            this.commandLink2.TabIndex = 5;
            this.commandLink2.Text = "Настроить лаунчер";
            this.commandLink2.UseVisualStyleBackColor = false;
            this.commandLink2.Click += new System.EventHandler(this.commandLink2_Click);
            // 
            // commandLink1
            // 
            this.commandLink1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.commandLink1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.commandLink1.Location = new System.Drawing.Point(3, 3);
            this.commandLink1.Name = "commandLink1";
            this.commandLink1.Note = "Нажмите сюда, если хотите настроить параметры игры, такие как - количество выделя" +
    "емой памяти, отключение всплывающего лого, аргументы и другое...";
            this.commandLink1.Size = new System.Drawing.Size(623, 76);
            this.commandLink1.TabIndex = 4;
            this.commandLink1.Text = "Настроить параметры игры";
            this.commandLink1.UseVisualStyleBackColor = false;
            this.commandLink1.Click += new System.EventHandler(this.commandLink1_Click);
            // 
            // SettingsMaster_MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.commandLink2);
            this.Controls.Add(this.commandLink1);
            this.Name = "SettingsMaster_MainPage";
            this.Title = "Настройки";
            this.ResumeLayout(false);

        }

        #endregion

        private CommandLink commandLink2;
        private CommandLink commandLink1;
    }
}
