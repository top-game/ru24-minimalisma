﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RU24Lib.Types;
using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;
using System.Xml;

namespace Simple_RU24.Forms
{
    public partial class Repositories_Add : Form
    {
        public Repositories_Add()
        {
            InitializeComponent();
        }

        public BaseRepository Show()
        {
            base.ShowDialog();

            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            infoLabel.Visible = true;
            TryToLoad();
        }

        private BaseRepository result;

        private void TryToLoad()
        {
            string current_str = "";
            try
            {
                if (comboBox1.SelectedIndex == 1)
                {
                    // Получаем XML документ
                    current_str = "Ошибка получения [X1Q1]";
                    XmlDocument Xml = new XmlDocument();
                    Xml.Load(textBox1.Text);
                    // Парсим его как JSON
                    current_str = "Ошибка парсинга [XXQ1]";
                    var str = JsonConvert.SerializeXmlNode(Xml);
                    if (textBox2.Text != "")
                    {
                        current_str = "Ошибка парсинга [XXQ2]";
                        // Если ветка задана парсим её
                        var json = JObject.Parse(str);
                        current_str = "Ошибка ветки [XXQ3]";
                        // Вход в ветку
                        var inner = json[textBox2.Text].ToString();
                        current_str = "Ошибка создания виртуального репозитория [XZQ4]";
                        // Обработка ветки как базовый репозиторий
                        result = JsonConvert.DeserializeObject<BaseRepository>(inner);
                    }
                    else
                    {
                        current_str = "Ошибка создания виртуального репозитория [XZQ5]";
                        // Просто обрабатываем
                        result = JsonConvert.DeserializeObject<BaseRepository>(str);
                    }
                }
                else if (comboBox1.SelectedIndex == 0)
                {
                    current_str = "Ошибка получения [X2Q1]";
                    // Получаем JSON
                    var str = (new WebClient()).DownloadString(textBox1.Text);
                    current_str = "Ошибка парсинга [XXQ2B]";
                    // Если ветвь задана:
                    if (textBox2.Text != "")
                    {
                        // Парсим все ветки
                        var json = JObject.Parse(str);
                        current_str = "Ошибка ветки [XXQ3B]";
                        // Вход в ветку
                        var inner = json[textBox2.Text].ToString();
                        current_str = "Ошибка создания виртуального репозитория [XZQ4B]";
                        // Обработка ветки как базовый репозиторий
                        result = JsonConvert.DeserializeObject<BaseRepository>(inner);
                    }
                    else
                    {
                        current_str = "Ошибка создания виртуального репозитория [XZQ5BA]";
                        // Также, просто обрабатываем
                        result = JsonConvert.DeserializeObject<BaseRepository>(str);
                    }
                }
                else
                {
                    infoLabel.Visible = true;
                    infoLabel.Text = "Неверно: формат [XXXQ0]";
                    return;
                }
                // Спрашиваем нашего пользователя
                var add_result = MessageBox.Show("Подтвердите добавление следующего репозитория:\nНазвание -\t" + result.Name + "\nВерсия -\t" + result.Version + "\nЕсли это то, что нужно нажмите кнопку \"Да\", иначе нажмите \"Нет\".\n\nНекоторые репозитории могут быть опасны! Не доверяйте непровренным репозиториям.", "Информация о репозитории", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (add_result == System.Windows.Forms.DialogResult.Yes)
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    return;
                }
                result.Info = new BaseRepositoryInfo();
                result.Info.LoadUri = new Uri(textBox1.Text);
                result.Info.Node = textBox2.Text.Length == 0 ? null : textBox2.Text;
                result.Info.LoadType = comboBox1.SelectedIndex == 0 ? BaseRepositoryLoadType.JSON : BaseRepositoryLoadType.XML;

                Console.WriteLine(result.Info.LoadUri);

                this.Close();
            }
            catch
            {
                infoLabel.Text = current_str;
                infoLabel.ForeColor = Color.OrangeRed;
                infoLabel.Visible = true;
            }
        }

        private void Repositories_Add_Load(object sender, EventArgs e)
        {
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }
    }
}