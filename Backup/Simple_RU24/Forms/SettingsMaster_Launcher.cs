﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_RU24.Forms
{
    public partial class SettingsMaster_Launcher : Controls.SettingsMasterForm
    {
        public SettingsMaster_Launcher()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void BrowseButton1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Укажите папку для загрузки модов";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                ModsFolder.Text = fbd.SelectedPath;
            }
        }

        private void BrowseButton2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Укажите папку для загрузки кэша. Она будет очищатся сразу после закрытия программы";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                CacheFolder.Text = fbd.SelectedPath;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (LimitSpeedCheckBox.Checked)
            {
                AllSettings.LauncherSettings.Default.LimitDownloadSpeed = (int)LimitSpeedDownload.Value;
                AllSettings.LauncherSettings.Default.LimitUploadSpeed = (int)LimitSpeedUpload.Value;
            }
            if (System.IO.Directory.Exists(ModsFolder.Text))
            {
                AllSettings.LauncherSettings.Default.ModsFolder = ModsFolder.Text;
            }
            else
            {
                ModsFolder.ForeColor = Color.Red;
            }
            if (System.IO.Directory.Exists(CacheFolder.Text))
            {
                AllSettings.LauncherSettings.Default.CacheFolder = CacheFolder.Text;
            }
            else
            {
                CacheFolder.ForeColor = Color.Red;
            }
            if (checkBox1.Enabled && System.IO.Directory.Exists(textBox1.Text))
            {
                AllSettings.LauncherSettings.Default.TorrentSavePath = textBox1.Text;
            }
            else if(!checkBox1.Enabled)
            {
                AllSettings.LauncherSettings.Default.TorrentSavePath = CacheFolder.Text;
            }
            else
            {
                textBox1.ForeColor = Color.Red;
            }

            if (AllSettings.LauncherSettings.Default.FirstRun)
            {
                AllSettings.LauncherSettings.Default.FirstRun = false;
                AllSettings.LauncherSettings.Default.Save();

                Parent.ChangePage(new SettingsMaster_Welcome());
            }
            else
            {
                AllSettings.LauncherSettings.Default.Save();
                var t = new Transitions.Transition(new Transitions.TransitionType_EaseInEaseOut(1000));
                t.add(SettingsSavedLabel, "Top", this.Height - SettingsSavedLabel.Height - 1);

                t.run();
            }

            
        }

        private void SettingsMaster_Launcher_Load(object sender, EventArgs e)
        {
            SettingsSavedLabel.Top = this.Height + SettingsSavedLabel.Height + 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Укажите папку для загрузки кэша. Она будет очищатся сразу после закрытия программы";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                CacheFolder.Text = fbd.SelectedPath;
            }
        }

        private void LimitSpeedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            LimitSpeedDownload.Enabled = LimitSpeedCheckBox.Enabled;
            LimitSpeedUpload.Enabled = LimitSpeedCheckBox.Enabled;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = checkBox1.Enabled;
            button2.Enabled = checkBox1.Enabled;
        }
    }
}
