﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_RU24.Forms
{
    public partial class SettingsMaster_MainPage : Controls.SettingsMasterForm
    {
        public SettingsMaster_MainPage()
        {
            InitializeComponent();
            this.OnPageClosing += SettingsMaster_MainPage_OnPageClosing;
        }

        void SettingsMaster_MainPage_OnPageClosing(object sender, Controls.SettingsMasterForm.OnPageClosingEventArgs e)
        {
        }

        private void commandLink1_Click(object sender, EventArgs e)
        {
            Parent.ChangePage(new Forms.SettingsMaster_ArmAII());
        }

        private void commandLink2_Click(object sender, EventArgs e)
        {
            Parent.ChangePage(new SettingsMaster_Launcher());
        }
    }
}
