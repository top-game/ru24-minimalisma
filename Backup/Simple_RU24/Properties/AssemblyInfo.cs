﻿using System.Reflection;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("RU24 Launcher")]
[assembly: AssemblyDescription("RU24 Minimalisma is launcher of RU24 TOP-GAME")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TOP-GAME, RU24")]
[assembly: AssemblyProduct("Simple_RU24")]
[assembly: AssemblyCopyright("Copyright © RU24 2015")]
[assembly: AssemblyTrademark("(C) RU24")]
[assembly: AssemblyCulture("")]

// Параметр ComVisible со значением FALSE делает типы в сборке невидимыми
// для COM-компонентов.  Если требуется обратиться к типу в этой сборке через
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("336a1eeb-7b8d-492a-b51c-28be2f4dcc27")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("00003")]
[assembly: AssemblyFileVersion("00003")]